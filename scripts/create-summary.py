#!/usr/bin/env python
import sys
import json
import subprocess
from pathlib import Path


def humansize(nbytes, suffixes=("B", "KiB", "MiB", "GiB", "TiB")):
    i = 0
    while nbytes >= 1024 and i < len(suffixes) - 1:
        nbytes /= 1024.0
        i += 1
    f = ("%.2f" % nbytes).rstrip("0").rstrip(".")
    return f"{f} {suffixes[i]}"


def git_show(format="fuller"):
    args = ["git", "show", "-s", f"--format={format}"]
    result = subprocess.run(args, capture_output=True, encoding="utf-8")
    return result.stdout


def git_rev_parse(target="HEAD", short=None):
    short = [] if not short else ["--short"] if short is True else [f"--short={short}"]
    args = ["git", "rev-parse"] + short + [target]
    result = subprocess.run(args, capture_output=True, encoding="utf-8")
    return result.stdout


def type_from_image_name(name):
    basename, sep, ext = name.rpartition(".")
    if basename.endswith(".img"):
        return "Raw image"
    if basename.endswith(".vdi"):
        return "VirtualBox VDI"


def build_images(pattern="*.gz", directory="Build/images/"):
    images = []
    for path in Path(directory).glob(pattern):
        image = {
            "name": path.name,
            "size": path.stat().st_size,
            "humansize": humansize(path.stat().st_size),
            "type": type_from_image_name(path.name),
        }
        images.append(image)
    return images


def build_summary():
    summary = {
        "commit_full": git_show().strip(),
        "commit_short": git_show(format="reference").strip(),
        "commit_hash": git_rev_parse().strip(),
        "images": build_images(),
    }
    return summary


def main(arguments):
    summary = build_summary()
    output = json.dumps(summary, indent=2)
    print(output)


if __name__ == "__main__":
    sys.exit(main(sys.argv))
