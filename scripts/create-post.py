#!/usr/bin/env python
import datetime
import json
import sys
from datetime import timezone
from pathlib import Path

import yaml
from jinja2 import Environment, FileSystemLoader, select_autoescape

now = datetime.datetime.now(timezone.utc)
env = Environment(
    loader=FileSystemLoader("templates"),
    autoescape=select_autoescape(),
    trim_blocks=True,
)
env.globals["builds_url"] = "https://serenity-files.halves.dev/builds/"


def str_presenter(dumper, data):
    """Nicer way to represent strings with multiple lines"""
    if len(data.splitlines()) > 1:
        return dumper.represent_scalar("tag:yaml.org,2002:str", data, style="|")
    return dumper.represent_scalar("tag:yaml.org,2002:str", data)


def datetime_presenter(dumper, data):
    """Improve datetime representation"""
    value = data.isoformat(timespec="seconds").replace("+00:00", "Z")
    return dumper.represent_scalar("tag:yaml.org,2002:timestamp", value)


yaml.add_representer(str, str_presenter)
yaml.add_representer(datetime.datetime, datetime_presenter)


def create_description(summary):
    template = env.get_template("post-preview.md.jinja")
    return template.render(summary)


def create_frontmatter(summary):
    params = {
        "title": "SerenityOS build: " + now.strftime("%A, %B %d"),
        "date": now,
        "author": "Buildbot",
        "description": create_description(summary),
    }
    return yaml.dump(params, sort_keys=False)


def create_post(summary):
    variables = dict(summary)
    variables["frontmatter"] = create_frontmatter(summary)
    template = env.get_template("post.md.jinja")
    return template.render(variables)


def main(arguments):
    path = Path(arguments[1] if len(arguments) >= 2 else "serenity/build-summary.json")
    with path.open() as f:
        summary = json.load(f)
    output = create_post(summary)
    print(output)


if __name__ == "__main__":
    sys.exit(main(sys.argv))
