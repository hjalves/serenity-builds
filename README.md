# Serenity Builds

Unofficial nightly builds of the [Serenity Operating System](https://serenityos.org/).

## About

This is a website that provides regular builds of the SerenityOS, compiled from the
[source code](https://github.com/SerenityOS/serenity) master branch.

Currently, only x86-64 images are being generated, with GRUB bootloader.
There should be more configurations in the future.
