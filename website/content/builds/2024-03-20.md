---
title: 'SerenityOS build: Wednesday, March 20'
date: 2024-03-20T06:16:58Z
author: Buildbot
description: |
  Commit: 0374c1eb3b (LibPDF: Handle indirect reference resolving during parsing more robustly, 2024-03-19)

  - [serenity-x86_64-20240320-0374c1e.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240320-0374c1e.img.gz) (Raw image, 220.22 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240320-0374c1e.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240320-0374c1e.img.gz) (Raw image, 220.22 MiB)

### Last commit :star:

```git
commit 0374c1eb3b5a4f69c05c9debe494b3770b398c3c
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Tue Mar 19 08:40:38 2024 -0400
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Tue Mar 19 19:20:01 2024 -0400

    LibPDF: Handle indirect reference resolving during parsing more robustly
    
    If `Document::resolve()` was called during parsing, it'd change the
    reader's current position, so the parsing code that called it would
    then end up at an unexpected position in the file.
    
    Parser.cpp already had special-case recovery when a stream's length
    was stored in an indirect reference.
    
    Commit ead02da98ac70c ("/JBIG2Globals") in #23503 added another case
    where we could resolve indirect reference during parsing, but wasn't
    aware of having to save and restore the reader position for that.
    
    Put the save/restore code in `DocumentParser::parse_object_with_index`
    instead, right before the place that ultimately changes the reader's
    position during `Document::resolve`. This fixes `/JBIG2Globals` and
    lets us remove the special-case code for `/Length` handling.
    
    Since this is kind of subtle, include a test.
```
