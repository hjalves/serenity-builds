---
title: 'SerenityOS build: Thursday, January 05'
date: 2023-01-05T05:07:43Z
author: Buildbot
description: |
  Commit: a7806d410a (Kernel: Convert 2 instances of `dbgln` to `dmesgln_pci` in AC'97, 2023-01-05)

  - [serenity-x86_64-20230105-a7806d4.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230105-a7806d4.img.gz) (Raw image, 660.81 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230105-a7806d4.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230105-a7806d4.img.gz) (Raw image, 660.81 MiB)

### Last commit :star:

```git
commit a7806d410a1c66e9e8ff824cad60d328f24d95a3
Author:     Jelle Raaijmakers <jelle@gmta.nl>
AuthorDate: Thu Jan 5 01:50:35 2023 +0100
Commit:     Jelle Raaijmakers <jelle@gmta.nl>
CommitDate: Thu Jan 5 01:50:35 2023 +0100

    Kernel: Convert 2 instances of `dbgln` to `dmesgln_pci` in AC'97
```
