---
title: 'SerenityOS build: Monday, February 05'
date: 2024-02-05T06:12:32Z
author: Buildbot
description: |
  Commit: ef9bfe3499 (Ports: Update poppler to 24.02.0, 2024-02-04)

  - [serenity-x86_64-20240205-ef9bfe3.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240205-ef9bfe3.img.gz) (Raw image, 214.23 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240205-ef9bfe3.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240205-ef9bfe3.img.gz) (Raw image, 214.23 MiB)

### Last commit :star:

```git
commit ef9bfe3499eccba59d727cb38af8513d43454eb4
Author:     Fabian Dellwing <fabian@dellwing.net>
AuthorDate: Sun Feb 4 13:21:11 2024 +0100
Commit:     Jelle Raaijmakers <jelle@gmta.nl>
CommitDate: Sun Feb 4 23:58:52 2024 +0100

    Ports: Update poppler to 24.02.0
```
