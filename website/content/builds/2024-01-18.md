---
title: 'SerenityOS build: Thursday, January 18'
date: 2024-01-18T06:12:30Z
author: Buildbot
description: |
  Commit: d186582d30 (LibAudio: Avoid UAF when loading WAV metadata, 2024-01-17)

  - [serenity-x86_64-20240118-d186582.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240118-d186582.img.gz) (Raw image, 210.1 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240118-d186582.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240118-d186582.img.gz) (Raw image, 210.1 MiB)

### Last commit :star:

```git
commit d186582d30a123e4f884c8d9a31c0387e019b076
Author:     Tim Ledbetter <timledbetter@gmail.com>
AuthorDate: Wed Jan 17 19:39:29 2024 +0000
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Wed Jan 17 16:09:59 2024 -0500

    LibAudio: Avoid UAF when loading WAV metadata
```
