---
title: 'SerenityOS build: Friday, December 22'
date: 2023-12-22T06:12:07Z
author: Buildbot
description: |
  Commit: e89163911c (Ladybird/AppKit: Use correct flag name for WebContent debugging, 2023-12-21)

  - [serenity-x86_64-20231222-e891639.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231222-e891639.img.gz) (Raw image, 206.81 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231222-e891639.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231222-e891639.img.gz) (Raw image, 206.81 MiB)

### Last commit :star:

```git
commit e89163911c1181af733a420b47075c4cecedb9fb
Author:     Andrew Kaster <akaster@serenityos.org>
AuthorDate: Thu Dec 21 22:56:47 2023 -0500
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Thu Dec 21 22:56:47 2023 -0500

    Ladybird/AppKit: Use correct flag name for WebContent debugging
    
    This fixes a CoPilot SNAFU from 1e68e484cc9c6b9c4c99e87b1fdd92dc58249b69
    where I tested the changes on Linux and assumed CI would catch any macOS
    problems.
```
