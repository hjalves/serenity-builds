---
title: 'SerenityOS build: Friday, May 05'
date: 2023-05-05T05:04:22Z
author: Buildbot
description: |
  Commit: 0318ac5ce4 (LibWeb: Remove setting length to 0px if it is not definite, 2023-05-04)

  - [serenity-x86_64-20230505-0318ac5.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230505-0318ac5.img.gz) (Raw image, 181.01 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230505-0318ac5.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230505-0318ac5.img.gz) (Raw image, 181.01 MiB)

### Last commit :star:

```git
commit 0318ac5ce450bb286a2b0fdd7a2d8aa90f6cf342
Author:     Aliaksandr Kalenik <kalenik.aliaksandr@gmail.com>
AuthorDate: Thu May 4 23:39:13 2023 +0300
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Fri May 5 06:20:44 2023 +0200

    LibWeb: Remove setting length to 0px if it is not definite
    
    If available width (or height) is max-content and width (or height)
    value is 100% it should be resolved in infinite px, not 0 px.
    
    Fixes #18639
```
