---
title: 'SerenityOS build: Thursday, March 23'
date: 2023-03-23T06:03:06Z
author: Buildbot
description: |
  Commit: d005b1ad1b (LibWeb: Support loading file:// URLs via fetch (through ResourceLoader), 2023-03-22)

  - [serenity-x86_64-20230323-d005b1a.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230323-d005b1a.img.gz) (Raw image, 168.72 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230323-d005b1a.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230323-d005b1a.img.gz) (Raw image, 168.72 MiB)

### Last commit :star:

```git
commit d005b1ad1b085fc1758854084609a9006c6df7f9
Author:     Andreas Kling <kling@serenityos.org>
AuthorDate: Wed Mar 22 23:56:11 2023 +0100
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Wed Mar 22 23:34:32 2023 +0000

    LibWeb: Support loading file:// URLs via fetch (through ResourceLoader)
    
    This builds on the existing ad-hoc ResourceLoader code for HTTP fetches
    which works for files as well.
    
    This also includes a test that checks that stylesheets loaded with the
    "file" URL scheme actually work.
```
