---
title: 'SerenityOS build: Sunday, September 03'
date: 2023-09-03T05:10:40Z
author: Buildbot
description: |
  Commit: cd3c06dcef (Ports: Add liblzf, 2023-08-25)

  - [serenity-x86_64-20230903-cd3c06d.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230903-cd3c06d.img.gz) (Raw image, 186.42 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230903-cd3c06d.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230903-cd3c06d.img.gz) (Raw image, 186.42 MiB)

### Last commit :star:

```git
commit cd3c06dcef82d941bb2adfa3b8460d305b29ea1b
Author:     Liav A <liavalb@gmail.com>
AuthorDate: Fri Aug 25 22:39:30 2023 +0300
Commit:     Tim Schumacher <timschumi@gmx.de>
CommitDate: Sun Sep 3 06:30:36 2023 +0200

    Ports: Add liblzf
    
    This small data compression library also provides userspace utilities to
    compress and decompress data.
```
