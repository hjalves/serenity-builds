---
title: 'SerenityOS build: Monday, October 14'
date: 2024-10-14T05:25:02Z
author: Buildbot
description: |
  Commit: f23a1c6e97 (AK: Use fast exponentiation for pow(), 2024-09-10)

  - [serenity-x86_64-20241014-f23a1c6.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241014-f23a1c6.img.gz) (Raw image, 224.38 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20241014-f23a1c6.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241014-f23a1c6.img.gz) (Raw image, 224.38 MiB)

### Last commit :star:

```git
commit f23a1c6e9791284d2af025e126bcca48310a16c3
Author:     kleines Filmröllchen <filmroellchen@serenityos.org>
AuthorDate: Tue Sep 10 00:03:04 2024 +0200
Commit:     Tim Schumacher <timschumi@gmx.de>
CommitDate: Sun Oct 13 03:44:49 2024 +0200

    AK: Use fast exponentiation for pow()
    
    Whatever we were doing beforehand is *really*
    slow.
```
