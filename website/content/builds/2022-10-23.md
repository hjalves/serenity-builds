---
title: 'SerenityOS build: Sunday, October 23'
date: 2022-10-23T04:01:18Z
author: Buildbot
description: |
  Commit: e2ebc8826f (Base: Add 25 emojis, 2022-10-21)

  - [serenity-x86_64-20221023-e2ebc88.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221023-e2ebc88.vdi.gz) (VirtualBox VDI, 136.01 MiB)
  - [serenity-x86_64-20221023-e2ebc88.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221023-e2ebc88.img.gz) (Raw image, 136.33 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221023-e2ebc88.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221023-e2ebc88.vdi.gz) (VirtualBox VDI, 136.01 MiB)
- [serenity-x86_64-20221023-e2ebc88.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221023-e2ebc88.img.gz) (Raw image, 136.33 MiB)

### Last commit :star:

```git
commit e2ebc8826f1be24c4fb75695090e6d15bc8f272e
Author:     electrikmilk <brandonjordan124@gmail.com>
AuthorDate: Fri Oct 21 13:44:49 2022 -0400
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Sun Oct 23 00:44:06 2022 +0200

    Base: Add 25 emojis
    
    🫁 - U+1FAC1 LUNGS
    🧟 - U+1F9DF ZOMBIE
    🪷 - U+1FAB7 LOTUS
    🫙 - U+1FAD9 JAR
    🚖 - U+1F696 ONCOMING TAXI
    🚢 - U+1F6A2 SHIP
    🧸 - U+1F9F8 TEDDY BEAR
    🪗 - U+1FA97 ACCORDION
    ↗️ - U+2197 UP-RIGHT ARROW
    ↘️ - U+2198 DOWN-RIGHT ARROW
    ↙️ - U+2199 DOWN-LEFT ARROW
    ↖️ - U+2196 UP-LEFT ARROW
    ↕️ - U+2195 UP-DOWN ARROW
    ↔️ - U+2194 LEFT-RIGHT ARROW
    🪯 - U+1FAAF KHANDA
    ⭕ - U+2B55 HOLLOW RED CIRCLE
    ➰ - U+27B0 CURLY LOOP
    ✳️ - U+2733 EIGHT-SPOKED ASTERISK
    Ⓜ️ - U+24C2 CIRCLED M
    ◼️ - U+25FC BLACK MEDIUM SQUARE
    ◻️ - U+25FB WHITE MEDIUM SQUARE
    ◾ - U+25FE BLACK MEDIUM-SMALL SQUARE
    ◽ - U+25FD WHITE MEDIUM-SMALL SQUARE
    ▪️ - U+25AA BLACK SMALL SQUARE
    ▫️ - U+25AB WHITE SMALL SQUARE
```
