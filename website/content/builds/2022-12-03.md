---
title: 'SerenityOS build: Saturday, December 03'
date: 2022-12-03T03:57:52Z
author: Buildbot
description: |
  Commit: 1aa07d7328 (AK: Implement FloatExtractor<f128>, 2022-11-25)

  - [serenity-x86_64-20221203-1aa07d7.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221203-1aa07d7.vdi.gz) (VirtualBox VDI, 142.04 MiB)
  - [serenity-x86_64-20221203-1aa07d7.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221203-1aa07d7.img.gz) (Raw image, 142.37 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221203-1aa07d7.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221203-1aa07d7.vdi.gz) (VirtualBox VDI, 142.04 MiB)
- [serenity-x86_64-20221203-1aa07d7.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221203-1aa07d7.img.gz) (Raw image, 142.37 MiB)

### Last commit :star:

```git
commit 1aa07d7328a4f1eaee0ac773429090e1963d22fd
Author:     Steffen Rusitschka <rusi@gmx.de>
AuthorDate: Fri Nov 25 18:15:55 2022 +0100
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Fri Dec 2 16:22:51 2022 +0100

    AK: Implement FloatExtractor<f128>
    
    This patch adds support for 128-bit floating points in FloatExtractor.
    
    This is required to build SerenityOS on MacOS/aarch64. It might break
    building for Raspberry Pi.
```
