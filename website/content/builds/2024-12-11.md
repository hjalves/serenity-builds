---
title: 'SerenityOS build: Wednesday, December 11'
date: 2024-12-11T06:25:55Z
author: Buildbot
description: |
  Commit: a3af84c5e6 (Kernel/aarch64: Get RPi timer address and IRQ number from the devicetree, 2024-11-09)

  - [serenity-x86_64-20241211-a3af84c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241211-a3af84c.img.gz) (Raw image, 239.99 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20241211-a3af84c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241211-a3af84c.img.gz) (Raw image, 239.99 MiB)

### Last commit :star:

```git
commit a3af84c5e6b9bd21320c7099e1c91542be94d87f
Author:     Sönke Holz <sholz8530@gmail.com>
AuthorDate: Sat Nov 9 23:22:15 2024 +0100
Commit:     Sönke Holz <sholz8530@gmail.com>
CommitDate: Tue Dec 10 16:25:46 2024 +0100

    Kernel/aarch64: Get RPi timer address and IRQ number from the devicetree
```
