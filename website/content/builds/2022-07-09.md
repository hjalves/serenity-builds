---
title: 'SerenityOS build: Saturday, July 09'
date: 2022-07-09T03:05:08Z
author: Buildbot
description: |
  Commit: b85666b3d2 (LibRegex: Fix lookup table-based range checks in Compare, 2022-07-07)

  - [serenity-i686-20220709-b85666b.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220709-b85666b.vdi.gz) (VirtualBox VDI, 124.92 MiB)
  - [serenity-i686-20220709-b85666b.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220709-b85666b.img.gz) (Raw image, 125.1 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220709-b85666b.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220709-b85666b.vdi.gz) (VirtualBox VDI, 124.92 MiB)
- [serenity-i686-20220709-b85666b.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220709-b85666b.img.gz) (Raw image, 125.1 MiB)

### Last commit :star:

```git
commit b85666b3d203bb39e75da8d582e428800f4cb26c
Author:     Ali Mohammad Pur <mpfard@serenityos.org>
AuthorDate: Thu Jul 7 23:27:51 2022 +0430
Commit:     Ali Mohammad Pur <mpfard@serenityos.org>
CommitDate: Sat Jul 9 01:00:44 2022 +0000

    LibRegex: Fix lookup table-based range checks in Compare
    
    The lowercase version of a range is not required to be a valid range,
    instead of casefolding the range and making it invalid, check twice with
    both cases of the input character (which are the same as the input if
    not insensitive).
    This time includes an actual test :^)
```
