---
title: 'SerenityOS build: Monday, July 24'
date: 2023-07-24T05:07:40Z
author: Buildbot
description: |
  Commit: ca1a98ba9f (LibPDF: Replace two more crashes with messages, 2023-07-23)

  - [serenity-x86_64-20230724-ca1a98b.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230724-ca1a98b.img.gz) (Raw image, 187.76 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230724-ca1a98b.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230724-ca1a98b.img.gz) (Raw image, 187.76 MiB)

### Last commit :star:

```git
commit ca1a98ba9f2d199fc7182e94b2da94aaf8f5d184
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Sun Jul 23 12:35:03 2023 -0400
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Sun Jul 23 23:05:32 2023 -0400

    LibPDF: Replace two more crashes with messages
```
