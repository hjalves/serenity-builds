---
title: 'SerenityOS build: Friday, November 29'
date: 2024-11-29T06:25:52Z
author: Buildbot
description: |
  Commit: a8e73f3d9f (LibWeb: Remove unnecessary use of TRY_OR_THROW_OOM in XMLHttpRequest, 2024-10-14)

  - [serenity-x86_64-20241129-a8e73f3.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241129-a8e73f3.img.gz) (Raw image, 229.91 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20241129-a8e73f3.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241129-a8e73f3.img.gz) (Raw image, 229.91 MiB)

### Last commit :star:

```git
commit a8e73f3d9f6f6df4a9aa1b11dcad65143f01d313
Author:     Andreas Kling <andreas@ladybird.org>
AuthorDate: Mon Oct 14 11:42:44 2024 +0200
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Thu Nov 28 22:42:14 2024 -0500

    LibWeb: Remove unnecessary use of TRY_OR_THROW_OOM in XMLHttpRequest
    
    (cherry picked from commit 279d229f71b52e1fd91100a74aa314c90db968da)
```
