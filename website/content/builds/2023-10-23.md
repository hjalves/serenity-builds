---
title: 'SerenityOS build: Monday, October 23'
date: 2023-10-23T05:08:53Z
author: Buildbot
description: |
  Commit: e108f394bf (LibGfx: Replace manual offsets when producing WOFF2 loca table, 2023-10-21)

  - [serenity-x86_64-20231023-e108f39.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231023-e108f39.img.gz) (Raw image, 190.15 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231023-e108f39.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231023-e108f39.img.gz) (Raw image, 190.15 MiB)

### Last commit :star:

```git
commit e108f394bff8ea4e13bbc49e8dca904cd4f9927d
Author:     Sam Atkins <atkinssj@serenityos.org>
AuthorDate: Sat Oct 21 14:40:47 2023 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Sun Oct 22 19:42:22 2023 +0200

    LibGfx: Replace manual offsets when producing WOFF2 loca table
```
