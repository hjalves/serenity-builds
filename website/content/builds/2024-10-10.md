---
title: 'SerenityOS build: Thursday, October 10'
date: 2024-10-10T05:25:20Z
author: Buildbot
description: |
  Commit: b01060c7d8 (Everywhere: Limit layout text fragments to use one font for all glyphs, 2024-06-29)

  - [serenity-x86_64-20241010-b01060c7.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241010-b01060c7.img.gz) (Raw image, 224.2 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20241010-b01060c7.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241010-b01060c7.img.gz) (Raw image, 224.2 MiB)

### Last commit :star:

```git
commit b01060c7d8b8b56b1fdc53792d3a37e13cc3f863
Author:     Aliaksandr Kalenik <kalenik.aliaksandr@gmail.com>
AuthorDate: Sat Jun 29 17:14:23 2024 +0200
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Wed Oct 9 20:12:39 2024 -0400

    Everywhere: Limit layout text fragments to use one font for all glyphs
    
    The ChunkIterator now limits a chunk to using only one font (before, it
    was possible to have a chunk with >1 font, when `unicode-range` CSS
    property is used).
    
    This change allows us to reduce some complexity in the text shaping and
    painting code and makes us compatible with the APIs in Skia and
    HarfBuzz.
    
    (cherry picked from commit 7181c3f2ea5fba73e77d98acbf9e46640b4a9015,
    minorly amended to fix conflicts caused by:
    * Our VectorFont not being renamed to Typeface
    * Us cherry-picking https://github.com/LadybirdBrowser/ladybird/pull/502
      first
    * Us still having bitmap fonts, and hence needing glyph_spacing()
    
    Also amended for:
    * AffineDisplayListPlayerCPU changes
    * Removing pure virtuals for glyph_id_for_code_point and
      glyph_id_for_code_point in Font.h again since we still have BitmapFont
      which can't implement them
    * Updating more Painter methods that we still had
      (Painter::draw_glyph_or_emoji(), Painter::draw_text_run())
    )
```
