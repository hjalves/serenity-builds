---
title: 'SerenityOS build: Friday, June 23'
date: 2023-06-23T05:04:28Z
author: Buildbot
description: |
  Commit: 702054dcbc (wc: Add `-L` option to show the length of the longest line, 2023-06-22)

  - [serenity-x86_64-20230623-702054d.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230623-702054d.img.gz) (Raw image, 184.7 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230623-702054d.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230623-702054d.img.gz) (Raw image, 184.7 MiB)

### Last commit :star:

```git
commit 702054dcbcb11502225dfa51c91e5637cbc609fc
Author:     Tim Ledbetter <timledbetter@gmail.com>
AuthorDate: Thu Jun 22 22:30:27 2023 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Fri Jun 23 06:24:33 2023 +0200

    wc: Add `-L` option to show the length of the longest line
    
    If more than one file is specified on the command line and the `-L`
    option is used, the totals field will show the longest line
    encountered; it is not a sum like the other values.
```
