---
title: 'SerenityOS build: Monday, July 03'
date: 2023-07-03T05:06:46Z
author: Buildbot
description: |
  Commit: e7da956090 (man: Change all uses of DeprecatedString to String, 2023-06-13)

  - [serenity-x86_64-20230703-e7da956.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230703-e7da956.img.gz) (Raw image, 185.92 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230703-e7da956.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230703-e7da956.img.gz) (Raw image, 185.92 MiB)

### Last commit :star:

```git
commit e7da9560905be23dc33b78ad963c6a337c83bbf4
Author:     Carwyn Nelson <carwyn@carwynnelson.com>
AuthorDate: Tue Jun 13 16:18:56 2023 +0100
Commit:     Jelle Raaijmakers <jelle@gmta.nl>
CommitDate: Mon Jul 3 02:05:32 2023 +0200

    man: Change all uses of DeprecatedString to String
```
