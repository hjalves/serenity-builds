---
title: 'SerenityOS build: Monday, March 25'
date: 2024-03-25T06:16:02Z
author: Buildbot
description: |
  Commit: c6899b79b6 (LibWeb: Normalize the angle delta in `CanvasPath::ellipse()`, 2024-03-24)

  - [serenity-x86_64-20240325-c6899b7.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240325-c6899b7.img.gz) (Raw image, 219.64 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240325-c6899b7.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240325-c6899b7.img.gz) (Raw image, 219.64 MiB)

### Last commit :star:

```git
commit c6899b79b609f99e8fd1091eb80ed5b2f430dde7
Author:     MacDue <macdue@dueutil.tech>
AuthorDate: Sun Mar 24 15:17:50 2024 +0000
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Sun Mar 24 18:37:44 2024 +0100

    LibWeb: Normalize the angle delta in `CanvasPath::ellipse()`
    
    This fixes both the incorrect arc and ellipse from #22817.
```
