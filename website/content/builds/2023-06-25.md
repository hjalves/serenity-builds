---
title: 'SerenityOS build: Sunday, June 25'
date: 2023-06-25T05:04:15Z
author: Buildbot
description: |
  Commit: cc1bd2d2d9 (LibGfx/JPEG: Use a look-up table for cosine values, 2023-06-23)

  - [serenity-x86_64-20230625-cc1bd2d.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230625-cc1bd2d.img.gz) (Raw image, 184.81 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230625-cc1bd2d.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230625-cc1bd2d.img.gz) (Raw image, 184.81 MiB)

### Last commit :star:

```git
commit cc1bd2d2d9f81e6c24d68c0d314ea7e965d08ff5
Author:     Lucas CHOLLET <lucas.chollet@free.fr>
AuthorDate: Fri Jun 23 19:01:03 2023 -0400
Commit:     Jelle Raaijmakers <jelle@gmta.nl>
CommitDate: Sun Jun 25 00:22:21 2023 +0200

    LibGfx/JPEG: Use a look-up table for cosine values
    
    This solution is a middle ground between re-computing `cos` every time
    and a much more mathematically complicated approach (as we have in the
    decoder).
    
    While still being far from optimal it already gives us a 10x
    improvement, not that bad :^)
    
    Co-authored-by: Tim Flynn <trflynn89@pm.me>
```
