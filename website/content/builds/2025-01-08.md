---
title: 'SerenityOS build: Wednesday, January 08'
date: 2025-01-08T06:23:26Z
author: Buildbot
description: |
  Commit: 801d7e2ced (LibGfx/JPEG2000: Add LRCP and RLCP progression iterators, 2025-01-05)

  - [serenity-x86_64-20250108-801d7e2.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250108-801d7e2.img.gz) (Raw image, 241.04 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20250108-801d7e2.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250108-801d7e2.img.gz) (Raw image, 241.04 MiB)

### Last commit :star:

```git
commit 801d7e2ced1f435631e6299d20eef6d99ec1e620
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Sun Jan 5 15:57:10 2025 -0500
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Tue Jan 7 08:23:28 2025 -0500

    LibGfx/JPEG2000: Add LRCP and RLCP progression iterators
```
