---
title: 'SerenityOS build: Monday, September 11'
date: 2023-09-11T05:09:55Z
author: Buildbot
description: |
  Commit: ec416563a8 (Maps: Zoom on doubleclick, 2023-09-05)

  - [serenity-x86_64-20230911-ec41656.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230911-ec41656.img.gz) (Raw image, 185.84 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230911-ec41656.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230911-ec41656.img.gz) (Raw image, 185.84 MiB)

### Last commit :star:

```git
commit ec416563a896f188072bac6e86a3234888a8f231
Author:     Jelle Raaijmakers <jelle@gmta.nl>
AuthorDate: Tue Sep 5 20:14:47 2023 +0200
Commit:     Jelle Raaijmakers <jelle@gmta.nl>
CommitDate: Sun Sep 10 22:45:42 2023 +0200

    Maps: Zoom on doubleclick
```
