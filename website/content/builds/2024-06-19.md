---
title: 'SerenityOS build: Wednesday, June 19'
date: 2024-06-19T05:17:53Z
author: Buildbot
description: |
  Commit: 1cc4d29d20 (LibGfx/WebPWriter: Use align_up_to(), 2024-06-16)

  - [serenity-x86_64-20240619-1cc4d29.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240619-1cc4d29.img.gz) (Raw image, 218.73 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240619-1cc4d29.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240619-1cc4d29.img.gz) (Raw image, 218.73 MiB)

### Last commit :star:

```git
commit 1cc4d29d20d45da7d45fc6230615e8fc3a611fab
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Sun Jun 16 13:07:04 2024 -0400
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Sun Jun 16 14:50:32 2024 -0400

    LibGfx/WebPWriter: Use align_up_to()
    
    No behavior change.
```
