---
title: 'SerenityOS build: Wednesday, October 04'
date: 2023-10-04T05:10:49Z
author: Buildbot
description: |
  Commit: 2e9a28272e (Kernel/Audio: Fail AC97 probe if no good BAR1 is found, 2023-07-22)

  - [serenity-x86_64-20231004-2e9a282.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231004-2e9a282.img.gz) (Raw image, 187.83 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231004-2e9a282.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231004-2e9a282.img.gz) (Raw image, 187.83 MiB)

### Last commit :star:

```git
commit 2e9a28272e790d36ee398ba7ab9934b82231ba18
Author:     Vladimir Serbinenko <phcoder@gmail.com>
AuthorDate: Sat Jul 22 20:45:59 2023 +0200
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Tue Oct 3 16:19:03 2023 -0600

    Kernel/Audio: Fail AC97 probe if no good BAR1 is found
    
    Otherwise we get a kernel panic later on Intel SOF.
```
