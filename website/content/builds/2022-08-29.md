---
title: 'SerenityOS build: Monday, August 29'
date: 2022-08-29T03:07:04Z
author: Buildbot
description: |
  Commit: 35c9aa7c05 (LibJS: Hide all the constructors!, 2022-08-28)

  - [serenity-i686-20220829-35c9aa7.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220829-35c9aa7.vdi.gz) (VirtualBox VDI, 129.72 MiB)
  - [serenity-i686-20220829-35c9aa7.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220829-35c9aa7.img.gz) (Raw image, 129.9 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220829-35c9aa7.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220829-35c9aa7.vdi.gz) (VirtualBox VDI, 129.72 MiB)
- [serenity-i686-20220829-35c9aa7.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220829-35c9aa7.img.gz) (Raw image, 129.9 MiB)

### Last commit :star:

```git
commit 35c9aa7c05c7d2b012e86cc72945ade3440ab0bf
Author:     Andreas Kling <kling@serenityos.org>
AuthorDate: Sun Aug 28 23:51:28 2022 +0200
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Mon Aug 29 03:24:54 2022 +0200

    LibJS: Hide all the constructors!
    
    Now that the GC allocator is able to invoke Cell subclass constructors
    directly via friendship, we no longer need to keep them public. :^)
```
