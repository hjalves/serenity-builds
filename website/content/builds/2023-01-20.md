---
title: 'SerenityOS build: Friday, January 20'
date: 2023-01-20T06:08:37Z
author: Buildbot
description: |
  Commit: 100c8f9bcf (CI: Add GitHub author presence check in commit linter, 2023-01-20)

  - [serenity-x86_64-20230120-100c8f9.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230120-100c8f9.img.gz) (Raw image, 680.75 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230120-100c8f9.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230120-100c8f9.img.gz) (Raw image, 680.75 MiB)

### Last commit :star:

```git
commit 100c8f9bcfd7c2e8096a7b2f8cc1b5da5227bf49
Author:     Jelle Raaijmakers <jelle@gmta.nl>
AuthorDate: Fri Jan 20 00:32:02 2023 +0100
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Thu Jan 19 23:48:57 2023 +0000

    CI: Add GitHub author presence check in commit linter
    
    If GitHub is unable to match a commit's author to a GitHub user, the
    `.author` object is set to `null` in the API's response.
```
