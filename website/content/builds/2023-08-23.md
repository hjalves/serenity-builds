---
title: 'SerenityOS build: Wednesday, August 23'
date: 2023-08-23T05:08:02Z
author: Buildbot
description: |
  Commit: ec340f03a5 (WebContent+LibWebView: Add support for user style sheets, 2023-08-21)

  - [serenity-x86_64-20230823-ec340f0.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230823-ec340f0.img.gz) (Raw image, 184.93 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230823-ec340f0.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230823-ec340f0.img.gz) (Raw image, 184.93 MiB)

### Last commit :star:

```git
commit ec340f03a54dcb0c00cfe1c80b25b23cb01237a3
Author:     Sam Atkins <atkinssj@serenityos.org>
AuthorDate: Mon Aug 21 15:50:31 2023 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Wed Aug 23 05:32:10 2023 +0200

    WebContent+LibWebView: Add support for user style sheets
```
