---
title: 'SerenityOS build: Monday, October 30'
date: 2023-10-30T06:09:05Z
author: Buildbot
description: |
  Commit: 246daa0810 (Meta: Port 7d26cbf52382a3dc8e808d12a763b9a87062b859 to gn build, 2023-10-29)

  - [serenity-x86_64-20231030-246daa0.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231030-246daa0.img.gz) (Raw image, 199.63 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231030-246daa0.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231030-246daa0.img.gz) (Raw image, 199.63 MiB)

### Last commit :star:

```git
commit 246daa08105e3c91bc59236b40870183780c7730
Author:     Andrew Kaster <akaster@serenityos.org>
AuthorDate: Sun Oct 29 16:04:57 2023 -0600
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Sun Oct 29 16:04:57 2023 -0600

    Meta: Port 7d26cbf52382a3dc8e808d12a763b9a87062b859 to gn build
```
