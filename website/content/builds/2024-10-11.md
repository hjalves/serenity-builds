---
title: 'SerenityOS build: Friday, October 11'
date: 2024-10-11T05:25:16Z
author: Buildbot
description: |
  Commit: 10dfaf1539 (Tests+headless-browser: Move screenshot ref-tests into own directory, 2024-07-19)

  - [serenity-x86_64-20241011-10dfaf1.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241011-10dfaf1.img.gz) (Raw image, 224.21 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20241011-10dfaf1.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241011-10dfaf1.img.gz) (Raw image, 224.21 MiB)

### Last commit :star:

```git
commit 10dfaf15395297da6474ceef8f7ad1de7da7cd87
Author:     Aliaksandr Kalenik <kalenik.aliaksandr@gmail.com>
AuthorDate: Fri Jul 19 12:45:52 2024 +0200
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Thu Oct 10 23:27:47 2024 -0400

    Tests+headless-browser: Move screenshot ref-tests into own directory
    
    This change will make it easier to disable screenshot comparison tests
    on a specific platform or have per-platform expectations.
    
    Additionally, it's nice to be able to tell if a ref-test uses a
    screenshot as an expectation by looking at the test path.
    
    (cherry picked from commit 715f0330070d9f8ecc88a8e66e4d8c85f795d622;
    amended to also move
    Tests/LibWeb/Screenshot/canvas-stroke-styles.html and support files)
```
