---
title: 'SerenityOS build: Saturday, March 18'
date: 2023-03-18T06:02:57Z
author: Buildbot
description: |
  Commit: 1dc074fc18 (LibWeb: Treat flex item's cyclic percentage cross size as auto, 2023-03-18)

  - [serenity-x86_64-20230318-1dc074f.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230318-1dc074f.img.gz) (Raw image, 167.62 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230318-1dc074f.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230318-1dc074f.img.gz) (Raw image, 167.62 MiB)

### Last commit :star:

```git
commit 1dc074fc18970915c08b0b9b5e07945c67bda7e4
Author:     Andreas Kling <kling@serenityos.org>
AuthorDate: Sat Mar 18 00:24:51 2023 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Sat Mar 18 00:26:19 2023 +0100

    LibWeb: Treat flex item's cyclic percentage cross size as auto
    
    This fixes an issue where e.g `height: 100%` on a flex item whose
    container has indefinite height was being resolved to 0. It now
    correctly behaves the same as auto.
```
