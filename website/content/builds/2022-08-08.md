---
title: 'SerenityOS build: Monday, August 08'
date: 2022-08-08T03:07:00Z
author: Buildbot
description: |
  Commit: a6505f6e6d (LibWeb: Allow % height of a % height parent in block-formatted elements, 2022-08-07)

  - [serenity-i686-20220808-a6505f6.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220808-a6505f6.vdi.gz) (VirtualBox VDI, 127.54 MiB)
  - [serenity-i686-20220808-a6505f6.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220808-a6505f6.img.gz) (Raw image, 127.72 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220808-a6505f6.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220808-a6505f6.vdi.gz) (VirtualBox VDI, 127.54 MiB)
- [serenity-i686-20220808-a6505f6.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220808-a6505f6.img.gz) (Raw image, 127.72 MiB)

### Last commit :star:

```git
commit a6505f6e6d8be7dc0136c7293f6da39ed98e27cb
Author:     MacDue <macdue@dueutil.tech>
AuthorDate: Sun Aug 7 20:22:55 2022 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Sun Aug 7 22:52:22 2022 +0200

    LibWeb: Allow % height of a % height parent in block-formatted elements
    
    With this you can start to see Francine's face in the CSS oil painting
    (https://diana-adrianne.com/purecss-francine/)
```
