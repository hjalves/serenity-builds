---
title: 'SerenityOS build: Sunday, November 19'
date: 2023-11-19T06:09:56Z
author: Buildbot
description: |
  Commit: 4e04f81626 (LibWeb: Don't encode painting limitations in RecordingPainter API, 2023-11-18)

  - [serenity-x86_64-20231119-4e04f81.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231119-4e04f81.img.gz) (Raw image, 203.69 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231119-4e04f81.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231119-4e04f81.img.gz) (Raw image, 203.69 MiB)

### Last commit :star:

```git
commit 4e04f81626cf3a5e1bea52ae26bb17f40695ad78
Author:     MacDue <macdue@dueutil.tech>
AuthorDate: Sat Nov 18 14:23:59 2023 +0000
Commit:     Alexander Kalenik <kalenik.aliaksandr@gmail.com>
CommitDate: Sat Nov 18 19:32:31 2023 +0100

    LibWeb: Don't encode painting limitations in RecordingPainter API
    
    The current set of stacking context commands do not encode the
    information needed to correctly paint the stacking context, instead,
    they're based on the limitations of the current CPU renderer.
    
    Stacking contexts should be able to be transformed by an arbitrary
    3D transformation matrix, not just scaled from a source to a destination
    rect. The `_with_mask()` stacking context also should not be separate
    from the regular stacking context.
    
    ```c++
    push_stacking_context(
            bool semitransparent_or_has_non_identity_transform,
            float opacity, Gfx::FloatRect const& source_rect,
            Gfx::FloatRect const& transformed_destination_rect,
            Gfx::IntPoint const& painter_location);
    
    pop_stacking_context(
            bool semitransparent_or_has_non_identity_transform,
            Gfx::Painter::ScalingMode scaling_mode);
    
    push_stacking_context_with_mask(
            Gfx::IntRect const& paint_rect);
    
    pop_stacking_context_with_mask(
            Gfx::IntRect const& paint_rect,
            RefPtr<Gfx::Bitmap> const& mask_bitmap,
            Gfx::Bitmap::MaskKind mask_kind, float opacity);
    ```
    
    This patch replaces this APIs with just:
    
    ```c++
    push_stacking_context(
            float opacity,
            bool is_fixed_position,
            Gfx::IntRect const& source_paintable_rect,
            Gfx::IntPoint post_transform_translation,
            CSS::ImageRendering image_rendering,
            StackingContextTransform transform,
            Optional<StackingContextMask> mask);
    
    pop_stacking_context()
    ```
    
    And moves the implementation details into the executor, this should
    allow future backends to implement stacking contexts without these
    limitations.
```
