---
title: 'SerenityOS build: Wednesday, November 15'
date: 2023-11-15T06:10:44Z
author: Buildbot
description: |
  Commit: 6b5a0100d9 (Browser: Move `BookmarksBarWidget::PerformEditOn` to anon namespace, 2023-11-14)

  - [serenity-x86_64-20231115-6b5a010.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231115-6b5a010.img.gz) (Raw image, 202.95 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231115-6b5a010.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231115-6b5a010.img.gz) (Raw image, 202.95 MiB)

### Last commit :star:

```git
commit 6b5a0100d9ef5a5934f6a2b806371baa478e3b7d
Author:     Kemal Zebari <kemalzebra@gmail.com>
AuthorDate: Tue Nov 14 12:43:45 2023 -0800
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Tue Nov 14 15:38:09 2023 -0700

    Browser: Move `BookmarksBarWidget::PerformEditOn` to anon namespace
    
    Since none of `BookmarksBarWidget`'s methods use this enum class any
    longer, let's just move it to the anonymous namespace so that
    `BookmarkEditor` can still make use of it.
```
