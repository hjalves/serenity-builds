---
title: 'SerenityOS build: Friday, October 20'
date: 2023-10-20T05:08:44Z
author: Buildbot
description: |
  Commit: 2f5ff14984 (LibWeb: Fix spec implementation mistakes in destroy_the_child_navigable, 2023-10-19)

  - [serenity-x86_64-20231020-2f5ff14.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231020-2f5ff14.img.gz) (Raw image, 190.07 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231020-2f5ff14.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231020-2f5ff14.img.gz) (Raw image, 190.07 MiB)

### Last commit :star:

```git
commit 2f5ff14984d2ddfd33c2ce234167c48ddf9ac5f8
Author:     Aliaksandr Kalenik <kalenik.aliaksandr@gmail.com>
AuthorDate: Thu Oct 19 18:26:06 2023 +0200
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Thu Oct 19 16:54:17 2023 -0400

    LibWeb: Fix spec implementation mistakes in destroy_the_child_navigable
    
    Fixes two places in child navigable destroy procedure where we used
    content navigable instead of container's navigable.
    
    With this change, iframe's nested histories are actually destroyed
    along with the document that created them.
```
