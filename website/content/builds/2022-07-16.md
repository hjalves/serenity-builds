---
title: 'SerenityOS build: Saturday, July 16'
date: 2022-07-16T03:06:09Z
author: Buildbot
description: |
  Commit: 864c7fb9f1 (LibJS: Update spec numbers from the array-find-from-last proposal merge, 2022-07-15)

  - [serenity-i686-20220716-864c7fb.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220716-864c7fb.vdi.gz) (VirtualBox VDI, 126.82 MiB)
  - [serenity-i686-20220716-864c7fb.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220716-864c7fb.img.gz) (Raw image, 126.98 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220716-864c7fb.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220716-864c7fb.vdi.gz) (VirtualBox VDI, 126.82 MiB)
- [serenity-i686-20220716-864c7fb.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220716-864c7fb.img.gz) (Raw image, 126.98 MiB)

### Last commit :star:

```git
commit 864c7fb9f16dadc6bc0d8c2e41f428051bc4f58e
Author:     Timothy Flynn <trflynn89@pm.me>
AuthorDate: Fri Jul 15 08:06:50 2022 -0400
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Fri Jul 15 14:14:00 2022 +0100

    LibJS: Update spec numbers from the array-find-from-last proposal merge
    
    See: https://github.com/tc39/ecma262/commit/5f31dd6
```
