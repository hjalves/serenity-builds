---
title: 'SerenityOS build: Saturday, August 26'
date: 2023-08-26T05:09:46Z
author: Buildbot
description: |
  Commit: aae7905369 (Applications: Use native style sheet for WebViews where appropriate, 2023-08-24)

  - [serenity-x86_64-20230826-aae7905.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230826-aae7905.img.gz) (Raw image, 185.64 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230826-aae7905.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230826-aae7905.img.gz) (Raw image, 185.64 MiB)

### Last commit :star:

```git
commit aae7905369bbd81b53624af353a4cb4b44525bc8
Author:     Sam Atkins <atkinssj@serenityos.org>
AuthorDate: Thu Aug 24 11:07:14 2023 +0100
Commit:     Sam Atkins <atkinssj@serenityos.org>
CommitDate: Fri Aug 25 20:30:20 2023 +0100

    Applications: Use native style sheet for WebViews where appropriate
```
