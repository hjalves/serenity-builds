---
title: 'SerenityOS build: Sunday, February 23'
date: 2025-02-23T06:23:13Z
author: Buildbot
description: |
  Commit: ae54ed458a (Kernel/x86: Remove `gdt64ptr` and `code64_sel`, 2025-02-21)

  - [serenity-x86_64-20250223-ae54ed4.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250223-ae54ed4.img.gz) (Raw image, 242.06 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20250223-ae54ed4.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250223-ae54ed4.img.gz) (Raw image, 242.06 MiB)

### Last commit :star:

```git
commit ae54ed458afd478ab83767453812255a9976d988
Author:     Sönke Holz <sholz8530@gmail.com>
AuthorDate: Fri Feb 21 15:07:44 2025 +0100
Commit:     Sönke Holz <sholz8530@gmail.com>
CommitDate: Sat Feb 22 13:46:13 2025 +0100

    Kernel/x86: Remove `gdt64ptr` and `code64_sel`
    
    The AP setup code no longer uses the Prekernel GDT, so these variables
    and arch-specific boot info members are unnecessary.
```
