---
title: 'SerenityOS build: Sunday, February 26'
date: 2023-02-26T05:59:54Z
author: Buildbot
description: |
  Commit: 1c3050245e (LibDSP: Don't crash on out-of-bounds parameter value, 2023-02-09)

  - [serenity-x86_64-20230226-1c30502.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230226-1c30502.img.gz) (Raw image, 163.87 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230226-1c30502.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230226-1c30502.img.gz) (Raw image, 163.87 MiB)

### Last commit :star:

```git
commit 1c3050245ea7660bec230c397ca1046bdf083bd2
Author:     kleines Filmröllchen <filmroellchen@serenityos.org>
AuthorDate: Thu Feb 9 15:36:30 2023 +0100
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Sat Feb 25 20:49:41 2023 -0700

    LibDSP: Don't crash on out-of-bounds parameter value
    
    We can just clamp instead.
```
