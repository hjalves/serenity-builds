---
title: 'SerenityOS build: Saturday, December 17'
date: 2022-12-17T03:58:26Z
author: Buildbot
description: |
  Commit: 0dd8066a79 (tsort: Suppress warnings with -q, 2022-12-10)

  - [serenity-x86_64-20221217-0dd8066.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221217-0dd8066.vdi.gz) (VirtualBox VDI, 144.07 MiB)
  - [serenity-x86_64-20221217-0dd8066.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221217-0dd8066.img.gz) (Raw image, 144.37 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221217-0dd8066.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221217-0dd8066.vdi.gz) (VirtualBox VDI, 144.07 MiB)
- [serenity-x86_64-20221217-0dd8066.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221217-0dd8066.img.gz) (Raw image, 144.37 MiB)

### Last commit :star:

```git
commit 0dd8066a79d3ef41eb9af526e1f346673d4ceb32
Author:     Eli Youngs <eli.m.youngs@gmail.com>
AuthorDate: Sat Dec 10 18:29:31 2022 -0800
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Fri Dec 16 10:41:56 2022 -0700

    tsort: Suppress warnings with -q
```
