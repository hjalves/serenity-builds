---
title: 'SerenityOS build: Saturday, May 13'
date: 2023-05-13T05:03:26Z
author: Buildbot
description: |
  Commit: 4ef997c47c (LibWeb: Align calculate_min/max_content_contribution with the spec, 2023-05-13)

  - [serenity-x86_64-20230513-4ef997c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230513-4ef997c.img.gz) (Raw image, 181.68 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230513-4ef997c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230513-4ef997c.img.gz) (Raw image, 181.68 MiB)

### Last commit :star:

```git
commit 4ef997c47cfdad150ee97eb5ba10c7204d7ada63
Author:     Aliaksandr Kalenik <kalenik.aliaksandr@gmail.com>
AuthorDate: Sat May 13 05:51:39 2023 +0300
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Sat May 13 06:29:42 2023 +0200

    LibWeb: Align calculate_min/max_content_contribution with the spec
    
    This change brings more spec compliant implementation of functions to
    calculate min/max contributions of grid items in containing block size.
```
