---
title: 'SerenityOS build: Friday, June 02'
date: 2023-06-02T05:04:20Z
author: Buildbot
description: |
  Commit: 2ef6aa5f3d (LibWeb: Parse `clamp()` css math function, 2023-05-26)

  - [serenity-x86_64-20230602-2ef6aa5.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230602-2ef6aa5.img.gz) (Raw image, 184.09 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230602-2ef6aa5.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230602-2ef6aa5.img.gz) (Raw image, 184.09 MiB)

### Last commit :star:

```git
commit 2ef6aa5f3db3e7526f338723358d210ed61be87e
Author:     stelar7 <dudedbz@gmail.com>
AuthorDate: Fri May 26 15:40:39 2023 +0200
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Fri Jun 2 05:22:12 2023 +0200

    LibWeb: Parse `clamp()` css math function
```
