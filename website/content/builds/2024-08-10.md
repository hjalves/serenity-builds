---
title: 'SerenityOS build: Saturday, August 10'
date: 2024-08-10T05:21:39Z
author: Buildbot
description: |
  Commit: 832b5ff603 (AK: Add `simd_cast<T>` and replace `to_TxN` with it, 2024-08-06)

  - [serenity-x86_64-20240810-832b5ff.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240810-832b5ff.img.gz) (Raw image, 221.4 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240810-832b5ff.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240810-832b5ff.img.gz) (Raw image, 221.4 MiB)

### Last commit :star:

```git
commit 832b5ff6035fd04413ce8dcb0c2e7ef7c57a3c17
Author:     Hendiadyoin1 <leon.a@serenityos.org>
AuthorDate: Tue Aug 6 14:35:38 2024 +0200
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Thu Aug 8 22:43:53 2024 -0400

    AK: Add `simd_cast<T>` and replace `to_TxN` with it
```
