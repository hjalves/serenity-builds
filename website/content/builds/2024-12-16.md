---
title: 'SerenityOS build: Monday, December 16'
date: 2024-12-16T06:25:56Z
author: Buildbot
description: |
  Commit: 7bcf97c8e8 (Kernel/aarch64: Implement `microseconds_delay`, 2024-12-14)

  - [serenity-x86_64-20241216-7bcf97c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241216-7bcf97c.img.gz) (Raw image, 239.99 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20241216-7bcf97c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241216-7bcf97c.img.gz) (Raw image, 239.99 MiB)

### Last commit :star:

```git
commit 7bcf97c8e8586b78768d9e546bd6954d10f98f1b
Author:     Sönke Holz <sholz8530@gmail.com>
AuthorDate: Sat Dec 14 19:09:06 2024 +0100
Commit:     Sönke Holz <sholz8530@gmail.com>
CommitDate: Sun Dec 15 15:26:40 2024 +0100

    Kernel/aarch64: Implement `microseconds_delay`
    
    This simple delay loop uses the EL1 virtual timer to wait for the given
    amount of time.
```
