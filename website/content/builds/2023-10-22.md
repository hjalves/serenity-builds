---
title: 'SerenityOS build: Sunday, October 22'
date: 2023-10-22T05:08:54Z
author: Buildbot
description: |
  Commit: 2745b48e16 (Shell: Don't try to cast NonnullRefPtrs when priting debug output, 2023-10-21)

  - [serenity-x86_64-20231022-2745b48.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231022-2745b48.img.gz) (Raw image, 190.2 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231022-2745b48.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231022-2745b48.img.gz) (Raw image, 190.2 MiB)

### Last commit :star:

```git
commit 2745b48e1692c9575e02490df8a57d8b945851fa
Author:     implicitfield <114500360+implicitfield@users.noreply.github.com>
AuthorDate: Sat Oct 21 22:21:49 2023 +0400
Commit:     Ali Mohammad Pur <mpfard@serenityos.org>
CommitDate: Sun Oct 22 02:02:35 2023 +0330

    Shell: Don't try to cast NonnullRefPtrs when priting debug output
    
    Fixes a regression from 8a48246e.
```
