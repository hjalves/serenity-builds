---
title: 'SerenityOS build: Friday, December 06'
date: 2024-12-06T06:25:44Z
author: Buildbot
description: |
  Commit: 46522b2efa (Utilities/sed: Add tests for the transform command & comments, 2024-12-03)

  - [serenity-x86_64-20241206-46522b2.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241206-46522b2.img.gz) (Raw image, 239.68 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20241206-46522b2.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241206-46522b2.img.gz) (Raw image, 239.68 MiB)

### Last commit :star:

```git
commit 46522b2efa0b693b9090ebbf47d813dd8a554215
Author:     Ninad Sachania <ninad.sachania@gmail.com>
AuthorDate: Tue Dec 3 17:09:56 2024 +0000
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Thu Dec 5 10:08:43 2024 -0500

    Utilities/sed: Add tests for the transform command & comments
```
