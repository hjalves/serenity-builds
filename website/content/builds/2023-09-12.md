---
title: 'SerenityOS build: Tuesday, September 12'
date: 2023-09-12T08:26:40Z
author: Buildbot
description: |
  Commit: 9240233378 (Revert "AK: Refill a BufferedStream when it has less than the, 2023-09-12)

  - [serenity-x86_64-20230912-9240233.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230912-9240233.img.gz) (Raw image, 185.86 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230912-9240233.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230912-9240233.img.gz) (Raw image, 185.86 MiB)

### Last commit :star:

```git
commit 924023337840da849603e73e666f6e1028dbf9ac
Author:     Jelle Raaijmakers <jelle@gmta.nl>
AuthorDate: Tue Sep 12 00:01:06 2023 +0200
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Mon Sep 11 21:38:24 2023 -0400

    Revert "AK: Refill a BufferedStream when it has less than the
    
    ...requested size"
    
    This reverts commit 13573a6c4bc0311f91371fd4563bc930d4811682.
    
    Some clients of `BufferedStream` expect a non-blocking read by
    `read_some` which the commit above made impossible by potentially
    performing a blocking read. For example, the following command hangs:
    
        pro http://ipecho.net/plain
    
    This is caused by the HTTP job expecting to read the body of the
    response which is included in the active buffer, but since the buffered
    data size is less than the buffer passed into `read_some`, another
    blocking read is performed before anything is returned.
    
    By reverting this commit, all tests still pass and `pro` no longer
    hangs. Note that because of another bug related to `stdout` / `stderr`
    mixing and the absence of a line ending, there is no output to the
    command above except a progress update.
```
