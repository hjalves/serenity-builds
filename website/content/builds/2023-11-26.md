---
title: 'SerenityOS build: Sunday, November 26'
date: 2023-11-26T06:11:45Z
author: Buildbot
description: |
  Commit: 743a9e9ebf (Kernel: Stop including the ethernet header in LoopbackAdapter's mtu, 2023-11-25)

  - [serenity-x86_64-20231126-743a9e9.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231126-743a9e9.img.gz) (Raw image, 205.01 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231126-743a9e9.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231126-743a9e9.img.gz) (Raw image, 205.01 MiB)

### Last commit :star:

```git
commit 743a9e9ebf61ac0f2f164376c906946d8dd7e3ea
Author:     Idan Horowitz <idan.horowitz@gmail.com>
AuthorDate: Sat Nov 25 16:38:19 2023 +0200
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Sat Nov 25 16:34:38 2023 +0100

    Kernel: Stop including the ethernet header in LoopbackAdapter's mtu
    
    The networking subsystem currently assumes all adapters are Ethernet
    adapters, including the LoopbackAdapter, so all packets are pre-pended
    with an Ethernet Frame header. Since the MTU must not include any
    overhead added by the data-link (Ethernet in this case) or physical
    layers, we need to subtract it from the MTU.
    
    This fixes a kernel panic which occurs when sending a packet that is at
    least 65523 bytes long through the loopback adapter, which results in
    the kernel "receiving" a packet which is larger than the support MTU
    out the other end. (As the actual final size was increased by the
    addition of the ethernet frame header)
```
