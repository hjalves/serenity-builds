---
title: 'SerenityOS build: Saturday, February 15'
date: 2025-02-15T06:23:34Z
author: Buildbot
description: |
  Commit: 3b796c6326 (Meta: Update the OSS-Fuzz badge URL, 2025-02-14)

  - [serenity-x86_64-20250215-3b796c6.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250215-3b796c6.img.gz) (Raw image, 241.98 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20250215-3b796c6.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250215-3b796c6.img.gz) (Raw image, 241.98 MiB)

### Last commit :star:

```git
commit 3b796c63265b84672748dea003f5543bad59280d
Author:     Sönke Holz <sholz8530@gmail.com>
AuthorDate: Fri Feb 14 22:00:24 2025 +0100
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Fri Feb 14 18:13:03 2025 -0500

    Meta: Update the OSS-Fuzz badge URL
    
    The old link no longer works.
```
