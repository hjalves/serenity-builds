---
title: 'SerenityOS build: Friday, November 04'
date: 2022-11-04T04:00:35Z
author: Buildbot
description: |
  Commit: 59e87cc998 (AK: Add static_assert to check for effective size of long double, 2022-11-03)

  - [serenity-x86_64-20221104-59e87cc.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221104-59e87cc.img.gz) (Raw image, 139.03 MiB)
  - [serenity-x86_64-20221104-59e87cc.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221104-59e87cc.vdi.gz) (VirtualBox VDI, 138.72 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221104-59e87cc.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221104-59e87cc.img.gz) (Raw image, 139.03 MiB)
- [serenity-x86_64-20221104-59e87cc.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221104-59e87cc.vdi.gz) (VirtualBox VDI, 138.72 MiB)

### Last commit :star:

```git
commit 59e87cc9985d90bca7141eb49f9d57e534b677de
Author:     Dan Klishch <danilklishch@gmail.com>
AuthorDate: Thu Nov 3 18:37:37 2022 -0400
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Thu Nov 3 20:17:09 2022 -0600

    AK: Add static_assert to check for effective size of long double
```
