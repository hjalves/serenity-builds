---
title: 'SerenityOS build: Sunday, June 18'
date: 2023-06-18T05:04:52Z
author: Buildbot
description: |
  Commit: a910c4d984 (LibWeb: Fix end position for objectBoundingBox SVG <radialGradient>s, 2023-06-17)

  - [serenity-x86_64-20230618-a910c4d.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230618-a910c4d.img.gz) (Raw image, 184.58 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230618-a910c4d.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230618-a910c4d.img.gz) (Raw image, 184.58 MiB)

### Last commit :star:

```git
commit a910c4d984559d704cec8b16971b85d1ecbe00d7
Author:     MacDue <macdue@dueutil.tech>
AuthorDate: Sat Jun 17 19:25:54 2023 +0100
Commit:     Sam Atkins <atkinssj@serenityos.org>
CommitDate: Sat Jun 17 22:25:26 2023 +0100

    LibWeb: Fix end position for objectBoundingBox SVG <radialGradient>s
    
    The translation to the bounding box location is handled by the gradient
    transform, also doing it here breaks things.
    
    This fixes the MDN <radialGradient> example.
```
