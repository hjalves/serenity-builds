---
title: 'SerenityOS build: Wednesday, August 16'
date: 2023-08-16T05:07:25Z
author: Buildbot
description: |
  Commit: c7f416682b (LibAudio: Write final FLAC audio data instead of discarding it, 2023-08-13)

  - [serenity-x86_64-20230816-c7f4166.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230816-c7f4166.img.gz) (Raw image, 184.31 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230816-c7f4166.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230816-c7f4166.img.gz) (Raw image, 184.31 MiB)

### Last commit :star:

```git
commit c7f416682b290fee62fab1d57d7d3bf9ae504ad0
Author:     kleines Filmröllchen <filmroellchen@serenityos.org>
AuthorDate: Sun Aug 13 14:16:33 2023 +0200
Commit:     Jelle Raaijmakers <jelle@gmta.nl>
CommitDate: Wed Aug 16 01:10:35 2023 +0200

    LibAudio: Write final FLAC audio data instead of discarding it
```
