---
title: 'SerenityOS build: Friday, February 02'
date: 2024-02-02T06:13:07Z
author: Buildbot
description: |
  Commit: d60758a947 (LibGfx: Remove now-unused Color::from_cmyk(), 2024-01-30)

  - [serenity-x86_64-20240202-d60758a.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240202-d60758a.img.gz) (Raw image, 214.06 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240202-d60758a.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240202-d60758a.img.gz) (Raw image, 214.06 MiB)

### Last commit :star:

```git
commit d60758a947e31794e08cd11949f857a0ea544a01
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Tue Jan 30 21:30:01 2024 -0500
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Thu Feb 1 13:42:04 2024 -0700

    LibGfx: Remove now-unused Color::from_cmyk()
    
    `CMYKBitmap::to_low_quality_rgb()` morally still does the same thing,
    but it has a slightly more scary name, and it doesn't use this exact
    function. So let's toss it :^)
```
