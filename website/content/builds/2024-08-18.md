---
title: 'SerenityOS build: Sunday, August 18'
date: 2024-08-18T05:21:59Z
author: Buildbot
description: |
  Commit: c10853ca89 (LibGfx/PNGWriter: Change filter heuristic to match comment, 2024-08-15)

  - [serenity-x86_64-20240818-c10853c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240818-c10853c.img.gz) (Raw image, 221.29 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240818-c10853c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240818-c10853c.img.gz) (Raw image, 221.29 MiB)

### Last commit :star:

```git
commit c10853ca89fac4b29f03bfc16ec7d4ddae803b5e
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Thu Aug 15 08:06:35 2024 -0400
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Sat Aug 17 11:03:29 2024 -0400

    LibGfx/PNGWriter: Change filter heuristic to match comment
    
    We were computing abs(sum(signed_pixels)), while the comment says
    that sum(abs(signed_pixels)) works well. Change the code to match
    the comment. (While I did tweak this code recently, as far as I can
    tell the code hasn't matched the comment from when it was originally
    added.)
    
    Using the benchmarking script and same inputs as in #24819, just changed
    to write .png files with different --png-compression-level values:
    
        level 0: 390M -> 390M (no change)
        level 1:  83M ->  78M (6% smaller)
        level 2:  73M ->  69M (5.8% smaller at default compression level)
        level 3:  71M ->  67M (5.6% smaller)
    
    Sizes as computed by `du -hAs`. (`-A` prints apparent size on macOS.
    It's important to use that flag to get consistent results. On Linux,
    this flag is spelled `-b` instead.)
    
    The size of sunset_retro.png goes up a tiny bit, but but less than
    0.4% at all sizes. At level 2, the size goes from 908K to 911K, for
    example.
    
    The size of Tests/LibGfx/test-inputs/jpg/big_image.jpg encoded as PNG
    goes down by about 2.7%, but it's the 2.7% that gets us over an MB
    boundary at levels 1 and 2. At level 1, from 14M to 13M; at level 2
    from 13M to 12M. (Exact numbers: 14417809 bytes to 13429605 at level 1,
    14076443 bytes to 13088791 at level 2.) For comparison, sips writes a
    15M (15610049 bytes) file. So we were already writing a smaller file,
    and now we're even better. (We need 778 ms at level 1 while
     sips needs 723ms. So it's a bit faster, but not a ton.)
    
    The size of wow.apng goes from 606K to 584K (3.6% smaller).
    
    Perf-wise, this is close to a wash. Worst case, it's maybe 2-3% slower,
    but the numbers are pretty noisy, even with many repetitions in
    `hyperfine`. I'm guessing `ministat` would claim that no significant
    difference can be shown, but I haven't tried it. For example, for
    sunset_retro.png at level 2, time goes from 179.3 ms ± 2.5 ms to
    182.8 ms ± 1.9 ms, which would be a 2% slowdown. At level 0, where
    the effect is relatively largest, it goes from 21.8 ms ± 0.7 ms to
    22.6 ms ± 0.7 ms, a 3.6% slowdown (but with huge error bars).
    For big_image.jpg level 1, time goes from 768.5 ms ± 8.4 ms to
    777.9 ms ± 6.0 ms, 1.2% slower.
```
