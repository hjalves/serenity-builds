---
title: 'SerenityOS build: Sunday, December 17'
date: 2023-12-17T06:10:40Z
author: Buildbot
description: |
  Commit: 36f0499cc8 (LibLine: Use grapheme clusters for cursor management, 2023-12-15)

  - [serenity-x86_64-20231217-36f0499.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231217-36f0499.img.gz) (Raw image, 206.56 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231217-36f0499.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231217-36f0499.img.gz) (Raw image, 206.56 MiB)

### Last commit :star:

```git
commit 36f0499cc84ccfcb2d1867b683ec732d247a6827
Author:     Ali Mohammad Pur <mpfard@serenityos.org>
AuthorDate: Fri Dec 15 15:01:58 2023 +0330
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Sat Dec 16 22:11:43 2023 +0100

    LibLine: Use grapheme clusters for cursor management
    
    This makes using the line editor much nicer when multi-code-point
    graphemes are present in the input (e.g. flag emojis, or some cjk
    glyphs), and avoids messing up the buffer when deleting text, or
    cursoring around.
```
