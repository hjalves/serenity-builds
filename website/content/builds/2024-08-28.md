---
title: 'SerenityOS build: Wednesday, August 28'
date: 2024-08-28T05:22:11Z
author: Buildbot
description: |
  Commit: a9a63c0f7c (CI: Build riscv64 in CI, 2024-08-25)

  - [serenity-x86_64-20240828-a9a63c0.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240828-a9a63c0.img.gz) (Raw image, 221.46 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240828-a9a63c0.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240828-a9a63c0.img.gz) (Raw image, 221.46 MiB)

### Last commit :star:

```git
commit a9a63c0f7cdfd712822d2736cabf33f7b21d2ea7
Author:     Sönke Holz <sholz8530@gmail.com>
AuthorDate: Sun Aug 25 21:28:22 2024 +0200
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Tue Aug 27 12:03:49 2024 +0200

    CI: Build riscv64 in CI
    
    Don't run any tests on riscv64 for now, since some tests still fail
    (mostly due to missing generic AK/Math.h implementations).
```
