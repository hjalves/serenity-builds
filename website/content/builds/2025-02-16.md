---
title: 'SerenityOS build: Sunday, February 16'
date: 2025-02-16T06:22:59Z
author: Buildbot
description: |
  Commit: 73805a0ba7 (LibPDF: Pass the EncodedByteAligned option to the Group4 decoder, 2025-02-15)

  - [serenity-x86_64-20250216-73805a0.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250216-73805a0.img.gz) (Raw image, 242.03 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20250216-73805a0.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250216-73805a0.img.gz) (Raw image, 242.03 MiB)

### Last commit :star:

```git
commit 73805a0ba76874c6646264639cdfea25c8902519
Author:     Lucas CHOLLET <lucas.chollet@free.fr>
AuthorDate: Sat Feb 15 20:03:44 2025 -0500
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Sat Feb 15 23:28:37 2025 -0500

    LibPDF: Pass the EncodedByteAligned option to the Group4 decoder
```
