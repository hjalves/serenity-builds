---
title: 'SerenityOS build: Friday, February 10'
date: 2023-02-10T06:06:34Z
author: Buildbot
description: |
  Commit: d32961777b (Spreadsheet: Don't recalculate column sizes when no data has changed, 2023-02-09)

  - [serenity-x86_64-20230210-d329617.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230210-d329617.img.gz) (Raw image, 158.53 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230210-d329617.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230210-d329617.img.gz) (Raw image, 158.53 MiB)

### Last commit :star:

```git
commit d32961777bcea26ed7ec96936f53cb080da3fb75
Author:     Tim Ledbetter <timledbetter@gmail.com>
AuthorDate: Thu Feb 9 19:38:50 2023 +0000
Commit:     Ali Mohammad Pur <mpfard@serenityos.org>
CommitDate: Fri Feb 10 05:12:06 2023 +0330

    Spreadsheet: Don't recalculate column sizes when no data has changed
    
    Use the Model::UpdateFlag::DontResizeColumns option when performing a
    model update where no data has been changed. This improves navigation
    performance on large spreadsheets.
```
