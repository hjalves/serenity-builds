---
title: 'SerenityOS build: Tuesday, December 24'
date: 2024-12-24T06:25:52Z
author: Buildbot
description: |
  Commit: 555d2ac5ef (Kernel/aarch64: Don't print the register state twice for crashes, 2024-12-23)

  - [serenity-x86_64-20241224-555d2ac.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241224-555d2ac.img.gz) (Raw image, 240.86 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20241224-555d2ac.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241224-555d2ac.img.gz) (Raw image, 240.86 MiB)

### Last commit :star:

```git
commit 555d2ac5ef2a47ad9e8510eb23388cb3ec3688d1
Author:     Sönke Holz <sholz8530@gmail.com>
AuthorDate: Mon Dec 23 21:23:25 2024 +0100
Commit:     Sönke Holz <sholz8530@gmail.com>
CommitDate: Tue Dec 24 01:12:26 2024 +0100

    Kernel/aarch64: Don't print the register state twice for crashes
    
    `handle_crash` already calls `dump_registers`, so there is no need for
    the AArch64 exception handler to do it as well.
    
    The second register dump (the one made by `handle_crash`) previously
    also often reported an incorrect ESR_EL1 value, as interrupts are
    enabled at that point in time.
    This problem can simply be solved by saving the ESR_EL1 value in the
    `RegisterState`, similar to how we save `scause` on RISC-V.
```
