---
title: 'SerenityOS build: Wednesday, December 07'
date: 2022-12-07T03:58:27Z
author: Buildbot
description: |
  Commit: daec065fde (LibJS: Move initialize_instance_elements() from VM to Object, 2022-12-07)

  - [serenity-x86_64-20221207-daec065.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221207-daec065.vdi.gz) (VirtualBox VDI, 142.47 MiB)
  - [serenity-x86_64-20221207-daec065.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221207-daec065.img.gz) (Raw image, 142.75 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221207-daec065.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221207-daec065.vdi.gz) (VirtualBox VDI, 142.47 MiB)
- [serenity-x86_64-20221207-daec065.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221207-daec065.img.gz) (Raw image, 142.75 MiB)

### Last commit :star:

```git
commit daec065fdea58840444e61ccfdb71bf6514fa6d5
Author:     Linus Groh <mail@linusgroh.de>
AuthorDate: Wed Dec 7 00:22:23 2022 +0000
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Wed Dec 7 00:23:51 2022 +0000

    LibJS: Move initialize_instance_elements() from VM to Object
    
    This makes more sense as an Object method rather than living within the
    VM class for no good reason. Most of the other 7.3.xx AOs already work
    the same way.
    Also add spec comments while we're here.
```
