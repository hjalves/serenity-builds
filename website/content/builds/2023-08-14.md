---
title: 'SerenityOS build: Monday, August 14'
date: 2023-08-14T05:07:09Z
author: Buildbot
description: |
  Commit: 582784d0cf (AK: Remove unused ApplyPercentDecoding enum from URL class, 2023-08-13)

  - [serenity-x86_64-20230814-582784d.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230814-582784d.img.gz) (Raw image, 184.34 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230814-582784d.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230814-582784d.img.gz) (Raw image, 184.34 MiB)

### Last commit :star:

```git
commit 582784d0cf9a1a06499ace22a0d5cf11b3dfa29a
Author:     Shannon Booth <shannon@serenityos.org>
AuthorDate: Sun Aug 13 08:49:04 2023 +1200
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Sun Aug 13 15:03:53 2023 -0600

    AK: Remove unused ApplyPercentDecoding enum from URL class
```
