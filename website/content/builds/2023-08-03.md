---
title: 'SerenityOS build: Thursday, August 03'
date: 2023-08-03T05:07:35Z
author: Buildbot
description: |
  Commit: 73fa58da34 (LibWeb: Implement the CSS `outline-offset` property, 2023-08-02)

  - [serenity-x86_64-20230803-73fa58d.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230803-73fa58d.img.gz) (Raw image, 188.55 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230803-73fa58d.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230803-73fa58d.img.gz) (Raw image, 188.55 MiB)

### Last commit :star:

```git
commit 73fa58da341260f819448e5652519e50d2653aff
Author:     Sam Atkins <atkinssj@serenityos.org>
AuthorDate: Wed Aug 2 20:09:10 2023 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Thu Aug 3 05:25:48 2023 +0200

    LibWeb: Implement the CSS `outline-offset` property
    
    This allows you to push the outline a certain distance away from the
    border (or inside it, if the offset is negative).
```
