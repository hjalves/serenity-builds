---
title: 'SerenityOS build: Saturday, September 10'
date: 2022-09-10T05:04:33Z
author: Buildbot
description: |
  Commit: d4736d17ae (LibJS: Allow negative pointers in Value, 2022-09-07)

  - [serenity-i686-20220910-d4736d1.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220910-d4736d1.vdi.gz) (VirtualBox VDI, 127.26 MiB)
  - [serenity-i686-20220910-d4736d1.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220910-d4736d1.img.gz) (Raw image, 127.43 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220910-d4736d1.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220910-d4736d1.vdi.gz) (VirtualBox VDI, 127.26 MiB)
- [serenity-i686-20220910-d4736d1.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220910-d4736d1.img.gz) (Raw image, 127.43 MiB)

### Last commit :star:

```git
commit d4736d17aed3ea0f856c1237986d553a6616c6ca
Author:     davidot <davidot@serenityos.org>
AuthorDate: Wed Sep 7 01:14:23 2022 +0200
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Sat Sep 10 00:05:32 2022 +0100

    LibJS: Allow negative pointers in Value
    
    Also ensure that all a nullptr input gives null object and you don't
    accidentally dereference a nullptr.
```
