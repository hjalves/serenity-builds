---
title: 'SerenityOS build: Monday, November 25'
date: 2024-11-25T06:25:11Z
author: Buildbot
description: |
  Commit: 1322fed4c8 (LibWeb+WebContent: Do not include DOM HTML in text test expectations, 2024-10-02)

  - [serenity-x86_64-20241125-1322fed.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241125-1322fed.img.gz) (Raw image, 229.44 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20241125-1322fed.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241125-1322fed.img.gz) (Raw image, 229.44 MiB)

### Last commit :star:

```git
commit 1322fed4c84e6683bf591fa90eec968e41fa2a56
Author:     Timothy Flynn <trflynn89@pm.me>
AuthorDate: Wed Oct 2 12:38:10 2024 -0400
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Sun Nov 24 22:42:15 2024 -0500

    LibWeb+WebContent: Do not include DOM HTML in text test expectations
    
    For example, in the following abbreviated test HTML:
    
        <span>some text</span>
        <script>println("whf")</script>
    
    We would have to craft the expectation file to include the "some text"
    segment, usually with some leading whitespace. This is a bit annoying,
    and makes it difficult to manually craft expectation files.
    
    So instead of comparing the expectation against the entire DOM inner
    text, we now send the inner text of just the <pre> element containing
    the test output when we invoke `internals.signalTextTestIsDone`.
    
    (cherry picked from commit bf668696de9a68bbcd6adfeaac809a475da015dc;
    amended a bit but not as much as one might think -- see PR for details)
```
