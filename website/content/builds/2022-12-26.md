---
title: 'SerenityOS build: Monday, December 26'
date: 2022-12-26T03:58:14Z
author: Buildbot
description: |
  Commit: ca134e8bfa (Base: Mention pledge promise for jail-specific syscalls, 2022-12-25)

  - [serenity-x86_64-20221226-ca134e8.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221226-ca134e8.img.gz) (Raw image, 145.12 MiB)
  - [serenity-x86_64-20221226-ca134e8.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221226-ca134e8.vdi.gz) (VirtualBox VDI, 144.78 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221226-ca134e8.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221226-ca134e8.img.gz) (Raw image, 145.12 MiB)
- [serenity-x86_64-20221226-ca134e8.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221226-ca134e8.vdi.gz) (VirtualBox VDI, 144.78 MiB)

### Last commit :star:

```git
commit ca134e8bfa9c26bd5bb9aab2bd044e4d6f759ae4
Author:     Liav A <liavalb@gmail.com>
AuthorDate: Sun Dec 25 19:09:09 2022 +0200
Commit:     Ali Mohammad Pur <mpfard@serenityos.org>
CommitDate: Mon Dec 26 04:59:54 2022 +0330

    Base: Mention pledge promise for jail-specific syscalls
```
