---
title: 'SerenityOS build: Wednesday, June 21'
date: 2023-06-21T05:04:30Z
author: Buildbot
description: |
  Commit: e5685078c1 (Meta: Add a gdb pretty-printer for `Optional`, 2023-06-20)

  - [serenity-x86_64-20230621-e568507.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230621-e568507.img.gz) (Raw image, 184.69 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230621-e568507.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230621-e568507.img.gz) (Raw image, 184.69 MiB)

### Last commit :star:

```git
commit e5685078c113754835153c897cb7e5aac475c1cf
Author:     Lucas CHOLLET <lucas.chollet@free.fr>
AuthorDate: Tue Jun 20 17:36:57 2023 -0400
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Wed Jun 21 06:15:35 2023 +0200

    Meta: Add a gdb pretty-printer for `Optional`
```
