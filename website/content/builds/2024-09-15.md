---
title: 'SerenityOS build: Sunday, September 15'
date: 2024-09-15T05:22:11Z
author: Buildbot
description: |
  Commit: a3077203fe (AK: Don't implicitly convert Optional<T&> to Optional<T>, 2024-09-10)

  - [serenity-x86_64-20240915-a307720.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240915-a307720.img.gz) (Raw image, 221.53 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240915-a307720.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240915-a307720.img.gz) (Raw image, 221.53 MiB)

### Last commit :star:

```git
commit a3077203fe64e39b4f7ae39dfb35a517342df462
Author:     kleines Filmröllchen <filmroellchen@serenityos.org>
AuthorDate: Tue Sep 10 21:16:49 2024 +0200
Commit:     Tim Schumacher <timschumi@gmx.de>
CommitDate: Sat Sep 14 13:30:27 2024 +0200

    AK: Don't implicitly convert Optional<T&> to Optional<T>
    
    C++ will jovially select the implicit conversion operator, even if it's
    complete bogus, such as for unknown-size types or non-destructible
    types. Therefore, all such conversions (which incur a copy) must
    (unfortunately) be explicit so that non-copyable types continue to work.
```
