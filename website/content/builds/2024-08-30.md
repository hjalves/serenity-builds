---
title: 'SerenityOS build: Friday, August 30'
date: 2024-08-30T05:22:10Z
author: Buildbot
description: |
  Commit: 500ff2a7ed (AK: Declare AK::Coroutine<T>::CoroutinePromiseBase::unhandled_exception, 2024-08-28)

  - [serenity-x86_64-20240830-500ff2a.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240830-500ff2a.img.gz) (Raw image, 221.48 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240830-500ff2a.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240830-500ff2a.img.gz) (Raw image, 221.48 MiB)

### Last commit :star:

```git
commit 500ff2a7ed69b9b22f9915881409314afd274b8e
Author:     Dan Klishch <danilklishch@gmail.com>
AuthorDate: Wed Aug 28 20:33:44 2024 -0400
Commit:     Ali Mohammad Pur <mpfard@serenityos.org>
CommitDate: Thu Aug 29 13:28:42 2024 +0200

    AK: Declare AK::Coroutine<T>::CoroutinePromiseBase::unhandled_exception
    
    For some reason, Clang wants AK to work with exceptions enabled so much
    that it produces a very annoying warning complaining about the absence
    of (completely useless in our setup) unhandled_exception method in
    promise type for every coroutine declared. We work around this during
    build by suppressing -Wcoroutine-missing-unhandled-exception in
    Meta/CMake/common_compile_options.cmake. However, the flag is only added
    if build is using Clang. If one builds coroutine code with GCC but still
    uses clangd, clangd, obviously, doesn't pick up warning suppression and
    annoys user with a squiggly yellow line under each coroutine function
    declaration.
```
