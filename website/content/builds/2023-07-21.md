---
title: 'SerenityOS build: Friday, July 21'
date: 2023-07-21T05:06:55Z
author: Buildbot
description: |
  Commit: fb94415f03 (LibJS: Delete Declaration::for_each_var_declared_name, 2023-07-20)

  - [serenity-x86_64-20230721-fb94415.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230721-fb94415.img.gz) (Raw image, 187.9 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230721-fb94415.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230721-fb94415.img.gz) (Raw image, 187.9 MiB)

### Last commit :star:

```git
commit fb94415f037816acf59fb44f945bb80bff511c99
Author:     Aliaksandr Kalenik <kalenik.aliaksandr@gmail.com>
AuthorDate: Thu Jul 20 17:00:43 2023 +0200
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Thu Jul 20 20:19:15 2023 +0200

    LibJS: Delete Declaration::for_each_var_declared_name
    
    1. Replaces for_each_var_declared_name usage with more generic
    for_each_var_declared_identifier.
    2. Deletes for_each_var_declared_name.
```
