---
title: 'SerenityOS build: Wednesday, May 15'
date: 2024-05-15T05:18:06Z
author: Buildbot
description: |
  Commit: c8e4499b08 (LibJS: Make return control flow more static, 2024-05-12)

  - [serenity-x86_64-20240515-c8e4499.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240515-c8e4499.img.gz) (Raw image, 215.72 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240515-c8e4499.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240515-c8e4499.img.gz) (Raw image, 215.72 MiB)

### Last commit :star:

```git
commit c8e4499b087b2562dce90f97bf48fa2cc86fb102
Author:     Hendiadyoin1 <leon.a@serenityos.org>
AuthorDate: Sun May 12 11:03:26 2024 +0200
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Wed May 15 04:25:14 2024 +0200

    LibJS: Make return control flow more static
    
    With this only `ContinuePendingUnwind` needs to dynamically check if a
    scheduled return needs to go through a `finally` block, making the
    interpreter loop a bit nicer
```
