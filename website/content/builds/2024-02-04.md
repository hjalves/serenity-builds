---
title: 'SerenityOS build: Sunday, February 04'
date: 2024-02-04T06:12:42Z
author: Buildbot
description: |
  Commit: 36cd2fb7c5 (Ladybird+WebContent: Update IPC calls to handle multiple traversables, 2024-02-02)

  - [serenity-x86_64-20240204-36cd2fb.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240204-36cd2fb.img.gz) (Raw image, 214.23 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240204-36cd2fb.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240204-36cd2fb.img.gz) (Raw image, 214.23 MiB)

### Last commit :star:

```git
commit 36cd2fb7c5d4099bc9f2ce6d46e02ee221e8dd7c
Author:     Andrew Kaster <akaster@serenityos.org>
AuthorDate: Fri Feb 2 18:00:48 2024 -0700
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Sat Feb 3 20:51:37 2024 -0500

    Ladybird+WebContent: Update IPC calls to handle multiple traversables
    
    The IPC layer between chromes and LibWeb now understands that multiple
    top level traversables can live in each WebContent process.
    
    This largely mechanical change adds a billion page_id/page_index
    arguments to make sure that pages that end up opening new WebViews
    through mechanisms like window.open() still work properly with those
    extra windows.
```
