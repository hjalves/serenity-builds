---
title: 'SerenityOS build: Saturday, November 18'
date: 2023-11-18T06:10:15Z
author: Buildbot
description: |
  Commit: bfe27228a3 (LibPDF+LibGfx: Don't invert CMYK channels in JPEG data in PDFs, 2023-11-15)

  - [serenity-x86_64-20231118-bfe2722.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231118-bfe2722.img.gz) (Raw image, 203.28 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231118-bfe2722.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231118-bfe2722.img.gz) (Raw image, 203.28 MiB)

### Last commit :star:

```git
commit bfe27228a317c138286bf8b60a8416ad3ae54e0b
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Wed Nov 15 12:58:06 2023 -0500
Commit:     Sam Atkins <atkinssj@serenityos.org>
CommitDate: Fri Nov 17 22:32:40 2023 +0000

    LibPDF+LibGfx: Don't invert CMYK channels in JPEG data in PDFs
    
    This is a hack: Ideally we'd have a CMYK Bitmap pixel format,
    and we'd convert to rgb at blit time. Then we could also apply color
    profiles (which for CMYK images are CMYK-based).
    
    Also, the colors for our CMYK->RGB conversion are off for PDFs,
    and we have distinct codepaths for this in Gfx::Color (for paths)
    and JPEGs. So when we fix that, we'll have to fix it in two places.
    
    But this doesn't require a lot of code and it's a huge visual
    progression, so let's go with it for now.
```
