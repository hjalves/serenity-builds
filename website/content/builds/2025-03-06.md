---
title: 'SerenityOS build: Thursday, March 06'
date: 2025-03-06T06:23:08Z
author: Buildbot
description: |
  Commit: c2f66b8508 (Kernel/x86: Replace 2 ACPI PDF spec references with URLs in IOAPIC.cpp, 2025-03-05)

  - [serenity-x86_64-20250306-c2f66b8.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250306-c2f66b8.img.gz) (Raw image, 242.53 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20250306-c2f66b8.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250306-c2f66b8.img.gz) (Raw image, 242.53 MiB)

### Last commit :star:

```git
commit c2f66b8508a4f72eb9bcf5a69424818ddc608b85
Author:     Sönke Holz <sholz8530@gmail.com>
AuthorDate: Wed Mar 5 11:34:22 2025 +0100
Commit:     Sönke Holz <sholz8530@gmail.com>
CommitDate: Wed Mar 5 17:38:15 2025 +0100

    Kernel/x86: Replace 2 ACPI PDF spec references with URLs in IOAPIC.cpp
```
