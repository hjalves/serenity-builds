---
title: 'SerenityOS build: Wednesday, November 01'
date: 2023-11-01T06:09:27Z
author: Buildbot
description: |
  Commit: 4676b288a2 (LibWeb: Set table width to GRIDMAX if calculated value is max-content, 2023-10-31)

  - [serenity-x86_64-20231101-4676b28.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231101-4676b28.img.gz) (Raw image, 199.87 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231101-4676b28.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231101-4676b28.img.gz) (Raw image, 199.87 MiB)

### Last commit :star:

```git
commit 4676b288a213a729c05d913a1ef39aa7b3f92c15
Author:     Aliaksandr Kalenik <kalenik.aliaksandr@gmail.com>
AuthorDate: Tue Oct 31 17:41:41 2023 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Tue Oct 31 18:13:14 2023 +0100

    LibWeb: Set table width to GRIDMAX if calculated value is max-content
    
    If the width of the table container is specified as max-content, then
    it seems sensible to resolve it as the sum of the maximum widths of the
    columns.
```
