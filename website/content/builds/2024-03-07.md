---
title: 'SerenityOS build: Thursday, March 07'
date: 2024-03-07T06:14:32Z
author: Buildbot
description: |
  Commit: 096ddb0021 (Ladybird: Include Userland/ in for Applications that use LibWeb, 2024-03-06)

  - [serenity-x86_64-20240307-096ddb0.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240307-096ddb0.img.gz) (Raw image, 218.45 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240307-096ddb0.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240307-096ddb0.img.gz) (Raw image, 218.45 MiB)

### Last commit :star:

```git
commit 096ddb0021152cb2eb839bf8c1efa3bc00b221c4
Author:     Andrew Kaster <akaster@serenityos.org>
AuthorDate: Wed Mar 6 17:10:16 2024 -0700
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Wed Mar 6 19:50:00 2024 -0500

    Ladybird: Include Userland/ in for Applications that use LibWeb
    
    After ea682207d099654dd0c5d1ed399e2060c4de1533, we need Userland/
    included directly in these application executables. This only impacts
    the build with Ladybird/CMakeLists.txt as the top level CMakeLists, as
    the Lagom/ directory includes Userland/ globally.
```
