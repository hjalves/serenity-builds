---
title: 'SerenityOS build: Tuesday, January 09'
date: 2024-01-09T06:13:36Z
author: Buildbot
description: |
  Commit: 00b24a55b1 (LibWeb: Fix drawing axis-aligned lines, 2024-01-08)

  - [serenity-x86_64-20240109-00b24a5.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240109-00b24a5.img.gz) (Raw image, 209.08 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240109-00b24a5.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240109-00b24a5.img.gz) (Raw image, 209.08 MiB)

### Last commit :star:

```git
commit 00b24a55b1897f97b430e5c05c7b730d4cc092e5
Author:     MacDue <macdue@dueutil.tech>
AuthorDate: Mon Jan 8 22:26:37 2024 +0000
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Tue Jan 9 00:03:09 2024 +0100

    LibWeb: Fix drawing axis-aligned lines
    
    Previously, these were clipped by the RecordingPainter, which used the
    path's bounding box (which in this case is zero width or height). The
    fix is to expand the bounding box by the stroke width.
    
    Fixes #22661
    
    Note: This is unrelated to any recent LibGfx changes :^)
```
