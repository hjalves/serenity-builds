---
title: 'SerenityOS build: Friday, September 01'
date: 2023-09-01T05:08:03Z
author: Buildbot
description: |
  Commit: 0adda642a6 (LibWeb: Fix int parsing in Element::tab_index, 2023-08-30)

  - [serenity-x86_64-20230901-0adda64.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230901-0adda64.img.gz) (Raw image, 186.19 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230901-0adda64.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230901-0adda64.img.gz) (Raw image, 186.19 MiB)

### Last commit :star:

```git
commit 0adda642a680567d76607ab2f57ac7e1ef4e55fa
Author:     Jonatan Klemets <jonatan.r.klemets@gmail.com>
AuthorDate: Wed Aug 30 20:43:25 2023 +0300
Commit:     Luke Wilde <25595356+Lubrsi@users.noreply.github.com>
CommitDate: Thu Aug 31 22:27:48 2023 +0100

    LibWeb: Fix int parsing in Element::tab_index
    
    We now have functions for parsing integers in a spec-compliant way. This
    patch replaces the `to_int` call with a call to the spec-compliant
    `Web::HTML::parse_integer` function.
```
