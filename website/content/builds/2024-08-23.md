---
title: 'SerenityOS build: Friday, August 23'
date: 2024-08-23T05:22:25Z
author: Buildbot
description: |
  Commit: 32a8ef390b (LibCrypto: Make dummy authenticated text length 16 bytes in GCM, 2024-08-21)

  - [serenity-x86_64-20240823-32a8ef3.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240823-32a8ef3.img.gz) (Raw image, 221.45 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240823-32a8ef3.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240823-32a8ef3.img.gz) (Raw image, 221.45 MiB)

### Last commit :star:

```git
commit 32a8ef390b9d020ac992219e09aa8caaa5242856
Author:     Dan Klishch <danilklishch@gmail.com>
AuthorDate: Wed Aug 21 21:54:44 2024 -0400
Commit:     Ali Mohammad Pur <mpfard@serenityos.org>
CommitDate: Thu Aug 22 11:55:11 2024 +0200

    LibCrypto: Make dummy authenticated text length 16 bytes in GCM
    
    Some callers of this API needlessly provide very large output buffer
    even when input is small. Since we throw away all authentication-related
    information here anyway, let's make the buffer (that gets
    authenticated!) as small as possible.
```
