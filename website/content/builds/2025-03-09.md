---
title: 'SerenityOS build: Sunday, March 09'
date: 2025-03-09T06:23:10Z
author: Buildbot
description: |
  Commit: 50b4c358ce (Ports: Update ninja to 1.12.1, 2025-03-08)

  - [serenity-x86_64-20250309-50b4c35.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250309-50b4c35.img.gz) (Raw image, 242.55 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20250309-50b4c35.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250309-50b4c35.img.gz) (Raw image, 242.55 MiB)

### Last commit :star:

```git
commit 50b4c358ce72ad7f326020d5b138ce09b24e097d
Author:     Linus Groh <mail@linusgroh.de>
AuthorDate: Sat Mar 8 21:56:06 2025 +0000
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Sat Mar 8 18:53:28 2025 -0500

    Ports: Update ninja to 1.12.1
```
