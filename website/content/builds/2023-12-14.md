---
title: 'SerenityOS build: Thursday, December 14'
date: 2023-12-14T06:11:16Z
author: Buildbot
description: |
  Commit: c145d5410c (Userland: Don't leak objects when constructor arguments throw, 2023-12-13)

  - [serenity-x86_64-20231214-c145d54.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231214-c145d54.img.gz) (Raw image, 206.37 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231214-c145d54.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231214-c145d54.img.gz) (Raw image, 206.37 MiB)

### Last commit :star:

```git
commit c145d5410ccca0f67018b7d57013dd2dcefbae6f
Author:     Andrew Kaster <akaster@serenityos.org>
AuthorDate: Wed Dec 13 15:45:32 2023 -0700
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Wed Dec 13 19:21:03 2023 -0700

    Userland: Don't leak objects when constructor arguments throw
    
    Similar to d253beb2f72442f6d05e1b4b0711a37bfe68fdc7.
    
    Found with Ali's clang-query script in Shell:
    
    ```
    for $(find AK Userland -type f -name '*.h' -o -name '*.cpp') {
        in_parallel -j 12 -- clang-query -p \
        Build/lagom/compile_commands.json $it -c \
        'm cxxNewExpr(has(cxxConstructExpr(hasAnyArgument(hasDescendant( \
            allOf(isExpandedFromMacro("TRY"), stmtExpr()))))))' \
        } | grep -v 'matches.' | tee results
    ```
```
