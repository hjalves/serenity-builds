---
title: 'SerenityOS build: Thursday, August 31'
date: 2023-08-31T05:07:40Z
author: Buildbot
description: |
  Commit: 8852561967 (LibWeb: Make use of fractions in `solve_replaced_size_constraint()`, 2023-08-26)

  - [serenity-x86_64-20230831-8852561.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230831-8852561.img.gz) (Raw image, 186.18 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230831-8852561.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230831-8852561.img.gz) (Raw image, 186.18 MiB)

### Last commit :star:

```git
commit 8852561967c7a959b01cc509b57d7e035018eb0f
Author:     Zaggy1024 <zaggy1024@gmail.com>
AuthorDate: Sat Aug 26 17:47:53 2023 -0500
Commit:     Alexander Kalenik <kalenik.aliaksandr@gmail.com>
CommitDate: Wed Aug 30 20:36:27 2023 +0200

    LibWeb: Make use of fractions in `solve_replaced_size_constraint()`
```
