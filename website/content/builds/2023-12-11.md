---
title: 'SerenityOS build: Monday, December 11'
date: 2023-12-11T06:11:12Z
author: Buildbot
description: |
  Commit: 3b3558865c (LibWeb: Improve select element CSS stylebility, 2023-12-10)

  - [serenity-x86_64-20231211-3b35588.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231211-3b35588.img.gz) (Raw image, 206.21 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231211-3b35588.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231211-3b35588.img.gz) (Raw image, 206.21 MiB)

### Last commit :star:

```git
commit 3b3558865ca4c1b8cea7aef31e43a602c6c7993d
Author:     Bastiaan van der Plaat <bastiaan.v.d.plaat@gmail.com>
AuthorDate: Sun Dec 10 19:59:25 2023 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Sun Dec 10 20:56:19 2023 +0100

    LibWeb: Improve select element CSS stylebility
```
