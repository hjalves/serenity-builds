---
title: 'SerenityOS build: Thursday, October 17'
date: 2024-10-17T05:26:50Z
author: Buildbot
description: |
  Commit: f29482b038 (LibTextCodec: Implement UTF8Decoder::to_utf8 using AK::String, 2024-08-11)

  - [serenity-x86_64-20241017-f29482b.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241017-f29482b.img.gz) (Raw image, 224.59 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20241017-f29482b.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241017-f29482b.img.gz) (Raw image, 224.59 MiB)

### Last commit :star:

```git
commit f29482b0384c106abea900cd92f5b82d91f75433
Author:     Shannon Booth <shannon@serenityos.org>
AuthorDate: Sun Aug 11 15:02:55 2024 +1200
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Wed Oct 16 23:56:40 2024 -0400

    LibTextCodec: Implement UTF8Decoder::to_utf8 using AK::String
    
    String::from_utf8_with_replacement_character is equivalent to
    https://encoding.spec.whatwg.org/#utf-8-decode from the encoding spec,
    so we can simply call through to it.
    
    (cherry picked from commit 0b864bef6040fa66f6719bf06898e310d4c5c02f)
```
