---
title: 'SerenityOS build: Saturday, May 28'
date: 2022-05-28T03:05:15Z
author: Buildbot
description: |
  Commit: 58b46d9e37 (Utilities: Add edid-dump program to dump EDID from Display connectors, 2022-05-21)

  - [serenity-i686-20220528-58b46d9.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220528-58b46d9.vdi.gz) (VirtualBox VDI, 122.84 MiB)
  - [serenity-i686-20220528-58b46d9.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220528-58b46d9.img.gz) (Raw image, 123.02 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220528-58b46d9.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220528-58b46d9.vdi.gz) (VirtualBox VDI, 122.84 MiB)
- [serenity-i686-20220528-58b46d9.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220528-58b46d9.img.gz) (Raw image, 123.02 MiB)

### Last commit :star:

```git
commit 58b46d9e37469dc4497867b2424cf8f23dde4db4
Author:     Liav A <liavalb@gmail.com>
AuthorDate: Sat May 21 19:38:50 2022 +0300
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Fri May 27 22:27:44 2022 +0100

    Utilities: Add edid-dump program to dump EDID from Display connectors
```
