---
title: 'SerenityOS build: Sunday, October 30'
date: 2022-10-30T03:58:34Z
author: Buildbot
description: |
  Commit: 63122d0276 (Base: Add Houses, Safety pin emoji, 2022-10-29)

  - [serenity-x86_64-20221030-63122d0.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221030-63122d0.vdi.gz) (VirtualBox VDI, 137.32 MiB)
  - [serenity-x86_64-20221030-63122d0.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221030-63122d0.img.gz) (Raw image, 137.63 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221030-63122d0.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221030-63122d0.vdi.gz) (VirtualBox VDI, 137.32 MiB)
- [serenity-x86_64-20221030-63122d0.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221030-63122d0.img.gz) (Raw image, 137.63 MiB)

### Last commit :star:

```git
commit 63122d0276c3e44c005d0c5eaf57f297ecd567f0
Author:     premek <1145361+premek@users.noreply.github.com>
AuthorDate: Sat Oct 29 18:49:22 2022 +0200
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Sat Oct 29 23:49:21 2022 +0100

    Base: Add Houses, Safety pin emoji
    
    🧷 - U+1F9F7 Safety Pin
    🏘 - U+1F3D8 House Buildings
```
