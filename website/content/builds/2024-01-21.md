---
title: 'SerenityOS build: Sunday, January 21'
date: 2024-01-21T06:12:12Z
author: Buildbot
description: |
  Commit: 1041dbb007 (LibWeb: Don't lose track of inline margins when collapsing whitespace, 2024-01-20)

  - [serenity-x86_64-20240121-1041dbb.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240121-1041dbb.img.gz) (Raw image, 210.71 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240121-1041dbb.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240121-1041dbb.img.gz) (Raw image, 210.71 MiB)

### Last commit :star:

```git
commit 1041dbb007653e654f3052c4d0ed81d27e1e789f
Author:     Andreas Kling <kling@serenityos.org>
AuthorDate: Sat Jan 20 20:15:09 2024 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Sat Jan 20 23:29:51 2024 +0100

    LibWeb: Don't lose track of inline margins when collapsing whitespace
    
    When iterating inline level chunks for a piece of text like " hello ",
    we will get three separate items from InlineLevelIterator:
    
    - Text " "
    - Text "hello"
    - Text " "
    
    If the first item also had some leading margin (e.g margin-left: 10px)
    we would lose that information when deciding that the whitespace is
    collapsible.
    
    This patch fixes the issue by accumulating the amount of leading margin
    present in any collapsed whitespace items, and then adding them to the
    next non-whitespace item in IFC.
    
    It's a wee bit hackish, but so is the rest of the leading/trailing
    margin mechanism.
    
    This makes the header menu on https://www.gimp.org/ look proper. :^)
```
