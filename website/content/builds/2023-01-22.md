---
title: 'SerenityOS build: Sunday, January 22'
date: 2023-01-22T08:51:54Z
author: Buildbot
description: |
  Commit: d33eef14a0 (LibGfx: Minorly simplify a line of code in ICCProfile with OptionalNone, 2023-01-21)

  - [serenity-x86_64-20230122-d33eef1.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230122-d33eef1.img.gz) (Raw image, 682.62 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230122-d33eef1.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230122-d33eef1.img.gz) (Raw image, 682.62 MiB)

### Last commit :star:

```git
commit d33eef14a0811fa461bfe1d0dee3faa9759aa719
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Sat Jan 21 18:30:01 2023 -0500
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Sat Jan 21 22:16:38 2023 -0500

    LibGfx: Minorly simplify a line of code in ICCProfile with OptionalNone
    
    No behavior change.
```
