---
title: 'SerenityOS build: Friday, December 13'
date: 2024-12-13T06:26:00Z
author: Buildbot
description: |
  Commit: 056cba3c0c (Kernel: Use MemoryType::IO for DMA regions everywhere, 2024-12-10)

  - [serenity-x86_64-20241213-056cba3.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241213-056cba3.img.gz) (Raw image, 240 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20241213-056cba3.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241213-056cba3.img.gz) (Raw image, 240 MiB)

### Last commit :star:

```git
commit 056cba3c0cb6d3d957235053cdb457e32799e00e
Author:     Sönke Holz <sholz8530@gmail.com>
AuthorDate: Tue Dec 10 14:14:03 2024 +0100
Commit:     Sönke Holz <sholz8530@gmail.com>
CommitDate: Thu Dec 12 16:07:25 2024 +0100

    Kernel: Use MemoryType::IO for DMA regions everywhere
    
    Drivers using DMA buffers are currently broken on bare metal since
    d3a0ae5c57b and b3bae90e71c made DMA buffers use the NonCacheable memory
    type.
    
    We should investigate each of these drivers and and add proper fences
    where needed.
    
    The only place where MemoryType::IO definitely isn't needed is the xHCI
    scratchpad regions, as they are only accessed by the device.
```
