---
title: 'SerenityOS build: Tuesday, October 18'
date: 2022-10-18T03:58:40Z
author: Buildbot
description: |
  Commit: 3dddc02400 (2048: Do not decrement m_target_tile_power, 2022-10-17)

  - [serenity-x86_64-20221018-3dddc02.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221018-3dddc02.img.gz) (Raw image, 136.25 MiB)
  - [serenity-x86_64-20221018-3dddc02.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221018-3dddc02.vdi.gz) (VirtualBox VDI, 135.92 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221018-3dddc02.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221018-3dddc02.img.gz) (Raw image, 136.25 MiB)
- [serenity-x86_64-20221018-3dddc02.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221018-3dddc02.vdi.gz) (VirtualBox VDI, 135.92 MiB)

### Last commit :star:

```git
commit 3dddc02400e61eed9fc9747ee9611a9242d29dc8
Author:     implicitfield <114500360+implicitfield@users.noreply.github.com>
AuthorDate: Mon Oct 17 20:03:48 2022 +0300
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Mon Oct 17 22:40:52 2022 +0200

    2048: Do not decrement m_target_tile_power
    
    This fixes an issue where the target tile would decrease each time that
    the settings window was opened.
```
