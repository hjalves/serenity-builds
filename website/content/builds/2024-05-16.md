---
title: 'SerenityOS build: Thursday, May 16'
date: 2024-05-16T05:18:33Z
author: Buildbot
description: |
  Commit: daa9e852c9 (Toolchain+CI: Remove unused TRY_USE_LOCAL_TOOLCHAIN parameter, 2024-05-15)

  - [serenity-x86_64-20240516-daa9e85.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240516-daa9e85.img.gz) (Raw image, 215.74 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240516-daa9e85.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240516-daa9e85.img.gz) (Raw image, 215.74 MiB)

### Last commit :star:

```git
commit daa9e852c9bb4d5fee99e4e4b28f14df36f57c86
Author:     Timothy Flynn <trflynn89@pm.me>
AuthorDate: Wed May 15 12:38:49 2024 -0400
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Wed May 15 15:20:29 2024 -0600

    Toolchain+CI: Remove unused TRY_USE_LOCAL_TOOLCHAIN parameter
    
    This was used to create a .tar of the built Clang toolchain, but now we
    just upload the built toolchain artifacts (same as the GNU toolchain).
```
