---
title: 'SerenityOS build: Thursday, December 22'
date: 2022-12-22T03:58:55Z
author: Buildbot
description: |
  Commit: ba60b01026 (HackStudio: Fix typo in one error message, 2022-12-21)

  - [serenity-x86_64-20221222-ba60b01.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221222-ba60b01.img.gz) (Raw image, 144.84 MiB)
  - [serenity-x86_64-20221222-ba60b01.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221222-ba60b01.vdi.gz) (VirtualBox VDI, 144.51 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221222-ba60b01.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221222-ba60b01.img.gz) (Raw image, 144.84 MiB)
- [serenity-x86_64-20221222-ba60b01.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221222-ba60b01.vdi.gz) (VirtualBox VDI, 144.51 MiB)

### Last commit :star:

```git
commit ba60b0102639183009773b229cea5b65a24df8a3
Author:     Karol Kosek <krkk@serenityos.org>
AuthorDate: Wed Dec 21 10:59:46 2022 +0100
Commit:     Sam Atkins <atkinssj@serenityos.org>
CommitDate: Wed Dec 21 19:05:13 2022 +0000

    HackStudio: Fix typo in one error message
    
    A regression from 4784ad66b29dc30fa27e0bd528b5cf85f5287b4b. oops.
```
