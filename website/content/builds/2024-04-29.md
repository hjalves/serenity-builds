---
title: 'SerenityOS build: Monday, April 29'
date: 2024-04-29T05:17:58Z
author: Buildbot
description: |
  Commit: b9df8deba2 (Kernel/USB: Don't include UHCIController.h in USBPipe.h, 2024-03-02)

  - [serenity-x86_64-20240429-b9df8de.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240429-b9df8de.img.gz) (Raw image, 221.13 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240429-b9df8de.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240429-b9df8de.img.gz) (Raw image, 221.13 MiB)

### Last commit :star:

```git
commit b9df8deba2586ac6521e6f420e5fcf2ac920df79
Author:     Liav A. <liavalb@gmail.com>
AuthorDate: Sat Mar 2 20:52:08 2024 +0200
Commit:     Jelle Raaijmakers <jelle@gmta.nl>
CommitDate: Sun Apr 28 22:30:48 2024 +0200

    Kernel/USB: Don't include UHCIController.h in USBPipe.h
    
    The USB::Pipe is abstracted from the actual USB host controller
    implementation, so don't include the UHCIController.h file.
    Also, we missed an include to UserOrKernelBuffer.h, so this is added to
    ensure the code can still compile.
```
