---
title: 'SerenityOS build: Friday, April 14'
date: 2023-04-14T05:03:36Z
author: Buildbot
description: |
  Commit: 957f89ce4a (AK: Add very naive implementation of {sin,cos,tan} for aarch64, 2023-04-13)

  - [serenity-x86_64-20230414-957f89c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230414-957f89c.img.gz) (Raw image, 178.63 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230414-957f89c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230414-957f89c.img.gz) (Raw image, 178.63 MiB)

### Last commit :star:

```git
commit 957f89ce4abb6ad2228b882cf4dde40325ee1ce0
Author:     Timon Kruiper <timonkruiper@gmail.com>
AuthorDate: Thu Apr 13 19:17:34 2023 +0200
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Thu Apr 13 20:24:25 2023 +0200

    AK: Add very naive implementation of {sin,cos,tan} for aarch64
    
    The {sin,cos,tan} functions in AK are used as the implementation of the
    same function in libm. We cannot use the __builtin_foo functions as
    these would just call the libc functions. This was causing an infinite
    loop. Fix this by adding a very naive implementation of
    AK::{sin, cos,tan}, that is only valid for small inputs. For the other
    functions in this file, I added a TODO() such that we'll crash, instead
    of infinite looping.
```
