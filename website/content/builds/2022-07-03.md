---
title: 'SerenityOS build: Sunday, July 03'
date: 2022-07-03T03:05:16Z
author: Buildbot
description: |
  Commit: 143339767b (Kernel/USB: Move buffer allocation from USB transfer to USB pipe, 2022-06-26)

  - [serenity-i686-20220703-1433397.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220703-1433397.vdi.gz) (VirtualBox VDI, 125.16 MiB)
  - [serenity-i686-20220703-1433397.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220703-1433397.img.gz) (Raw image, 125.35 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220703-1433397.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220703-1433397.vdi.gz) (VirtualBox VDI, 125.16 MiB)
- [serenity-i686-20220703-1433397.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220703-1433397.img.gz) (Raw image, 125.35 MiB)

### Last commit :star:

```git
commit 143339767b330bfe1dac820b70ca1c519fec5f55
Author:     b14ckcat <b14ckcat@protonmail.com>
AuthorDate: Sun Jun 26 18:33:53 2022 -0400
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Sun Jul 3 01:15:07 2022 +0200

    Kernel/USB: Move buffer allocation from USB transfer to USB pipe
    
    Currently when allocating buffers for USB transfers, it is done
    once for every transfer rather than once upon creation of the
    USB device. This commit changes that by moving allocation of buffers
    to the USB Pipe class where they can be reused.
```
