---
title: 'SerenityOS build: Saturday, January 06'
date: 2024-01-06T06:11:21Z
author: Buildbot
description: |
  Commit: 107bfbe283 (LibWeb: Remove available space param from track init methods in GFC, 2024-01-05)

  - [serenity-x86_64-20240106-107bfbe.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240106-107bfbe.img.gz) (Raw image, 206.78 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240106-107bfbe.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240106-107bfbe.img.gz) (Raw image, 206.78 MiB)

### Last commit :star:

```git
commit 107bfbe2830fe9ba18327ed36521d371ff0673db
Author:     Aliaksandr Kalenik <kalenik.aliaksandr@gmail.com>
AuthorDate: Fri Jan 5 21:40:29 2024 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Fri Jan 5 22:52:25 2024 +0100

    LibWeb: Remove available space param from track init methods in GFC
    
    Let's consistently use available space from `m_available_space`.
    
    No behavior change expected.
```
