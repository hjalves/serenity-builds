---
title: 'SerenityOS build: Saturday, March 11'
date: 2023-03-11T06:02:05Z
author: Buildbot
description: |
  Commit: 3d33217d60 (Ladybird+CI: Move layout_test.sh test runner from CI yml into CMake, 2023-03-10)

  - [serenity-x86_64-20230311-3d33217.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230311-3d33217.img.gz) (Raw image, 166.72 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230311-3d33217.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230311-3d33217.img.gz) (Raw image, 166.72 MiB)

### Last commit :star:

```git
commit 3d33217d6075f04c714e197a27865b38e381aad2
Author:     Andrew Kaster <akaster@serenityos.org>
AuthorDate: Fri Mar 10 10:45:50 2023 -0700
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Fri Mar 10 23:01:55 2023 +0000

    Ladybird+CI: Move layout_test.sh test runner from CI yml into CMake
    
    We should be able to run this locally, as long as ENABLE_LAGOM_LADYBIRD
    is true, or if building ladybird from the ladybird source directory.
    
    This removes a special case from the Lagom CI yml file.
```
