---
title: 'SerenityOS build: Monday, December 23'
date: 2024-12-23T06:25:41Z
author: Buildbot
description: |
  Commit: cd2a614732 (Base: Add some missing window icons to Redmond Plastic, 2024-12-22)

  - [serenity-x86_64-20241223-cd2a614.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241223-cd2a614.img.gz) (Raw image, 240.82 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20241223-cd2a614.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241223-cd2a614.img.gz) (Raw image, 240.82 MiB)

### Last commit :star:

```git
commit cd2a6147321ed64f4b67f51815368678c6acaf6f
Author:     MacDue <macdue@dueutil.tech>
AuthorDate: Sun Dec 22 15:34:09 2024 +0000
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Sun Dec 22 13:11:10 2024 -0500

    Base: Add some missing window icons to Redmond Plastic
```
