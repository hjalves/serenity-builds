---
title: 'SerenityOS build: Wednesday, September 28'
date: 2022-09-28T03:56:37Z
author: Buildbot
description: |
  Commit: e1edd620ee (pls: Use `LibCore::Account::login()` instead of manually setting the uid, 2022-08-14)

  - [serenity-i686-20220928-e1edd62.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220928-e1edd62.vdi.gz) (VirtualBox VDI, 129.67 MiB)
  - [serenity-i686-20220928-e1edd62.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220928-e1edd62.img.gz) (Raw image, 129.83 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220928-e1edd62.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220928-e1edd62.vdi.gz) (VirtualBox VDI, 129.67 MiB)
- [serenity-i686-20220928-e1edd62.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220928-e1edd62.img.gz) (Raw image, 129.83 MiB)

### Last commit :star:

```git
commit e1edd620ee284794befa65a1b3c316d2bc959eb5
Author:     Lucas CHOLLET <lucas.chollet@free.fr>
AuthorDate: Sun Aug 14 21:19:05 2022 +0200
Commit:     Sam Atkins <atkinssj@serenityos.org>
CommitDate: Wed Sep 28 00:35:36 2022 +0100

    pls: Use `LibCore::Account::login()` instead of manually setting the uid
    
    In addition to changing the uid, the method also changes the gid and
    properly sets groups. So this patch will also mitigate the security
    issue of `pls`.
```
