---
title: 'SerenityOS build: Thursday, July 18'
date: 2024-07-18T05:19:24Z
author: Buildbot
description: |
  Commit: 24fa7ae18f (Tests/LibWeb: Add test for MouseEvent and WheelEvent bubbling, 2024-07-16)

  - [serenity-x86_64-20240718-24fa7ae.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240718-24fa7ae.img.gz) (Raw image, 219.85 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240718-24fa7ae.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240718-24fa7ae.img.gz) (Raw image, 219.85 MiB)

### Last commit :star:

```git
commit 24fa7ae18ff6af1ca2503326678d48804e21c003
Author:     circl <circl.lastname@gmail.com>
AuthorDate: Tue Jul 16 16:18:26 2024 +0200
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Tue Jul 16 15:04:06 2024 -0400

    Tests/LibWeb: Add test for MouseEvent and WheelEvent bubbling
    
    (cherry picked from commit 89531fb115b0df23967a29ffdeda15a738e6f9d5)
```
