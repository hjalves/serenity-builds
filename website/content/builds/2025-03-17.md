---
title: 'SerenityOS build: Monday, March 17'
date: 2025-03-17T06:23:33Z
author: Buildbot
description: |
  Commit: 45dcb51150 (Tests/JPEG2000: Add a test for overflowing the tile part index field, 2025-03-16)

  - [serenity-x86_64-20250317-45dcb51.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250317-45dcb51.img.gz) (Raw image, 242.6 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20250317-45dcb51.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250317-45dcb51.img.gz) (Raw image, 242.6 MiB)

### Last commit :star:

```git
commit 45dcb511501032f9edc12de6db8709440a1aeef2
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Sun Mar 16 19:16:09 2025 -0400
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Sun Mar 16 20:43:55 2025 -0400

    Tests/JPEG2000: Add a test for overflowing the tile part index field
    
    The progression order tests accidentally tested for that previously.
    I fixed that now, but it seems nice to have a test for this, given
    it's easy to create.
    
    I created the input file by running:
    
        ../openjpeg/build/bin/opj_compress -i ref.bmp \
            -o output0-many.jp2 \
            -n 2 -b 4,16 -c '[16,32]' -t 39,53 -r 20,10,1 -TP P -p LRCP \
            -C "opj_compress <all flags from previous line>"
    
    This uses progression order 0 and splits tile parts on the
    fastest-changing part of this order (P). opj_compress then creates
    hundreds of tile parts.  According to Table A.5, the tile-part index
    TPsot is supposed to be in [0, 254], so these images are technically
    invalid per spec -- and jasper, grok, and even opj_decompress do refuse
    to decode them.
    
    Preview.app and Photoshop show them no problem though, so we'll
    want to be able to decode files with >= 255 tile parts at some point.
```
