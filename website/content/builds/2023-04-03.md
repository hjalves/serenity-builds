---
title: 'SerenityOS build: Monday, April 03'
date: 2023-04-03T05:02:47Z
author: Buildbot
description: |
  Commit: 875c3d109f (Meta: Check if SERENITY_USE_SDCARD is set before checking its value, 2023-04-02)

  - [serenity-x86_64-20230403-875c3d1.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230403-875c3d1.img.gz) (Raw image, 169.46 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230403-875c3d1.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230403-875c3d1.img.gz) (Raw image, 169.46 MiB)

### Last commit :star:

```git
commit 875c3d109fbbc4941a2a5d058b9097c979ef38f6
Author:     Andrew Kaster <akaster@serenityos.org>
AuthorDate: Sun Apr 2 13:07:31 2023 -0600
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Sun Apr 2 13:07:31 2023 -0600

    Meta: Check if SERENITY_USE_SDCARD is set before checking its value
    
    This avoids a bash complaint for the new option
```
