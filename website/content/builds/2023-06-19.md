---
title: 'SerenityOS build: Monday, June 19'
date: 2023-06-19T05:04:39Z
author: Buildbot
description: |
  Commit: d89e23d63e (ping: Don't call `exit()` from within `closing_statistics()`, 2023-05-28)

  - [serenity-x86_64-20230619-d89e23d.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230619-d89e23d.img.gz) (Raw image, 184.45 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230619-d89e23d.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230619-d89e23d.img.gz) (Raw image, 184.45 MiB)

### Last commit :star:

```git
commit d89e23d63ee0013edfec98ed350bef57a036ba7f
Author:     Tim Ledbetter <timledbetter@gmail.com>
AuthorDate: Sun May 28 07:01:53 2023 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Mon Jun 19 06:15:40 2023 +0200

    ping: Don't call `exit()` from within `closing_statistics()`
    
    This improves readability slightly, as it isn't immediately obvious
    that calling this function terminates the program.
```
