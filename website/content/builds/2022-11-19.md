---
title: 'SerenityOS build: Saturday, November 19'
date: 2022-11-19T04:00:24Z
author: Buildbot
description: |
  Commit: 7725042235 (Kernel: Fix includes when building aarch64, 2022-11-18)

  - [serenity-x86_64-20221119-7725042.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221119-7725042.vdi.gz) (VirtualBox VDI, 140.03 MiB)
  - [serenity-x86_64-20221119-7725042.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221119-7725042.img.gz) (Raw image, 140.36 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221119-7725042.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221119-7725042.vdi.gz) (VirtualBox VDI, 140.03 MiB)
- [serenity-x86_64-20221119-7725042.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221119-7725042.img.gz) (Raw image, 140.36 MiB)

### Last commit :star:

```git
commit 7725042235b8e59c7f3f927e8cfb27bf124bfbaf
Author:     Steffen Rusitschka <rusi@gmx.de>
AuthorDate: Fri Nov 18 23:17:06 2022 +0100
Commit:     Brian Gianforcaro <bgianf@serenityos.org>
CommitDate: Fri Nov 18 16:25:33 2022 -0800

    Kernel: Fix includes when building aarch64
    
    This patch fixes some include problems on aarch64. aarch64 is still
    currently broken but this will get us back to the underlying problem
    of FloatExtractor.
```
