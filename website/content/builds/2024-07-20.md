---
title: 'SerenityOS build: Saturday, July 20'
date: 2024-07-20T05:18:58Z
author: Buildbot
description: |
  Commit: 02157e3eaf (LibGUI: Improve how SpinBox handles negative numbers, 2024-07-15)

  - [serenity-x86_64-20240720-02157e3.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240720-02157e3.img.gz) (Raw image, 219.89 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240720-02157e3.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240720-02157e3.img.gz) (Raw image, 219.89 MiB)

### Last commit :star:

```git
commit 02157e3eaf8e8f7b67c8ebdd3e92b1ef0d62f5be
Author:     mkblumenau <98577204+mkblumenau@users.noreply.github.com>
AuthorDate: Mon Jul 15 23:54:20 2024 -0400
Commit:     Sam Atkins <atkinssj@serenityos.org>
CommitDate: Fri Jul 19 21:20:52 2024 +0100

    LibGUI: Improve how SpinBox handles negative numbers
    
    Negative numbers now display correctly in SpinBox.
    Previously, they displayed as the negative sign only (no number).
    Now the user can also type negative numbers into the SpinBox.
```
