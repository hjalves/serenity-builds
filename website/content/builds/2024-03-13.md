---
title: 'SerenityOS build: Wednesday, March 13'
date: 2024-03-13T06:14:42Z
author: Buildbot
description: |
  Commit: 5713c2ffdd (CI: Work around a libasan bug seen on Ubuntu with the Linux 6.5 kernel, 2024-03-12)

  - [serenity-x86_64-20240313-5713c2f.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240313-5713c2f.img.gz) (Raw image, 218.73 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240313-5713c2f.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240313-5713c2f.img.gz) (Raw image, 218.73 MiB)

### Last commit :star:

```git
commit 5713c2ffdd7f0c701d7edb468d07f6cea5cd8770
Author:     Timothy Flynn <trflynn89@pm.me>
AuthorDate: Tue Mar 12 19:53:15 2024 -0400
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Tue Mar 12 21:45:25 2024 -0400

    CI: Work around a libasan bug seen on Ubuntu with the Linux 6.5 kernel
    
    We are recently seeing SEGV crashes during the build (while running code
    generators) from within libasan itself. Turns out this is libasan bug
    seen with the Linux 6.5 kernel on Ubuntu.
```
