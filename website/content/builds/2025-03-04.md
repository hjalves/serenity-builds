---
title: 'SerenityOS build: Tuesday, March 04'
date: 2025-03-04T06:23:09Z
author: Buildbot
description: |
  Commit: 27e1910c95 (LibPDF: Remove incorrect page tree heuristic, 2025-03-03)

  - [serenity-x86_64-20250304-27e1910.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250304-27e1910.img.gz) (Raw image, 242.55 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20250304-27e1910.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250304-27e1910.img.gz) (Raw image, 242.55 MiB)

### Last commit :star:

```git
commit 27e1910c95c8c81d4be47a4891bb150c43c1c7b2
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Mon Mar 3 17:40:28 2025 -0500
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Mon Mar 3 18:01:36 2025 -0500

    LibPDF: Remove incorrect page tree heuristic
    
    add_page_tree_node_to_page_tree() had a heuristic where it would decide
    that a page tree node where number of items in the /Kids array was equal
    of the number of total children stored in /Count in that same node must
    be the final non-leaf level of the page tree.
    
    That seems sensible, but it is e.g. possible to have a page tree node
    that has one kid and one page tree node total below it. Such a node
    isn't useful, but it can exist, and it does exist e.g. in 0000037.pdf
    page 37:
    
    ```
    505 0 obj
    <<
      /Type /Pages
      /Count 37
      /Kids [ 503 0 R 504 0 R ]
    >>
    endobj
    
    504 0 obj
    <<
      /Type /Pages
      /Count 1
      /Parent 505 0 R
      /Kids [ 467 0 R ]
    >>
    endobj
    
    467 0 obj
    <<
      /Type /Pages
      /Count 1
      /Parent 504 0 R
      /Kids [ 464 0 R ]
    >>
    endobj
    ```
    
    Object 504 is useless here and object 505 could just reference object
    467 directly, but it doesn't.
    
    Furthermore, conditionally_parse_page_tree_node() already handles all
    this correctly, so we can just remove this special case.
    
    With this, we render page 37 of 0000037.pdf correctly (previously, we
    thought that the page didn't have any contents.)
    
    Add such a silly internal page tree node to colorspaces.pdf, so that we
    have an automated test for this slightly subtle behavior.
```
