---
title: 'SerenityOS build: Sunday, January 05'
date: 2025-01-05T06:22:44Z
author: Buildbot
description: |
  Commit: fd789689aa (Ports/SDL2: Sync scancode map with Kernel/API/KeyCode.h, 2025-01-05)

  - [serenity-x86_64-20250105-fd78968.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250105-fd78968.img.gz) (Raw image, 240.9 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20250105-fd78968.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250105-fd78968.img.gz) (Raw image, 240.9 MiB)

### Last commit :star:

```git
commit fd789689aa7c18c3d2d1b5946bf2da484ba8a869
Author:     ddorando <87425418+ddorando@users.noreply.github.com>
AuthorDate: Sun Jan 5 00:52:34 2025 +0200
Commit:     Tim Schumacher <timschumi@gmx.de>
CommitDate: Sun Jan 5 01:44:46 2025 +0100

    Ports/SDL2: Sync scancode map with Kernel/API/KeyCode.h
    
    We got out of sync in 965e1ba. Sync the names in order to fix build.
```
