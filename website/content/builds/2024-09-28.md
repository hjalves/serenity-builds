---
title: 'SerenityOS build: Saturday, September 28'
date: 2024-09-28T05:22:26Z
author: Buildbot
description: |
  Commit: 88d432e9a3 (UI/Qt: Display query results on find in page panel, 2024-06-09)

  - [serenity-x86_64-20240928-88d432e.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240928-88d432e.img.gz) (Raw image, 221.73 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240928-88d432e.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240928-88d432e.img.gz) (Raw image, 221.73 MiB)

### Last commit :star:

```git
commit 88d432e9a3de025247f8842e956e505626be8fa8
Author:     Tim Ledbetter <timledbetter@gmail.com>
AuthorDate: Sun Jun 9 19:25:17 2024 +0100
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Sat Sep 28 00:16:05 2024 -0400

    UI/Qt: Display query results on find in page panel
    
    The number of matches and current match index are now displayed to the
    user when a find in page query is performed.
    
    (cherry picked from commit 2ea680b5b3fa91d7b9df8c16b0e0bca557fc559d)
```
