---
title: 'SerenityOS build: Tuesday, October 24'
date: 2023-10-24T05:08:24Z
author: Buildbot
description: |
  Commit: 7e10f76021 (LibGUI: Don't update ComboBox text when model index is invalid, 2023-10-22)

  - [serenity-x86_64-20231024-7e10f76.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231024-7e10f76.img.gz) (Raw image, 190.27 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231024-7e10f76.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231024-7e10f76.img.gz) (Raw image, 190.27 MiB)

### Last commit :star:

```git
commit 7e10f76021cc4664105b97064bda01f1a51c7c4a
Author:     Adrian Mück <adrian.mueck@magenta.de>
AuthorDate: Sun Oct 22 19:48:20 2023 +0200
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Mon Oct 23 21:18:36 2023 -0400

    LibGUI: Don't update ComboBox text when model index is invalid
    
    Without ENABLE_TIME_ZONE_DATA the user is able to update the time zone
    selection when using "on_mousewheel" or "on_{up,down}_pressed" even
    though UTC is supposed to be the only option. Due to an invalid model
    index, the selection is set to "[null]".
    
    Prior to this patch, the ComboBox text in "selection_updated" is set
    at each call.
```
