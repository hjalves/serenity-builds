---
title: 'SerenityOS build: Saturday, May 04'
date: 2024-05-04T05:18:00Z
author: Buildbot
description: |
  Commit: aede010096 (LibWeb: Use GCPtr for Element::layout_node, 2024-05-02)

  - [serenity-x86_64-20240504-aede010.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240504-aede010.img.gz) (Raw image, 221.28 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240504-aede010.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240504-aede010.img.gz) (Raw image, 221.28 MiB)

### Last commit :star:

```git
commit aede0100960b6b9cacd0e5a0bd0887bd34651e29
Author:     Shannon Booth <shannon@serenityos.org>
AuthorDate: Thu May 2 22:32:44 2024 +1200
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Fri May 3 20:48:32 2024 +0200

    LibWeb: Use GCPtr for Element::layout_node
    
    This improves debuggability by converting a SIGSEGV into a
    verification failed on a null dereference.
```
