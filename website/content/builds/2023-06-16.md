---
title: 'SerenityOS build: Friday, June 16'
date: 2023-06-16T05:04:22Z
author: Buildbot
description: |
  Commit: 7302f8838c (Base: Remove unused png file, 2023-06-15)

  - [serenity-x86_64-20230616-7302f88.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230616-7302f88.img.gz) (Raw image, 184.31 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230616-7302f88.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230616-7302f88.img.gz) (Raw image, 184.31 MiB)

### Last commit :star:

```git
commit 7302f8838cde693f280a71a46af35e1771d8d4fd
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Thu Jun 15 14:01:50 2023 -0700
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Thu Jun 15 17:44:44 2023 -0400

    Base: Remove unused png file
    
    See here: https://github.com/SerenityOS/serenity/pull/15234#issuecomment-1591477603
```
