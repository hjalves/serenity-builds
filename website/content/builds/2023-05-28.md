---
title: 'SerenityOS build: Sunday, May 28'
date: 2023-05-28T05:03:53Z
author: Buildbot
description: |
  Commit: ea54c58930 (WebP/Lossy: Variable naming fix for constants from last pull request, 2023-05-27)

  - [serenity-x86_64-20230528-ea54c58.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230528-ea54c58.img.gz) (Raw image, 183.12 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230528-ea54c58.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230528-ea54c58.img.gz) (Raw image, 183.12 MiB)

### Last commit :star:

```git
commit ea54c589301c224c429ffd8f368ae11d15780e45
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Sat May 27 16:06:33 2023 -0400
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Sat May 27 15:25:00 2023 -0600

    WebP/Lossy: Variable naming fix for constants from last pull request
```
