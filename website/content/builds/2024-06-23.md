---
title: 'SerenityOS build: Sunday, June 23'
date: 2024-06-23T05:18:33Z
author: Buildbot
description: |
  Commit: 5cb1d2a63e (Utilities/pkg: Move ports database handling from AvailablePort{.h,.cpp}, 2024-05-25)

  - [serenity-x86_64-20240623-5cb1d2a.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240623-5cb1d2a.img.gz) (Raw image, 219.73 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240623-5cb1d2a.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240623-5cb1d2a.img.gz) (Raw image, 219.73 MiB)

### Last commit :star:

```git
commit 5cb1d2a63ed46a667e19591d101cd7355f63c60a
Author:     Liav A. <liavalb@gmail.com>
AuthorDate: Sat May 25 15:17:01 2024 +0300
Commit:     Tim Schumacher <timschumi@gmx.de>
CommitDate: Sun Jun 23 00:59:54 2024 +0200

    Utilities/pkg: Move ports database handling from AvailablePort{.h,.cpp}
    
    Instead, let's create a new class called AvailablePortDatabase that will
    handle updating (from the Internet, by using our repository) of the list
    and instantiating an object that could be used for querying.
```
