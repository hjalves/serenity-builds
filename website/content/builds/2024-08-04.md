---
title: 'SerenityOS build: Sunday, August 04'
date: 2024-08-04T05:21:45Z
author: Buildbot
description: |
  Commit: cb10f70394 (Kernel: Change internal handling of filesystem-specific options, 2024-07-14)

  - [serenity-x86_64-20240804-cb10f70.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240804-cb10f70.img.gz) (Raw image, 221.39 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240804-cb10f70.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240804-cb10f70.img.gz) (Raw image, 221.39 MiB)

### Last commit :star:

```git
commit cb10f70394fb7e9cfc77f827adb2e46d199bc3a5
Author:     Liav A. <liavalb@gmail.com>
AuthorDate: Sun Jul 14 14:07:23 2024 +0300
Commit:     Tim Schumacher <timschumi@gmx.de>
CommitDate: Sat Aug 3 20:35:06 2024 +0200

    Kernel: Change internal handling of filesystem-specific options
    
    Instead of using a raw `KBuffer` and letting each implementation to
    populating the specific flags on its own, we change things so we only
    let each FileSystem implementation to validate the flag and its value
    but then store it in a HashMap which its key is the flag name and
    the value is a special new class called `FileSystemSpecificOption`
    which wraps around `AK::Variant<...>`.
    
    This approach has multiple advantages over the previous:
    - It allows runtime inspection of what the user has set on a `MountFile`
      description for a specific filesystem.
    - It ensures accidental overriding of filesystem specific option that
      was already set is not possible
    - It removes ugly casting of a `KBuffer` contents to a strongly-typed
      values. Instead, a strongly-typed `AK::Variant` is used which ensures
      we always get a value without doing any casting.
    
    Please note that we have removed support for ASCII string-oriented flags
    as there were no actual use cases, and supporting such type would make
    `FileSystemSpecificOption` more complicated unnecessarily for now.
```
