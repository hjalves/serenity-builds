---
title: 'SerenityOS build: Tuesday, October 03'
date: 2023-10-03T05:10:27Z
author: Buildbot
description: |
  Commit: 361e29cfc9 (LibGUI: Don't enter TableView edit mode when a control key is pressed, 2023-09-27)

  - [serenity-x86_64-20231003-361e29c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231003-361e29c.img.gz) (Raw image, 186.34 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231003-361e29c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231003-361e29c.img.gz) (Raw image, 186.34 MiB)

### Last commit :star:

```git
commit 361e29cfc9320e2873d5ec12cd34caec79d6afee
Author:     Tim Ledbetter <timledbetter@gmail.com>
AuthorDate: Wed Sep 27 09:42:18 2023 +0100
Commit:     Jelle Raaijmakers <jelle@gmta.nl>
CommitDate: Mon Oct 2 21:38:09 2023 +0200

    LibGUI: Don't enter TableView edit mode when a control key is pressed
    
    A key press, which is an ASCII control character will no longer cause
    TableView to begin editing.
    
    This fixes an issue in Spreadsheet where navigating to a cell then
    pressing escape would cause a that cell's text to be set to a
    non-printable value. Pressing escape after navigating to a cell
    now has no effect.
```
