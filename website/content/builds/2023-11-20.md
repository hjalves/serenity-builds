---
title: 'SerenityOS build: Monday, November 20'
date: 2023-11-20T06:10:43Z
author: Buildbot
description: |
  Commit: 7ee47d81ca (LibWeb: Allocate custom element reactions queue on demand, 2023-11-19)

  - [serenity-x86_64-20231120-7ee47d8.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231120-7ee47d8.img.gz) (Raw image, 204.82 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231120-7ee47d8.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231120-7ee47d8.img.gz) (Raw image, 204.82 MiB)

### Last commit :star:

```git
commit 7ee47d81ca52017178c44b4c605eb03b6d3fa090
Author:     Andreas Kling <kling@serenityos.org>
AuthorDate: Sun Nov 19 21:53:58 2023 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Mon Nov 20 00:39:42 2023 +0100

    LibWeb: Allocate custom element reactions queue on demand
    
    This shrinks most DOM elements by 16 bytes.
```
