---
title: 'SerenityOS build: Wednesday, July 03'
date: 2024-07-03T05:19:32Z
author: Buildbot
description: |
  Commit: a2a6bc5348 (Documentation: Fix some minor ESL grammar issues, 2024-07-02)

  - [serenity-x86_64-20240703-a2a6bc5.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240703-a2a6bc5.img.gz) (Raw image, 219.18 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240703-a2a6bc5.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240703-a2a6bc5.img.gz) (Raw image, 219.18 MiB)

### Last commit :star:

```git
commit a2a6bc534868773b9320ec3ca7399283cf7a375b
Author:     Ryan Castellucci <code@ryanc.org>
AuthorDate: Tue Jul 2 20:50:33 2024 +0100
Commit:     Jelle Raaijmakers <jelle@gmta.nl>
CommitDate: Wed Jul 3 00:17:46 2024 +0200

    Documentation: Fix some minor ESL grammar issues
    
    There are a few instances where comments and documentation have minor
    grammar issues likely resulting from English being the author's second
    language.
    
    This PR fixes several such cases, changing to idiomatic English and
    resolving where it is unclear whether the user or program/code is
    being referred to.
```
