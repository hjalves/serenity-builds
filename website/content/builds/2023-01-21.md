---
title: 'SerenityOS build: Saturday, January 21'
date: 2023-01-21T06:08:37Z
author: Buildbot
description: |
  Commit: a0522aec90 (icc: Remove needless use of DateTime::to_deprecated_string(), 2023-01-20)

  - [serenity-x86_64-20230121-a0522ae.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230121-a0522ae.img.gz) (Raw image, 680.62 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230121-a0522ae.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230121-a0522ae.img.gz) (Raw image, 680.62 MiB)

### Last commit :star:

```git
commit a0522aec90048fa41cf2ba568f0ef9b1da6984fb
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Fri Jan 20 20:46:39 2023 -0500
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Sat Jan 21 01:50:24 2023 +0000

    icc: Remove needless use of DateTime::to_deprecated_string()
    
    Turns out DateTime has a Formatter that prints the same thing that
    to_deprecated_string() returns.
    
    No behavior change.
```
