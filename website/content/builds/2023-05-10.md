---
title: 'SerenityOS build: Wednesday, May 10'
date: 2023-05-10T05:03:31Z
author: Buildbot
description: |
  Commit: 14cb0067bb (LibWeb: Implement more of "Resolve Intrinsic Track Sizes" in GFC, 2023-05-10)

  - [serenity-x86_64-20230510-14cb006.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230510-14cb006.img.gz) (Raw image, 181.61 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230510-14cb006.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230510-14cb006.img.gz) (Raw image, 181.61 MiB)

### Last commit :star:

```git
commit 14cb0067bba18a23fe53b615a7f8417787d95fc2
Author:     Aliaksandr Kalenik <kalenik.aliaksandr@gmail.com>
AuthorDate: Wed May 10 05:21:04 2023 +0300
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Wed May 10 05:52:47 2023 +0200

    LibWeb: Implement more of "Resolve Intrinsic Track Sizes" in GFC
    
    Implements some parts of "Resolve Intrinsic Track Sizes" algorithm
    from spec to make it more spec compliant.
```
