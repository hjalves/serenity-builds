---
title: 'SerenityOS build: Sunday, July 10'
date: 2022-07-10T03:05:16Z
author: Buildbot
description: |
  Commit: 071b92e920 (LibJS: Fix typos in Temporal spec comments, 2022-07-10)

  - [serenity-i686-20220710-071b92e.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220710-071b92e.vdi.gz) (VirtualBox VDI, 125.02 MiB)
  - [serenity-i686-20220710-071b92e.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220710-071b92e.img.gz) (Raw image, 125.19 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220710-071b92e.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220710-071b92e.vdi.gz) (VirtualBox VDI, 125.02 MiB)
- [serenity-i686-20220710-071b92e.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220710-071b92e.img.gz) (Raw image, 125.19 MiB)

### Last commit :star:

```git
commit 071b92e920f49783973619f8edd8877b6754d75f
Author:     Linus Groh <mail@linusgroh.de>
AuthorDate: Sun Jul 10 01:29:28 2022 +0200
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Sun Jul 10 01:29:28 2022 +0200

    LibJS: Fix typos in Temporal spec comments
    
    This is an editorial change in the Temporal spec.
    
    See: https://github.com/tc39/proposal-temporal/commit/b0411b4
```
