---
title: 'SerenityOS build: Saturday, June 15'
date: 2024-06-15T05:19:02Z
author: Buildbot
description: |
  Commit: 462abc242e (Tests/LibWeb: Add test to verify correctness of getImageData, 2024-06-14)

  - [serenity-x86_64-20240615-462abc2.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240615-462abc2.img.gz) (Raw image, 218.73 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240615-462abc2.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240615-462abc2.img.gz) (Raw image, 218.73 MiB)

### Last commit :star:

```git
commit 462abc242e7295f4469275a3ccc5a4dbadec4972
Author:     circl <circl.lastname@gmail.com>
AuthorDate: Fri Jun 14 15:22:25 2024 +0200
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Fri Jun 14 18:18:40 2024 -0400

    Tests/LibWeb: Add test to verify correctness of getImageData
    
    (cherry picked from commit dbc94ce92e09e987f8a07a5b1e978250286b015a)
```
