---
title: 'SerenityOS build: Thursday, December 28'
date: 2023-12-28T06:11:26Z
author: Buildbot
description: |
  Commit: 522302d5d6 (LibWeb: Skip erroneous blit/sample corner commands in RecordingPainter, 2023-12-27)

  - [serenity-x86_64-20231228-522302d.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231228-522302d.img.gz) (Raw image, 206.37 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231228-522302d.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231228-522302d.img.gz) (Raw image, 206.37 MiB)

### Last commit :star:

```git
commit 522302d5d647ca8cd0a5e2820e18cbcfdd3c7a62
Author:     Aliaksandr Kalenik <kalenik.aliaksandr@gmail.com>
AuthorDate: Wed Dec 27 18:48:28 2023 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Wed Dec 27 20:00:16 2023 +0100

    LibWeb: Skip erroneous blit/sample corner commands in RecordingPainter
    
    Fixes https://github.com/SerenityOS/serenity/issues/22451
```
