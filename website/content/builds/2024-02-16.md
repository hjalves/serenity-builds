---
title: 'SerenityOS build: Friday, February 16'
date: 2024-02-16T06:16:12Z
author: Buildbot
description: |
  Commit: 17b22250b6 (LibGfx/OpenType: Replace a magic number with a calculation, 2024-02-15)

  - [serenity-x86_64-20240216-17b2225.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240216-17b2225.img.gz) (Raw image, 215.35 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240216-17b2225.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240216-17b2225.img.gz) (Raw image, 215.35 MiB)

### Last commit :star:

```git
commit 17b22250b6afa30366903b314a62b78a8a94fb79
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Thu Feb 15 19:48:53 2024 -0500
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Thu Feb 15 22:53:24 2024 -0500

    LibGfx/OpenType: Replace a magic number with a calculation
    
    Makes this code look like the corresponding code in the ScaledFont ctor.
    
    Similar to the last commit on #20084.
    
    No behavior change.
```
