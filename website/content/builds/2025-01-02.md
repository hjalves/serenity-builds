---
title: 'SerenityOS build: Thursday, January 02'
date: 2025-01-02T06:22:46Z
author: Buildbot
description: |
  Commit: e6099bea77 (Meta: Add User-Agent string on request for download_file, 2024-12-31)

  - [serenity-x86_64-20250102-e6099be.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250102-e6099be.img.gz) (Raw image, 240.89 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20250102-e6099be.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250102-e6099be.img.gz) (Raw image, 240.89 MiB)

### Last commit :star:

```git
commit e6099bea77b6ee57340710d35dcae699bcb2e88a
Author:     Eduardo HCS <educasadei@gmail.com>
AuthorDate: Tue Dec 31 17:02:14 2024 -0400
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Wed Jan 1 14:06:03 2025 -0500

    Meta: Add User-Agent string on request for download_file
```
