---
title: 'SerenityOS build: Saturday, July 01'
date: 2023-07-01T05:06:50Z
author: Buildbot
description: |
  Commit: 6eb06384b3 (Kernel: Increase SD Data Timeout, 2023-06-24)

  - [serenity-x86_64-20230701-6eb0638.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230701-6eb0638.img.gz) (Raw image, 185.71 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230701-6eb0638.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230701-6eb0638.img.gz) (Raw image, 185.71 MiB)

### Last commit :star:

```git
commit 6eb06384b30592837cea423014b87c868d5beae4
Author:     Daniel Bertalan <dani@danielbertalan.dev>
AuthorDate: Sat Jun 24 11:43:10 2023 +0200
Commit:     Jelle Raaijmakers <jelle@gmta.nl>
CommitDate: Fri Jun 30 23:45:47 2023 +0200

    Kernel: Increase SD Data Timeout
    
    Otherwise, reading will sometimes fail on the Raspberry Pi.
    
    This is mostly a hack, the spec has some info about how the correct
    divisor should be calculated and how we can recover from timeouts.
```
