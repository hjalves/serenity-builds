---
title: 'SerenityOS build: Monday, May 30'
date: 2022-05-30T03:05:54Z
author: Buildbot
description: |
  Commit: be36557198 (Lagom/Fuzzers: Add CSS parser fuzzer, 2022-05-29)

  - [serenity-i686-20220530-be36557.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220530-be36557.img.gz) (Raw image, 123.38 MiB)
  - [serenity-i686-20220530-be36557.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220530-be36557.vdi.gz) (VirtualBox VDI, 123.2 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220530-be36557.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220530-be36557.img.gz) (Raw image, 123.38 MiB)
- [serenity-i686-20220530-be36557.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220530-be36557.vdi.gz) (VirtualBox VDI, 123.2 MiB)

### Last commit :star:

```git
commit be365571980d3d9622735a7432e1958b30e9cf33
Author:     Luke Wilde <lukew@serenityos.org>
AuthorDate: Sun May 29 23:55:49 2022 +0100
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Mon May 30 00:17:11 2022 +0100

    Lagom/Fuzzers: Add CSS parser fuzzer
```
