---
title: 'SerenityOS build: Friday, August 25'
date: 2023-08-25T05:08:02Z
author: Buildbot
description: |
  Commit: 70a87795e4 (LibWeb: Remove the Tests subfolder, 2023-08-24)

  - [serenity-x86_64-20230825-70a8779.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230825-70a8779.img.gz) (Raw image, 185.49 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230825-70a8779.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230825-70a8779.img.gz) (Raw image, 185.49 MiB)

### Last commit :star:

```git
commit 70a87795e43f39309ae0a066c838cf4a51ac6df9
Author:     Timothy Flynn <trflynn89@pm.me>
AuthorDate: Thu Aug 24 17:12:21 2023 -0400
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Fri Aug 25 05:39:58 2023 +0200

    LibWeb: Remove the Tests subfolder
    
    These were used only by test-web, which was removed in commit 81aa60163.
```
