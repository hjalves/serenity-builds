---
title: 'SerenityOS build: Sunday, May 15'
date: 2022-05-15T03:05:03Z
author: Buildbot
description: |
  Commit: f8984146bd (Ports: Add thesilversearcher (ag), 2022-05-14)

  - [serenity-i686-20220515-f898414.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220515-f898414.img.gz) (Raw image, 122.08 MiB)
  - [serenity-i686-20220515-f898414.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220515-f898414.vdi.gz) (VirtualBox VDI, 121.89 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220515-f898414.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220515-f898414.img.gz) (Raw image, 122.08 MiB)
- [serenity-i686-20220515-f898414.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220515-f898414.vdi.gz) (VirtualBox VDI, 121.89 MiB)

### Last commit :star:

```git
commit f8984146bd7e98cefa28ea48f10275096a9ff663
Author:     Raymond Lucke <ray@raylucke.com>
AuthorDate: Sat May 14 13:25:23 2022 -0400
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Sat May 14 19:38:07 2022 +0200

    Ports: Add thesilversearcher (ag)
```
