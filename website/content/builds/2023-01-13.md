---
title: 'SerenityOS build: Friday, January 13'
date: 2023-01-13T06:09:34Z
author: Buildbot
description: |
  Commit: e2db717bf5 (LibWeb: Fix ignored .to_string() errors in Web::dump_sheet(), 2023-01-13)

  - [serenity-x86_64-20230113-e2db717.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230113-e2db717.img.gz) (Raw image, 678.59 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230113-e2db717.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230113-e2db717.img.gz) (Raw image, 678.59 MiB)

### Last commit :star:

```git
commit e2db717bf52039f9f3f253e8e351cf8df4b5172b
Author:     MacDue <macdue@dueutil.tech>
AuthorDate: Fri Jan 13 20:10:00 2023 +0100
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Thu Jan 12 23:29:57 2023 +0000

    LibWeb: Fix ignored .to_string() errors in Web::dump_sheet()
```
