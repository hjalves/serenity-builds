---
title: 'SerenityOS build: Sunday, March 03'
date: 2024-03-03T06:14:09Z
author: Buildbot
description: |
  Commit: cd0302426f (LibWeb: Move serialization of serializable object's interface name, 2024-03-02)

  - [serenity-x86_64-20240303-cd03024.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240303-cd03024.img.gz) (Raw image, 218.42 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240303-cd03024.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240303-cd03024.img.gz) (Raw image, 218.42 MiB)

### Last commit :star:

```git
commit cd0302426f5ae0e5ded3346a8b59ee7c89779441
Author:     Kenneth Myhra <kennethmyhra@serenityos.org>
AuthorDate: Sat Mar 2 22:32:50 2024 +0100
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Sat Mar 2 17:04:09 2024 -0700

    LibWeb: Move serialization of serializable object's interface name
    
    To be consistent with the deserialization steps, move serialization of
    the serializable object's interface name out of the serialization steps.
```
