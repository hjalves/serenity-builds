---
title: 'SerenityOS build: Wednesday, March 15'
date: 2023-03-15T06:02:13Z
author: Buildbot
description: |
  Commit: 5141c86587 (AK: Rename CaseInsensitiveStringViewTraits to reflect intent, 2023-03-14)

  - [serenity-x86_64-20230315-5141c86.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230315-5141c86.img.gz) (Raw image, 166.96 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230315-5141c86.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230315-5141c86.img.gz) (Raw image, 166.96 MiB)

### Last commit :star:

```git
commit 5141c865879bc8de6ffd98d4ea6f3fbd59b6434b
Author:     gustrb <gustavoreisbauer@gmail.com>
AuthorDate: Tue Mar 14 08:40:53 2023 -0300
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Tue Mar 14 21:34:32 2023 +0000

    AK: Rename CaseInsensitiveStringViewTraits to reflect intent
    
    Now it is called `CaseInsensitiveASCIIStringViewTraits`, so we can be
    more specific about what data structure does it operate onto. ;)
```
