---
title: 'SerenityOS build: Friday, November 01'
date: 2024-11-01T06:26:07Z
author: Buildbot
description: |
  Commit: 59a95fbbf6 (UI/Qt: Update placeholder text if search disabled, 2024-06-10)

  - [serenity-x86_64-20241101-59a95fb.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241101-59a95fb.img.gz) (Raw image, 225.23 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20241101-59a95fb.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241101-59a95fb.img.gz) (Raw image, 225.23 MiB)

### Last commit :star:

```git
commit 59a95fbbf6e04bf1afafb6c50392e0b9670404f3
Author:     Hugh Davenport <hugh@davenport.co.nz>
AuthorDate: Mon Jun 10 13:43:50 2024 +1200
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Thu Oct 31 23:00:58 2024 -0400

    UI/Qt: Update placeholder text if search disabled
    
    The placeholder text is there to prompt the user as to what could be
    added in the address bar. The current text tells the user that they can
    "Search or enter web address" even when the search setting is disabled.
    When attempting to "Search" the user is instead sent to page ":", with
    an error in the console:
        WebContent(575249): (FIXME) Don't know how to navigate to :
    
    This patch fixes this by checking whether the search feature is enabled
    and setting the placeholder appropriately. This provides a slightly
    better user experience.
    
    Closes #132
    
    (cherry picked from commit cb657a038f0c0f598993d6c3c3b8ff21f344ac40)
```
