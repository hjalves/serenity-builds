---
title: 'SerenityOS build: Sunday, December 10'
date: 2023-12-10T06:10:55Z
author: Buildbot
description: |
  Commit: 6a4e3d9002 (Solitaire: Ability to automatically solve the end of the game, 2023-11-11)

  - [serenity-x86_64-20231210-6a4e3d9.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231210-6a4e3d9.img.gz) (Raw image, 205.85 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231210-6a4e3d9.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231210-6a4e3d9.img.gz) (Raw image, 205.85 MiB)

### Last commit :star:

```git
commit 6a4e3d90023b529c70e297201156530187fda4a1
Author:     david072 <david.g.ganz@gmail.com>
AuthorDate: Sat Nov 11 13:41:49 2023 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Sun Dec 10 00:02:32 2023 +0100

    Solitaire: Ability to automatically solve the end of the game
    
    In Solitaire, when the stock stack is empty and all cards have been
    revealed, finishing the game is trivial, since you only need to sort the
    already visible cards onto the foundation stacks. To simplify this, the
    game will now give you the option to finish the game automatically if
    these conditions are met. It then sorts the cards with a delay of 100ms
    between each card.
```
