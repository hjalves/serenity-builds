---
title: 'SerenityOS build: Monday, October 02'
date: 2023-10-02T05:10:59Z
author: Buildbot
description: |
  Commit: 001abbcd66 (AK: Rename `URLParser:parse_host()` param is_not_special to is_opaque, 2023-09-30)

  - [serenity-x86_64-20231002-001abbc.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231002-001abbc.img.gz) (Raw image, 186.23 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231002-001abbc.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231002-001abbc.img.gz) (Raw image, 186.23 MiB)

### Last commit :star:

```git
commit 001abbcd66867c8bf134db5986a3f475c43daa9f
Author:     Kemal Zebari <kemalzebra@gmail.com>
AuthorDate: Sat Sep 30 23:07:03 2023 -0700
Commit:     Jelle Raaijmakers <jelle@gmta.nl>
CommitDate: Sun Oct 1 14:35:40 2023 +0200

    AK: Rename `URLParser:parse_host()` param is_not_special to is_opaque
    
    Since the spec renamed it to make it easier to read.
```
