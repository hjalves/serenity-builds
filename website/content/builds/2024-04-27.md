---
title: 'SerenityOS build: Saturday, April 27'
date: 2024-04-27T05:17:37Z
author: Buildbot
description: |
  Commit: 2925fcd4bc (Userland: Compile a special version of LibELF for DynamicLoader, 2024-04-24)

  - [serenity-x86_64-20240427-2925fcd.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240427-2925fcd.img.gz) (Raw image, 220.97 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240427-2925fcd.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240427-2925fcd.img.gz) (Raw image, 220.97 MiB)

### Last commit :star:

```git
commit 2925fcd4bc93f8d9672c713d9cda1818ae1c1fc5
Author:     Dan Klishch <danilklishch@gmail.com>
AuthorDate: Wed Apr 24 16:30:16 2024 -0400
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Fri Apr 26 19:08:13 2024 -0600

    Userland: Compile a special version of LibELF for DynamicLoader
    
    ... instead of shamelessly stealing its sources.
```
