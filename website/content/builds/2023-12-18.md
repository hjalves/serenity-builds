---
title: 'SerenityOS build: Monday, December 18'
date: 2023-12-18T06:11:08Z
author: Buildbot
description: |
  Commit: bd08b1815f (LibWeb: Implement border radius corner clipping in GPU painter, 2023-12-17)

  - [serenity-x86_64-20231218-bd08b18.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231218-bd08b18.img.gz) (Raw image, 206.54 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231218-bd08b18.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231218-bd08b18.img.gz) (Raw image, 206.54 MiB)

### Last commit :star:

```git
commit bd08b1815ffc9f973f77dddec26aacb30665f741
Author:     Aliaksandr Kalenik <kalenik.aliaksandr@gmail.com>
AuthorDate: Sun Dec 17 14:34:44 2023 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Sun Dec 17 23:12:48 2023 +0100

    LibWeb: Implement border radius corner clipping in GPU painter
    
    It is implemented in the way identical to how it works in CPU painter:
    1. SampleUnderCorners command saves pixels within corners into a
       texture.
    2. BlitCornerClipping command uses the texture prepared earlier to
       restore pixels within corners.
```
