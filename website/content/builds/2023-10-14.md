---
title: 'SerenityOS build: Saturday, October 14'
date: 2023-10-14T05:09:20Z
author: Buildbot
description: |
  Commit: d2c7e1ea7d (Browser: Sanitize user-provided URLs with LibWebView, 2023-10-13)

  - [serenity-x86_64-20231014-d2c7e1e.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231014-d2c7e1e.img.gz) (Raw image, 190.01 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231014-d2c7e1e.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231014-d2c7e1e.img.gz) (Raw image, 190.01 MiB)

### Last commit :star:

```git
commit d2c7e1ea7db060c1f01a740fab65f3e35a45545b
Author:     Timothy Flynn <trflynn89@pm.me>
AuthorDate: Fri Oct 13 10:31:52 2023 -0400
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Fri Oct 13 13:37:11 2023 -0400

    Browser: Sanitize user-provided URLs with LibWebView
```
