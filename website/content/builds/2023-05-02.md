---
title: 'SerenityOS build: Tuesday, May 02'
date: 2023-05-02T05:04:20Z
author: Buildbot
description: |
  Commit: 8d446d2a9e (LibWeb: Make XMLDocumentBuilder create elements with the HTML namespace, 2023-05-01)

  - [serenity-x86_64-20230502-8d446d2.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230502-8d446d2.img.gz) (Raw image, 180.56 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230502-8d446d2.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230502-8d446d2.img.gz) (Raw image, 180.56 MiB)

### Last commit :star:

```git
commit 8d446d2a9e7732f7badfa00e98fc130d5faee5dc
Author:     Ali Mohammad Pur <mpfard@serenityos.org>
AuthorDate: Mon May 1 15:26:37 2023 +0330
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Mon May 1 15:22:55 2023 +0200

    LibWeb: Make XMLDocumentBuilder create elements with the HTML namespace
    
    Otherwise we'll end up with all-generic elements and not run any special
    HTML sauce logic, leading to very plain pages.
```
