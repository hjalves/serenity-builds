---
title: 'SerenityOS build: Wednesday, January 24'
date: 2024-01-24T06:12:12Z
author: Buildbot
description: |
  Commit: 11d2e6101f (Meta: Add AK and LibRIFF to macOS bundles in GN build, 2024-01-23)

  - [serenity-x86_64-20240124-11d2e61.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240124-11d2e61.img.gz) (Raw image, 212.33 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240124-11d2e61.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240124-11d2e61.img.gz) (Raw image, 212.33 MiB)

### Last commit :star:

```git
commit 11d2e6101ffdd01fce8b571997111c7a6509f57f
Author:     Timothy Flynn <trflynn89@pm.me>
AuthorDate: Tue Jan 23 09:05:55 2024 -0500
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Tue Jan 23 14:07:46 2024 -0700

    Meta: Add AK and LibRIFF to macOS bundles in GN build
```
