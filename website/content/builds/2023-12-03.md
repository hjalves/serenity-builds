---
title: 'SerenityOS build: Sunday, December 03'
date: 2023-12-03T06:10:26Z
author: Buildbot
description: |
  Commit: f976ec005c (LibWeb: Port DOM::Document from DeprecatedString, 2023-12-03)

  - [serenity-x86_64-20231203-f976ec0.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231203-f976ec0.img.gz) (Raw image, 204.62 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231203-f976ec0.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231203-f976ec0.img.gz) (Raw image, 204.62 MiB)

### Last commit :star:

```git
commit f976ec005c25e10c68961798aee248a403c1e296
Author:     Shannon Booth <shannon@serenityos.org>
AuthorDate: Sun Dec 3 08:58:43 2023 +1300
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Sat Dec 2 22:54:53 2023 +0100

    LibWeb: Port DOM::Document from DeprecatedString
```
