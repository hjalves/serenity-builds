---
title: 'SerenityOS build: Saturday, April 22'
date: 2023-04-22T05:03:53Z
author: Buildbot
description: |
  Commit: cf3835b29b (LibGfx/JPEG: Make non-zero-terminated APPn starts non-fatal, 2023-04-21)

  - [serenity-x86_64-20230422-cf3835b.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230422-cf3835b.img.gz) (Raw image, 179.92 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230422-cf3835b.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230422-cf3835b.img.gz) (Raw image, 179.92 MiB)

### Last commit :star:

```git
commit cf3835b29bc13b1713ef511b6948ba247b4e7594
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Fri Apr 21 10:29:58 2023 -0400
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Fri Apr 21 22:09:31 2023 -0400

    LibGfx/JPEG: Make non-zero-terminated APPn starts non-fatal
    
    Necessary but not sufficient for #18456.
```
