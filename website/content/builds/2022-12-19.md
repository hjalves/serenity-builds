---
title: 'SerenityOS build: Monday, December 19'
date: 2022-12-19T03:58:41Z
author: Buildbot
description: |
  Commit: 1a85539741 (Meta: Find mke2fs on macOS even if HOMEBREW_PREFIX is not set, 2022-12-18)

  - [serenity-x86_64-20221219-1a85539.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221219-1a85539.img.gz) (Raw image, 144.74 MiB)
  - [serenity-x86_64-20221219-1a85539.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221219-1a85539.vdi.gz) (VirtualBox VDI, 144.43 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221219-1a85539.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221219-1a85539.img.gz) (Raw image, 144.74 MiB)
- [serenity-x86_64-20221219-1a85539.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221219-1a85539.vdi.gz) (VirtualBox VDI, 144.43 MiB)

### Last commit :star:

```git
commit 1a855397410ce6d78c67e4d3828ad3f7f78a3b73
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Sun Dec 18 18:56:47 2022 -0500
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Sun Dec 18 17:49:49 2022 -0700

    Meta: Find mke2fs on macOS even if HOMEBREW_PREFIX is not set
    
    ...as long as `brew` is on the path and `brew --prefix e2fsprogs`
    tells us where it is.
    
    This is for people who don't have `"$(brew shellenv)"` in their .zshrc.
```
