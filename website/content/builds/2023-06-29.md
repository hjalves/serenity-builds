---
title: 'SerenityOS build: Thursday, June 29'
date: 2023-06-29T05:06:20Z
author: Buildbot
description: |
  Commit: 55faff80df (Kernet/Net: Close a TCP connection using FIN|ACK instead of just FIN, 2023-06-28)

  - [serenity-x86_64-20230629-55faff8.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230629-55faff8.img.gz) (Raw image, 185.71 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230629-55faff8.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230629-55faff8.img.gz) (Raw image, 185.71 MiB)

### Last commit :star:

```git
commit 55faff80df2b2be5b0d5f1bd87f9ddced7e3a30e
Author:     Pierre Delagrave <pierre.delagrave@gmail.com>
AuthorDate: Wed Jun 28 18:40:44 2023 -0400
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Thu Jun 29 05:58:03 2023 +0200

    Kernet/Net: Close a TCP connection using FIN|ACK instead of just FIN
    
    When initiating a connection termination, the FIN should be sent with
    a ACK from the last received segment even if that ACK already been sent.
```
