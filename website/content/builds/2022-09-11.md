---
title: 'SerenityOS build: Sunday, September 11'
date: 2022-09-11T05:04:15Z
author: Buildbot
description: |
  Commit: 1346a653e4 (WidgetGallery: Port file picker to use FileSystemAccessClient, 2022-09-10)

  - [serenity-i686-20220911-1346a65.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220911-1346a65.img.gz) (Raw image, 127.5 MiB)
  - [serenity-i686-20220911-1346a65.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220911-1346a65.vdi.gz) (VirtualBox VDI, 127.31 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220911-1346a65.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220911-1346a65.img.gz) (Raw image, 127.5 MiB)
- [serenity-i686-20220911-1346a65.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220911-1346a65.vdi.gz) (VirtualBox VDI, 127.31 MiB)

### Last commit :star:

```git
commit 1346a653e48b5f734e3956bf8d05223bd8c2937a
Author:     networkException <git@nwex.de>
AuthorDate: Sat Sep 10 13:12:35 2022 +0200
Commit:     Sam Atkins <atkinssj@serenityos.org>
CommitDate: Sat Sep 10 20:41:16 2022 +0100

    WidgetGallery: Port file picker to use FileSystemAccessClient
    
    Previously we would unveil the home directory of anon to allow showing
    anything in the file picker. This patch removes direct access to the
    home directory and instead makes WidgetGallery connect to
    FileSystemAccessServer to open a file, making the application more user
    agnostic and allowing directories outside /home/anon to be shown.
```
