---
title: 'SerenityOS build: Monday, January 29'
date: 2024-01-29T06:12:13Z
author: Buildbot
description: |
  Commit: e1d1aac7bc (LibJS/Bytecode: Apply BigInt/Symbol ToObject avoidance in another place, 2024-01-28)

  - [serenity-x86_64-20240129-e1d1aac.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240129-e1d1aac.img.gz) (Raw image, 213.27 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240129-e1d1aac.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240129-e1d1aac.img.gz) (Raw image, 213.27 MiB)

### Last commit :star:

```git
commit e1d1aac7bcc37e35d34c87ca900399d6c4c45b17
Author:     Linus Groh <mail@linusgroh.de>
AuthorDate: Sun Jan 28 22:33:19 2024 +0000
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Sun Jan 28 19:49:51 2024 -0500

    LibJS/Bytecode: Apply BigInt/Symbol ToObject avoidance in another place
    
    Same as d667721b2 but in a different place.
```
