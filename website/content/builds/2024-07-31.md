---
title: 'SerenityOS build: Wednesday, July 31'
date: 2024-07-31T05:21:59Z
author: Buildbot
description: |
  Commit: 26555baf53 (Kernel/USB: Make USBHubDescriptor::hub_characteristics union packed, 2024-07-30)

  - [serenity-x86_64-20240731-26555ba.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240731-26555ba.img.gz) (Raw image, 221.3 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240731-26555ba.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240731-26555ba.img.gz) (Raw image, 221.3 MiB)

### Last commit :star:

```git
commit 26555baf53fd89622c564d3e87582088a3fb09e8
Author:     Sönke Holz <sholz8530@gmail.com>
AuthorDate: Tue Jul 30 12:10:19 2024 +0200
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Tue Jul 30 09:07:32 2024 -0400

    Kernel/USB: Make USBHubDescriptor::hub_characteristics union packed
    
    AArch64 Clang otherwise complains that this anonymous union is more
    aligned than the struct.
```
