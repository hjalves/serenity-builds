---
title: 'SerenityOS build: Wednesday, May 31'
date: 2023-05-31T05:04:03Z
author: Buildbot
description: |
  Commit: f5da6d61b4 (pgrep: Add `-x` option to only select exact matches, 2023-05-30)

  - [serenity-x86_64-20230531-f5da6d6.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230531-f5da6d6.img.gz) (Raw image, 183.88 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230531-f5da6d6.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230531-f5da6d6.img.gz) (Raw image, 183.88 MiB)

### Last commit :star:

```git
commit f5da6d61b43e38b02c22b35f074966c1a36cd797
Author:     Tim Ledbetter <timledbetter@gmail.com>
AuthorDate: Tue May 30 17:51:30 2023 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Wed May 31 06:04:48 2023 +0200

    pgrep: Add `-x` option to only select exact matches
```
