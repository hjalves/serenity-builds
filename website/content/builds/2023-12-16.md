---
title: 'SerenityOS build: Saturday, December 16'
date: 2023-12-16T06:11:14Z
author: Buildbot
description: |
  Commit: 1f171cb60b (LibWeb: Add support for the meta key modifier, 2023-12-15)

  - [serenity-x86_64-20231216-1f171cb.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231216-1f171cb.img.gz) (Raw image, 206.62 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231216-1f171cb.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231216-1f171cb.img.gz) (Raw image, 206.62 MiB)

### Last commit :star:

```git
commit 1f171cb60b0954af5f54913434c0e70526840f47
Author:     Bastiaan van der Plaat <bastiaan.v.d.plaat@gmail.com>
AuthorDate: Fri Dec 15 17:31:21 2023 +0100
Commit:     Sam Atkins <atkinssj@serenityos.org>
CommitDate: Fri Dec 15 23:19:23 2023 +0000

    LibWeb: Add support for the meta key modifier
```
