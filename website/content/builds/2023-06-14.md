---
title: 'SerenityOS build: Wednesday, June 14'
date: 2023-06-14T05:04:14Z
author: Buildbot
description: |
  Commit: bd26d022ac (LibWeb: Implement FileAPI::Blob::stream(), 2023-06-13)

  - [serenity-x86_64-20230614-bd26d02.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230614-bd26d02.img.gz) (Raw image, 184.27 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230614-bd26d02.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230614-bd26d02.img.gz) (Raw image, 184.27 MiB)

### Last commit :star:

```git
commit bd26d022ac294a17d9c1966880f74dff2debfb8e
Author:     Shannon Booth <shannon.ml.booth@gmail.com>
AuthorDate: Tue Jun 13 07:36:00 2023 +1200
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Wed Jun 14 06:27:04 2023 +0200

    LibWeb: Implement FileAPI::Blob::stream()
```
