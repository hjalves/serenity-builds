---
title: 'SerenityOS build: Tuesday, February 14'
date: 2023-02-14T06:06:00Z
author: Buildbot
description: |
  Commit: 617d112780 (Meta: Add a `Shell --posix` parser fuzzer, 2023-02-12)

  - [serenity-x86_64-20230214-617d112.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230214-617d112.img.gz) (Raw image, 163.08 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230214-617d112.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230214-617d112.img.gz) (Raw image, 163.08 MiB)

### Last commit :star:

```git
commit 617d11278016831bc8f2256a226fca5b5e94dc9c
Author:     Ali Mohammad Pur <mpfard@serenityos.org>
AuthorDate: Sun Feb 12 17:54:30 2023 +0330
Commit:     Ali Mohammad Pur <mpfard@serenityos.org>
CommitDate: Mon Feb 13 23:00:15 2023 +0330

    Meta: Add a `Shell --posix` parser fuzzer
```
