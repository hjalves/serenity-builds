---
title: 'SerenityOS build: Tuesday, September 19'
date: 2023-09-19T06:17:04Z
author: Buildbot
description: |
  Commit: edd2f8b37f (LibJS: Run reusable steps of FunctionDeclarationInstantiation only once, 2023-09-19)

  - [serenity-x86_64-20230919-edd2f8b.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230919-edd2f8b.img.gz) (Raw image, 186.31 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230919-edd2f8b.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230919-edd2f8b.img.gz) (Raw image, 186.31 MiB)

### Last commit :star:

```git
commit edd2f8b37f345508862df56d96b290c3a200648a
Author:     Aliaksandr Kalenik <kalenik.aliaksandr@gmail.com>
AuthorDate: Tue Sep 19 02:46:28 2023 +0200
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Tue Sep 19 06:54:10 2023 +0200

    LibJS: Run reusable steps of FunctionDeclarationInstantiation only once
    
    This change moves steps that can be executed only once and then reused
    in subsequent function instantiations from
    `function_declaration_instantiation` to the ECMAScriptFunctionObject:
    - Determine if there are any parameters with duplicate names.
    - Determine if there are any parameters with expressions.
    - Determine if an arguments object needs to be created.
    - Create a list of distinct function names for which bindings need to
      be created.
    - Create a list of distinct variable names for which bindings need to
      be created.
    
    This change makes React-Redux-TodoMVC test in Speedometer
    run 10% faster :)
```
