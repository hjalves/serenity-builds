---
title: 'SerenityOS build: Saturday, October 08'
date: 2022-10-08T04:07:20Z
author: Buildbot
description: |
  Commit: a0d5724a58 (LibWeb: Add initial implementation of Element.blur(), 2022-10-02)

  - [serenity-x86_64-20221008-a0d5724.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221008-a0d5724.img.gz) (Raw image, 134.39 MiB)
  - [serenity-x86_64-20221008-a0d5724.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221008-a0d5724.vdi.gz) (VirtualBox VDI, 134.04 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221008-a0d5724.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221008-a0d5724.img.gz) (Raw image, 134.39 MiB)
- [serenity-x86_64-20221008-a0d5724.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221008-a0d5724.vdi.gz) (VirtualBox VDI, 134.04 MiB)

### Last commit :star:

```git
commit a0d5724a58a0ce130a4712fc4a8bfa4b9636fd3b
Author:     Andrew Kaster <akaster@serenityos.org>
AuthorDate: Sun Oct 2 14:42:47 2022 -0600
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Fri Oct 7 21:17:50 2022 +0100

    LibWeb: Add initial implementation of Element.blur()
    
    This implementation includes a first cut at run the unfocusing steps
    from the spec, with many things left unimplemented.
    
    The viewport related spec steps in particular don't seem to map to
    LibWeb concepts, which makes figuring out if things are properly focused
    much more difficult.
```
