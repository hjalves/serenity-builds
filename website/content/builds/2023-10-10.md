---
title: 'SerenityOS build: Tuesday, October 10'
date: 2023-10-10T05:08:06Z
author: Buildbot
description: |
  Commit: bc6638682d (LibGfx/BMPLoader: Ensure DIB size and offset are within expected range, 2023-10-08)

  - [serenity-x86_64-20231010-bc66386.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231010-bc66386.img.gz) (Raw image, 187.55 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231010-bc66386.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231010-bc66386.img.gz) (Raw image, 187.55 MiB)

### Last commit :star:

```git
commit bc6638682da6d8eca8267e603dfec84b48832902
Author:     Tim Ledbetter <timledbetter@gmail.com>
AuthorDate: Sun Oct 8 13:53:51 2023 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Tue Oct 10 05:50:02 2023 +0200

    LibGfx/BMPLoader: Ensure DIB size and offset are within expected range
```
