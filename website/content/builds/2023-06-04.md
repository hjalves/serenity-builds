---
title: 'SerenityOS build: Sunday, June 04'
date: 2023-06-04T05:05:25Z
author: Buildbot
description: |
  Commit: e853139cf0 (LibGfx: Fix adding active edges in filled path rasterizer, 2023-06-03)

  - [serenity-x86_64-20230604-e853139.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230604-e853139.img.gz) (Raw image, 183.64 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230604-e853139.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230604-e853139.img.gz) (Raw image, 183.64 MiB)

### Last commit :star:

```git
commit e853139cf0d2903c9173e6327908c6a57e27e409
Author:     MacDue <macdue@dueutil.tech>
AuthorDate: Sat Jun 3 23:47:31 2023 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Sun Jun 4 05:40:39 2023 +0200

    LibGfx: Fix adding active edges in filled path rasterizer
    
    This was previously masked by sorting the edges on max_y, but if the
    last added edge pointed to an edge that ended on the current scanline,
    that edge (and what it points to) would also end up added to the active
    edges. These edges would then never get removed, and break things very
    badly!
```
