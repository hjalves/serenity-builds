---
title: 'SerenityOS build: Friday, June 30'
date: 2023-06-30T05:06:52Z
author: Buildbot
description: |
  Commit: bfd6deed1e (AK+Meta: Disable consteval completely when building for oss-fuzz, 2023-06-29)

  - [serenity-x86_64-20230630-bfd6dee.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230630-bfd6dee.img.gz) (Raw image, 185.72 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230630-bfd6dee.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230630-bfd6dee.img.gz) (Raw image, 185.72 MiB)

### Last commit :star:

```git
commit bfd6deed1e860ce9516c7ca1613150a2c2c75f75
Author:     Andrew Kaster <akaster@serenityos.org>
AuthorDate: Thu Jun 29 14:35:03 2023 -0600
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Thu Jun 29 15:55:54 2023 -0600

    AK+Meta: Disable consteval completely when building for oss-fuzz
    
    This was missed in 02b74e5a70e1eef2fe3123fe8aa157301424783a
    
    We need to disable consteval in AK::String as well as AK::StringView,
    and we need to disable it when building both the tools build and the
    fuzzer build.
```
