---
title: 'SerenityOS build: Thursday, January 26'
date: 2023-01-26T06:09:27Z
author: Buildbot
description: |
  Commit: 95c469ca4c (Kernel: Move Aarch64 MMU debug message into memory manager initializer, 2023-01-08)

  - [serenity-x86_64-20230126-95c469c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230126-95c469c.img.gz) (Raw image, 688.06 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230126-95c469c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230126-95c469c.img.gz) (Raw image, 688.06 MiB)

### Last commit :star:

```git
commit 95c469ca4c34d6bea2c546f722352852d5887cb1
Author:     konrad <konrad@serenityos.org>
AuthorDate: Sun Jan 8 05:31:43 2023 +0100
Commit:     Jelle Raaijmakers <jelle@gmta.nl>
CommitDate: Wed Jan 25 23:17:36 2023 +0100

    Kernel: Move Aarch64 MMU debug message into memory manager initializer
    
    Doing so unifies startup debug messages visually.
```
