---
title: 'SerenityOS build: Wednesday, February 08'
date: 2023-02-08T06:06:13Z
author: Buildbot
description: |
  Commit: 83b0613c68 (LibWeb: Null check `nearest_sibling` in `generate_missing_parents`, 2023-02-07)

  - [serenity-x86_64-20230208-83b0613.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230208-83b0613.img.gz) (Raw image, 157.74 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230208-83b0613.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230208-83b0613.img.gz) (Raw image, 157.74 MiB)

### Last commit :star:

```git
commit 83b0613c688bd943e58c86bdaae177ca24f252ee
Author:     Aliaksandr Kalenik <kalenik.aliaksandr@gmail.com>
AuthorDate: Tue Feb 7 01:33:10 2023 +0300
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Tue Feb 7 17:04:38 2023 +0100

    LibWeb: Null check `nearest_sibling` in `generate_missing_parents`
    
    Caught by AddressSanitizer.
```
