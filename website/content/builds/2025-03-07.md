---
title: 'SerenityOS build: Friday, March 07'
date: 2025-03-07T06:23:17Z
author: Buildbot
description: |
  Commit: 9218689698 (Kernel: Include the command set in the storage device dump, 2025-03-06)

  - [serenity-x86_64-20250307-9218689.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250307-9218689.img.gz) (Raw image, 242.48 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20250307-9218689.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250307-9218689.img.gz) (Raw image, 242.48 MiB)

### Last commit :star:

```git
commit 9218689698e4c195584b2dbfcb4a5e5e0082df37
Author:     Sönke Holz <sholz8530@gmail.com>
AuthorDate: Thu Mar 6 16:07:55 2025 +0100
Commit:     Sönke Holz <sholz8530@gmail.com>
CommitDate: Fri Mar 7 01:43:28 2025 +0100

    Kernel: Include the command set in the storage device dump
    
    This helps identifying e.g. NVMe drives as NVMe drives in this dump.
```
