---
title: 'SerenityOS build: Thursday, April 06'
date: 2023-04-06T05:02:56Z
author: Buildbot
description: |
  Commit: 73c291f5ae (LibGfx: Pass in format and size to webp image decoding function, 2023-04-04)

  - [serenity-x86_64-20230406-73c291f.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230406-73c291f.img.gz) (Raw image, 175.06 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230406-73c291f.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230406-73c291f.img.gz) (Raw image, 175.06 MiB)

### Last commit :star:

```git
commit 73c291f5aea2583f8c2bc5dac8a5474d6ea0e5d9
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Tue Apr 4 09:48:48 2023 -0400
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Thu Apr 6 00:16:52 2023 +0100

    LibGfx: Pass in format and size to webp image decoding function
```
