---
title: 'SerenityOS build: Sunday, December 04'
date: 2022-12-04T03:57:24Z
author: Buildbot
description: |
  Commit: eb44a90e62 (Documentation: Recommend CLion code style settings over manual steps, 2022-11-25)

  - [serenity-x86_64-20221204-eb44a90.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221204-eb44a90.img.gz) (Raw image, 142.46 MiB)
  - [serenity-x86_64-20221204-eb44a90.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221204-eb44a90.vdi.gz) (VirtualBox VDI, 142.13 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221204-eb44a90.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221204-eb44a90.img.gz) (Raw image, 142.46 MiB)
- [serenity-x86_64-20221204-eb44a90.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221204-eb44a90.vdi.gz) (VirtualBox VDI, 142.13 MiB)

### Last commit :star:

```git
commit eb44a90e628be066dbfe685e19d3b64f977dcef1
Author:     Andreas Oppebøen <andreas.oppeboen@gmail.com>
AuthorDate: Fri Nov 25 00:03:57 2022 +0100
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Sat Dec 3 23:54:48 2022 +0000

    Documentation: Recommend CLion code style settings over manual steps
    
    Changing the naming conventions one-by-one was tedious and error-prone.
    A settings file is likely to be more forward compatible than a
    screenshot. The settings file was made by repeating the manual steps
    provided in the documentation, and exporting the file in CLion.
```
