---
title: 'SerenityOS build: Sunday, November 13'
date: 2022-11-13T03:59:44Z
author: Buildbot
description: |
  Commit: 9dc622475e (LibVideo: Rename parse_tree_new to parse_tree in VP9/TreeParser.cpp, 2022-11-07)

  - [serenity-x86_64-20221113-9dc6224.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221113-9dc6224.img.gz) (Raw image, 139.74 MiB)
  - [serenity-x86_64-20221113-9dc6224.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221113-9dc6224.vdi.gz) (VirtualBox VDI, 139.44 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221113-9dc6224.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221113-9dc6224.img.gz) (Raw image, 139.74 MiB)
- [serenity-x86_64-20221113-9dc6224.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221113-9dc6224.vdi.gz) (VirtualBox VDI, 139.44 MiB)

### Last commit :star:

```git
commit 9dc622475eba17ef3a3d7ea146961d66a2397317
Author:     Zaggy1024 <zaggy1024@gmail.com>
AuthorDate: Mon Nov 7 18:48:57 2022 -0600
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Sat Nov 12 10:17:27 2022 -0700

    LibVideo: Rename parse_tree_new to parse_tree in VP9/TreeParser.cpp
    
    It is now the only function used to parse the binary trees in the VP9
    decoder.
```
