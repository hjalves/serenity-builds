---
title: 'SerenityOS build: Friday, June 21'
date: 2024-06-21T05:18:46Z
author: Buildbot
description: |
  Commit: a711bb63b7 (LibHTTP+LibCore+RequestServer: Use async streams for HTTP, 2024-06-11)

  - [serenity-x86_64-20240621-a711bb6.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240621-a711bb6.img.gz) (Raw image, 219.7 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240621-a711bb6.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240621-a711bb6.img.gz) (Raw image, 219.7 MiB)

### Last commit :star:

```git
commit a711bb63b7801780f6a68dac41c438bbb17ca045
Author:     Ali Mohammad Pur <mpfard@serenityos.org>
AuthorDate: Tue Jun 11 17:38:21 2024 +0200
Commit:     Ali Mohammad Pur <mpfard@serenityos.org>
CommitDate: Wed Jun 19 15:45:02 2024 +0200

    LibHTTP+LibCore+RequestServer: Use async streams for HTTP
    
    We no longer use blocking reads and writes, yielding a nice performance
    boost and a significantly cleaner (and more readable) implementation :^)
```
