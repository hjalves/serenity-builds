---
title: 'SerenityOS build: Monday, August 28'
date: 2023-08-28T05:08:03Z
author: Buildbot
description: |
  Commit: 5d7e73adfe (Meta: Add and document convenient method to build non-Qt Ladybird chrome, 2023-08-25)

  - [serenity-x86_64-20230828-5d7e73a.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230828-5d7e73a.img.gz) (Raw image, 185.87 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230828-5d7e73a.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230828-5d7e73a.img.gz) (Raw image, 185.87 MiB)

### Last commit :star:

```git
commit 5d7e73adfe2aafa04240241894267aee2c86f2d0
Author:     Timothy Flynn <trflynn89@pm.me>
AuthorDate: Fri Aug 25 07:05:57 2023 -0400
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Sun Aug 27 19:02:36 2023 -0400

    Meta: Add and document convenient method to build non-Qt Ladybird chrome
    
    This lets us switch between the AppKit and Qt chromes more easily.
```
