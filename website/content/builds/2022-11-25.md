---
title: 'SerenityOS build: Friday, November 25'
date: 2022-11-25T03:57:39Z
author: Buildbot
description: |
  Commit: 1944e8936d (cat: Return a non-null value if an error occurred, 2022-11-24)

  - [serenity-x86_64-20221125-1944e89.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221125-1944e89.img.gz) (Raw image, 141.16 MiB)
  - [serenity-x86_64-20221125-1944e89.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221125-1944e89.vdi.gz) (VirtualBox VDI, 140.82 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221125-1944e89.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221125-1944e89.img.gz) (Raw image, 141.16 MiB)
- [serenity-x86_64-20221125-1944e89.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221125-1944e89.vdi.gz) (VirtualBox VDI, 140.82 MiB)

### Last commit :star:

```git
commit 1944e8936d72a93e42911f3b42a3dbb40536b060
Author:     Lucas CHOLLET <lucas.chollet@free.fr>
AuthorDate: Thu Nov 24 16:04:29 2022 +0100
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Thu Nov 24 21:01:54 2022 -0500

    cat: Return a non-null value if an error occurred
```
