---
title: 'SerenityOS build: Wednesday, January 10'
date: 2024-01-10T06:11:54Z
author: Buildbot
description: |
  Commit: 9507157f04 (Escalator: Port Escalator to GML Compiler, 2024-01-09)

  - [serenity-x86_64-20240110-9507157.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240110-9507157.img.gz) (Raw image, 209.17 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240110-9507157.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240110-9507157.img.gz) (Raw image, 209.17 MiB)

### Last commit :star:

```git
commit 9507157f0412b2328b19190e2027417916b3ae64
Author:     Mr.UNIX <mrunix00@protonmail.com>
AuthorDate: Tue Jan 9 23:07:42 2024 +0100
Commit:     Tim Schumacher <timschumi@gmx.de>
CommitDate: Wed Jan 10 00:03:27 2024 +0100

    Escalator: Port Escalator to GML Compiler
```
