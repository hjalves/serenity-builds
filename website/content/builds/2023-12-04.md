---
title: 'SerenityOS build: Monday, December 04'
date: 2023-12-04T06:10:13Z
author: Buildbot
description: |
  Commit: 6d743ce9e8 (LibWebView: Allow editing the DOM through the Inspector WebView, 2023-12-03)

  - [serenity-x86_64-20231204-6d743ce.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231204-6d743ce.img.gz) (Raw image, 204.83 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231204-6d743ce.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231204-6d743ce.img.gz) (Raw image, 204.83 MiB)

### Last commit :star:

```git
commit 6d743ce9e8481b5a3767d6cef0aa10afe342613c
Author:     Timothy Flynn <trflynn89@pm.me>
AuthorDate: Sun Dec 3 16:01:19 2023 -0500
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Mon Dec 4 01:33:57 2023 +0100

    LibWebView: Allow editing the DOM through the Inspector WebView
    
    This allows a limited amount of DOM manipulation through the Inspector.
    Users may edit node tag names, text content, and attributes. To initiate
    an edit, double-click the tag/text/attribute of interest.
    
    To remove an attribute, begin editing the attribute and remove all of
    its text. To add an attribute, begin editing an existing attribute and
    add the new attribute's text before or after the existing attribute's
    text. This isn't going to be the final UX, but works for now just as a
    consequence of how attribute changes are implemented. A future patch
    will add more explicit add/delete actions.
```
