---
title: 'SerenityOS build: Thursday, May 02'
date: 2024-05-02T05:17:13Z
author: Buildbot
description: |
  Commit: 46b8a3afb7 (LibWeb: Move the read bytes into ReadLoopReadRequest's success callback, 2024-04-30)

  - [serenity-x86_64-20240502-46b8a3a.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240502-46b8a3a.img.gz) (Raw image, 220.99 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240502-46b8a3a.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240502-46b8a3a.img.gz) (Raw image, 220.99 MiB)

### Last commit :star:

```git
commit 46b8a3afb70d4e40c112fef69efa656c0e575764
Author:     Timothy Flynn <trflynn89@pm.me>
AuthorDate: Tue Apr 30 07:07:38 2024 -0400
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Wed May 1 21:46:45 2024 +0200

    LibWeb: Move the read bytes into ReadLoopReadRequest's success callback
    
    The callback is already specified to take the bytes by value. No need to
    copy the bytes here.
```
