---
title: 'SerenityOS build: Tuesday, November 07'
date: 2023-11-07T06:09:36Z
author: Buildbot
description: |
  Commit: a1cdfe1b54 (Shell: Add support for showing shortened prompt path with ellipsis, 2023-11-03)

  - [serenity-x86_64-20231107-a1cdfe1.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231107-a1cdfe1.img.gz) (Raw image, 201.71 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231107-a1cdfe1.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231107-a1cdfe1.img.gz) (Raw image, 201.71 MiB)

### Last commit :star:

```git
commit a1cdfe1b5411ad6c659fe53e2a69261a3201d951
Author:     Adam Harald Jørgensen <58829763+adamjoer@users.noreply.github.com>
AuthorDate: Fri Nov 3 15:24:18 2023 +0100
Commit:     Ali Mohammad Pur <mpfard@serenityos.org>
CommitDate: Tue Nov 7 07:46:52 2023 +0330

    Shell: Add support for showing shortened prompt path with ellipsis
```
