---
title: 'SerenityOS build: Monday, June 05'
date: 2023-06-05T05:03:55Z
author: Buildbot
description: |
  Commit: 59cab85002 (Kernel: Rename Syscall.cpp => Syscalls/SyscallHandler.cpp, 2023-02-24)

  - [serenity-x86_64-20230605-59cab85.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230605-59cab85.img.gz) (Raw image, 183.64 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230605-59cab85.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230605-59cab85.img.gz) (Raw image, 183.64 MiB)

### Last commit :star:

```git
commit 59cab85002f0ae950c1325b093971945be73fae7
Author:     Liav A <liavalb@gmail.com>
AuthorDate: Fri Feb 24 22:18:34 2023 +0200
Commit:     Jelle Raaijmakers <jelle@gmta.nl>
CommitDate: Sun Jun 4 21:32:34 2023 +0200

    Kernel: Rename Syscall.cpp => Syscalls/SyscallHandler.cpp
```
