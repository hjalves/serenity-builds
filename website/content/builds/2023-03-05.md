---
title: 'SerenityOS build: Sunday, March 05'
date: 2023-03-05T06:01:33Z
author: Buildbot
description: |
  Commit: 2d27c98659 (AK: Implement Knuth's algorithm D for dividing UFixedBigInt's, 2023-02-04)

  - [serenity-x86_64-20230305-2d27c98.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230305-2d27c98.img.gz) (Raw image, 166.36 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230305-2d27c98.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230305-2d27c98.img.gz) (Raw image, 166.36 MiB)

### Last commit :star:

```git
commit 2d27c98659bef2fa69cf4033c26c330936f52dd8
Author:     Dan Klishch <danilklishch@gmail.com>
AuthorDate: Sat Feb 4 18:57:10 2023 +0300
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Sat Mar 4 22:10:03 2023 -0700

    AK: Implement Knuth's algorithm D for dividing UFixedBigInt's
```
