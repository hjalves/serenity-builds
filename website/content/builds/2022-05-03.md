---
title: 'SerenityOS build: Tuesday, May 03'
date: 2022-05-03T03:36:01Z
author: Buildbot
description: |
  Commit: 7b76bc2b49 (Ports: Update openssh to 9.0, 2022-04-30)

  - [serenity-i686-20220503-7b76bc2.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220503-7b76bc2.vdi.gz) (VirtualBox VDI, 122.65 MiB)
  - [serenity-i686-20220503-7b76bc2.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220503-7b76bc2.img.gz) (Raw image, 122.82 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220503-7b76bc2.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220503-7b76bc2.vdi.gz) (VirtualBox VDI, 122.65 MiB)
- [serenity-i686-20220503-7b76bc2.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220503-7b76bc2.img.gz) (Raw image, 122.82 MiB)

### Last commit :star:

```git
commit 7b76bc2b4983b5fc0c648c5ae0bd9e3f402518fc
Author:     Patrick Meyer <git@the-space.agency>
AuthorDate: Sat Apr 30 10:58:10 2022 +0000
Commit:     Brian Gianforcaro <bgianf@serenityos.org>
CommitDate: Mon May 2 17:12:51 2022 -0700

    Ports: Update openssh to 9.0
```
