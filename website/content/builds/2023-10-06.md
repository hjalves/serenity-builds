---
title: 'SerenityOS build: Friday, October 06'
date: 2023-10-06T05:10:52Z
author: Buildbot
description: |
  Commit: 03be26317f (LibJS: Alphabetize handling some Intl.NumberFormat/PluralRules options, 2023-10-04)

  - [serenity-x86_64-20231006-03be263.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231006-03be263.img.gz) (Raw image, 187.85 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231006-03be263.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231006-03be263.img.gz) (Raw image, 187.85 MiB)

### Last commit :star:

```git
commit 03be26317f0c6a4019c3c40a846dd51eacbfb8ce
Author:     Timothy Flynn <trflynn89@pm.me>
AuthorDate: Wed Oct 4 17:01:25 2023 -0400
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Thu Oct 5 17:01:02 2023 +0200

    LibJS: Alphabetize handling some Intl.NumberFormat/PluralRules options
    
    This is a normative change in the ECMA-402 spec. See:
    https://github.com/tc39/ecma402/commit/5a43090
```
