---
title: 'SerenityOS build: Thursday, July 07'
date: 2022-07-07T03:05:18Z
author: Buildbot
description: |
  Commit: 94509c9844 (LibWeb: Use correct margin & padding values in anonymous wrapper boxes, 2022-07-06)

  - [serenity-i686-20220707-94509c9.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220707-94509c9.vdi.gz) (VirtualBox VDI, 124.89 MiB)
  - [serenity-i686-20220707-94509c9.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220707-94509c9.img.gz) (Raw image, 125.08 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220707-94509c9.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220707-94509c9.vdi.gz) (VirtualBox VDI, 124.89 MiB)
- [serenity-i686-20220707-94509c9.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220707-94509c9.img.gz) (Raw image, 125.08 MiB)

### Last commit :star:

```git
commit 94509c984412d24014a342cdb4336a400794dc9a
Author:     Andreas Kling <kling@serenityos.org>
AuthorDate: Wed Jul 6 20:17:12 2022 +0200
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Wed Jul 6 20:31:27 2022 +0200

    LibWeb: Use correct margin & padding values in anonymous wrapper boxes
    
    Anonymous wrappers get their non-inherited properties from the initial
    state of a new CSS::ComputedValues object. Before this patch, all the
    values in their margin and padding LengthBox would be "auto".
```
