---
title: 'SerenityOS build: Tuesday, October 22'
date: 2024-10-22T05:25:34Z
author: Buildbot
description: |
  Commit: 6123113255 (Meta: Add SVGImageElement to GN build, 2024-10-21)

  - [serenity-x86_64-20241022-6123113.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241022-6123113.img.gz) (Raw image, 224.88 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20241022-6123113.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241022-6123113.img.gz) (Raw image, 224.88 MiB)

### Last commit :star:

```git
commit 6123113255b7aec0d7f9e81041c3229f534b49ba
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Mon Oct 21 07:40:30 2024 -0400
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Mon Oct 21 12:01:12 2024 -0400

    Meta: Add SVGImageElement to GN build
```
