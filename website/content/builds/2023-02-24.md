---
title: 'SerenityOS build: Friday, February 24'
date: 2023-02-24T06:17:19Z
author: Buildbot
description: |
  Commit: 0fb6f87d49 (LibWeb: Add spec link to border-width: thin/medium/thick definitions, 2023-02-23)

  - [serenity-x86_64-20230224-0fb6f87.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230224-0fb6f87.img.gz) (Raw image, 163.94 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230224-0fb6f87.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230224-0fb6f87.img.gz) (Raw image, 163.94 MiB)

### Last commit :star:

```git
commit 0fb6f87d49466330423ce3b8f2f7c30c2c41b46e
Author:     Sam Atkins <atkinssj@serenityos.org>
AuthorDate: Thu Feb 23 16:18:55 2023 +0000
Commit:     Sam Atkins <atkinssj@serenityos.org>
CommitDate: Thu Feb 23 16:20:44 2023 +0000

    LibWeb: Add spec link to border-width: thin/medium/thick definitions
    
    In the latest backgrounds-3 spec, these have official fixed values. :^)
```
