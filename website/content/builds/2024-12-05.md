---
title: 'SerenityOS build: Thursday, December 05'
date: 2024-12-05T06:25:46Z
author: Buildbot
description: |
  Commit: 4a52145f8e (LibWebView: Implement drag-and-drop for OutOfProcessWebView, 2024-12-02)

  - [serenity-x86_64-20241205-4a52145.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241205-4a52145.img.gz) (Raw image, 239.66 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20241205-4a52145.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241205-4a52145.img.gz) (Raw image, 239.66 MiB)

### Last commit :star:

```git
commit 4a52145f8e860c98e7f102cff872dab7da5a89ff
Author:     Timothy Flynn <trflynn89@pm.me>
AuthorDate: Mon Dec 2 09:10:33 2024 -0500
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Wed Dec 4 08:00:21 2024 -0500

    LibWebView: Implement drag-and-drop for OutOfProcessWebView
    
    Almost the entirety of drag-and-drop operations are handled within
    LibWeb. This patch just hooks up the UI-side operations to trigger
    the events.
    
    A FIXME is left here to open files in the browser that were not accepted
    by the web page.
```
