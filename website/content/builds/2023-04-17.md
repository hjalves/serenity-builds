---
title: 'SerenityOS build: Monday, April 17'
date: 2023-04-17T05:03:27Z
author: Buildbot
description: |
  Commit: d4e114a31e (Kernel: Remove unused functions related to reading full inodes, 2023-04-16)

  - [serenity-x86_64-20230417-d4e114a.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230417-d4e114a.img.gz) (Raw image, 178.87 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230417-d4e114a.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230417-d4e114a.img.gz) (Raw image, 178.87 MiB)

### Last commit :star:

```git
commit d4e114a31e322d96a62f2184f7180f79956bb2bf
Author:     Tim Schumacher <timschumi@gmx.de>
AuthorDate: Sun Apr 16 20:42:16 2023 +0200
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Mon Apr 17 01:20:23 2023 +0200

    Kernel: Remove unused functions related to reading full inodes
```
