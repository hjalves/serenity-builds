---
title: 'SerenityOS build: Wednesday, February 26'
date: 2025-02-26T06:23:26Z
author: Buildbot
description: |
  Commit: d21e7ccb52 (LibGfx+PixelPaint: Unify `Rect::centered_at` and `Rect::centered_on`, 2025-02-25)

  - [serenity-x86_64-20250226-d21e7ccb5.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250226-d21e7ccb5.img.gz) (Raw image, 242.11 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20250226-d21e7ccb5.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250226-d21e7ccb5.img.gz) (Raw image, 242.11 MiB)

### Last commit :star:

```git
commit d21e7ccb52e284686418e46760a51a388b7207f4
Author:     Vinicius Deolindo <andrade.vinicius934@gmail.com>
AuthorDate: Tue Feb 25 16:54:28 2025 -0300
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Tue Feb 25 19:06:21 2025 -0500

    LibGfx+PixelPaint: Unify `Rect::centered_at` and `Rect::centered_on`
    
    I have chosen `centered_on` to prevail for it is the older one.
```
