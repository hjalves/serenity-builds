---
title: 'SerenityOS build: Thursday, May 11'
date: 2023-05-11T05:03:26Z
author: Buildbot
description: |
  Commit: b98252728e (LibWeb: Fix percentage min/max sizes on flex items with intrinsic ratio, 2023-05-10)

  - [serenity-x86_64-20230511-b982527.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230511-b982527.img.gz) (Raw image, 181.61 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230511-b982527.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230511-b982527.img.gz) (Raw image, 181.61 MiB)

### Last commit :star:

```git
commit b98252728efaece9cdf8fb939cc98aa4c7ea59cf
Author:     Andreas Kling <kling@serenityos.org>
AuthorDate: Wed May 10 17:15:19 2023 +0200
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Wed May 10 18:46:04 2023 +0200

    LibWeb: Fix percentage min/max sizes on flex items with intrinsic ratio
    
    We were resolving percentage values against the containing block size in
    the wrong axis.
```
