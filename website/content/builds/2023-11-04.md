---
title: 'SerenityOS build: Saturday, November 04'
date: 2023-11-04T06:09:21Z
author: Buildbot
description: |
  Commit: a1f9d2420f (Fuzzers: Disable debug logging for all fuzzers, 2023-11-03)

  - [serenity-x86_64-20231104-a1f9d24.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231104-a1f9d24.img.gz) (Raw image, 199.94 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231104-a1f9d24.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231104-a1f9d24.img.gz) (Raw image, 199.94 MiB)

### Last commit :star:

```git
commit a1f9d2420f26a82e87013cee7d71c223e86a3c60
Author:     Tim Ledbetter <timledbetter@gmail.com>
AuthorDate: Fri Nov 3 17:49:54 2023 +0000
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Fri Nov 3 20:56:44 2023 -0600

    Fuzzers: Disable debug logging for all fuzzers
    
    Previously, some fuzzers were generating an excessive amount of debug
    logging. This change explicitly disables debug logging for all fuzzers.
    This allows higher test throughput and makes the logs easier to read
    when fuzzing locally.
```
