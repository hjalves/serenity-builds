---
title: 'SerenityOS build: Tuesday, April 18'
date: 2023-04-18T05:18:32Z
author: Buildbot
description: |
  Commit: eef07d2f57 (ldd: Pledge map_fixed, 2023-04-17)

  - [serenity-x86_64-20230418-eef07d2.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230418-eef07d2.img.gz) (Raw image, 178.88 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230418-eef07d2.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230418-eef07d2.img.gz) (Raw image, 178.88 MiB)

### Last commit :star:

```git
commit eef07d2f578727d3ac0b55ea221716ea1c2ce9a7
Author:     Romain Chardiny <romain.chardiny@gmail.com>
AuthorDate: Mon Apr 17 12:26:58 2023 +0200
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Mon Apr 17 19:29:20 2023 -0400

    ldd: Pledge map_fixed
```
