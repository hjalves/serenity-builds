---
title: 'SerenityOS build: Sunday, March 24'
date: 2024-03-24T06:15:31Z
author: Buildbot
description: |
  Commit: 259a84ddac (Tests/JBIG2: Add a test for symbol and text segment decoding, 2024-03-23)

  - [serenity-x86_64-20240324-259a84d.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240324-259a84d.img.gz) (Raw image, 219.54 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240324-259a84d.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240324-259a84d.img.gz) (Raw image, 219.54 MiB)

### Last commit :star:

```git
commit 259a84ddac2791c56b0beefa3e3a20d0e7006527
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Sat Mar 23 10:15:53 2024 -0400
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Sat Mar 23 17:30:15 2024 -0400

    Tests/JBIG2: Add a test for symbol and text segment decoding
```
