---
title: 'SerenityOS build: Monday, December 05'
date: 2022-12-05T03:57:17Z
author: Buildbot
description: |
  Commit: 3f38f61043 (Ports: Update serenity-theming app use latest commit, 2022-12-05)

  - [serenity-x86_64-20221205-3f38f61.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221205-3f38f61.vdi.gz) (VirtualBox VDI, 142.21 MiB)
  - [serenity-x86_64-20221205-3f38f61.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221205-3f38f61.img.gz) (Raw image, 142.53 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221205-3f38f61.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221205-3f38f61.vdi.gz) (VirtualBox VDI, 142.21 MiB)
- [serenity-x86_64-20221205-3f38f61.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221205-3f38f61.img.gz) (Raw image, 142.53 MiB)

### Last commit :star:

```git
commit 3f38f610439f60bceafd718855d4fe6c259198fb
Author:     djwisdom <djwisdom@gmail.com>
AuthorDate: Mon Dec 5 02:13:02 2022 +0800
Commit:     Brian Gianforcaro <bgianf@serenityos.org>
CommitDate: Sun Dec 4 14:02:18 2022 -0800

    Ports: Update serenity-theming app use latest commit
    
    Add fonts Hantschrift and Schwedische Schreibschrift
```
