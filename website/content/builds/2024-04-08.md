---
title: 'SerenityOS build: Monday, April 08'
date: 2024-04-08T05:16:53Z
author: Buildbot
description: |
  Commit: aada06757b (LibGfx/JBIG2: Add early out to composite_bitbuffer(), 2024-04-05)

  - [serenity-x86_64-20240408-aada067.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240408-aada067.img.gz) (Raw image, 221.1 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240408-aada067.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240408-aada067.img.gz) (Raw image, 221.1 MiB)

### Last commit :star:

```git
commit aada06757ba4e2837dee905145f998272d0949de
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Fri Apr 5 21:45:59 2024 -0700
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Mon Apr 8 06:27:51 2024 +0200

    LibGfx/JBIG2: Add early out to composite_bitbuffer()
    
    The halftone region decoding procedure can draw patterns completely
    outside the page bitmap. I haven't seen this in practice yet (not
    many files use pattern/halftone segments), but it seems like it's
    possible in theory and seems like a good thing to check for.
```
