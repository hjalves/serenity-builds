---
title: 'SerenityOS build: Tuesday, March 21'
date: 2023-03-21T06:03:01Z
author: Buildbot
description: |
  Commit: 03c225b023 (LibWeb: Move Element.prototype.style to ElementCSSInlineStyle mixin, 2023-03-20)

  - [serenity-x86_64-20230321-03c225b.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230321-03c225b.img.gz) (Raw image, 168.14 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230321-03c225b.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230321-03c225b.img.gz) (Raw image, 168.14 MiB)

### Last commit :star:

```git
commit 03c225b02309872398f977c9a25cc0ea75aff77b
Author:     Simon Wanner <simon+git@skyrising.xyz>
AuthorDate: Mon Mar 20 19:31:13 2023 +0100
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Mon Mar 20 20:37:40 2023 -0400

    LibWeb: Move Element.prototype.style to ElementCSSInlineStyle mixin
    
    Also adds the `PutForwards` extended attribute allowing setting the
    style property.
```
