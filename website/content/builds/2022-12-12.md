---
title: 'SerenityOS build: Monday, December 12'
date: 2022-12-12T03:58:24Z
author: Buildbot
description: |
  Commit: cda5a530e6 (Ports: Add zig port :^), 2022-09-25)

  - [serenity-x86_64-20221212-cda5a53.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221212-cda5a53.img.gz) (Raw image, 143.37 MiB)
  - [serenity-x86_64-20221212-cda5a53.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221212-cda5a53.vdi.gz) (VirtualBox VDI, 143.08 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221212-cda5a53.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221212-cda5a53.img.gz) (Raw image, 143.37 MiB)
- [serenity-x86_64-20221212-cda5a53.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221212-cda5a53.vdi.gz) (VirtualBox VDI, 143.08 MiB)

### Last commit :star:

```git
commit cda5a530e63872ee8700c518f51a4ea5dfba8604
Author:     sin-ack <sin-ack@users.noreply.github.com>
AuthorDate: Sun Sep 25 11:19:54 2022 +0000
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Sun Dec 11 19:55:37 2022 -0700

    Ports: Add zig port :^)
    
    :yakkie:
    
    The build process for the Zig compiler is more involved than most of
    the other ports, because the Zig compiler is mostly self-hosting. In
    order to build it, the zig-bootstrap build system is used, which does
    the following:
    
    1) Build LLVM for the host OS;
    2) Build Zig for the host OS with the SerenityOS target enabled;
    3) Build zlib, zstd and LLVM for SerenityOS using `zig cc` as the C/C++
       compiler;
    4) Build Zig for SerenityOS using the host Zig.
    
    A few hacks are required in order to tell `zig cc` and zig about what
    Serenity's libc looks like in the build process, but other than that
    it's fairly straightforward. All of the patches that are included with
    this commit are Zig-upstream ready once the LLVM patches are upstreamed.
```
