---
title: 'SerenityOS build: Monday, December 25'
date: 2023-12-25T06:11:00Z
author: Buildbot
description: |
  Commit: 37f2d49818 (CI: Bump github/codeql-action from 2 to 3, 2023-12-18)

  - [serenity-x86_64-20231225-37f2d49.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231225-37f2d49.img.gz) (Raw image, 206.13 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231225-37f2d49.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231225-37f2d49.img.gz) (Raw image, 206.13 MiB)

### Last commit :star:

```git
commit 37f2d49818a82387284c41845d7a76f40d5a6d24
Author:     dependabot[bot] <49699333+dependabot[bot]@users.noreply.github.com>
AuthorDate: Mon Dec 18 01:32:10 2023 +0000
Commit:     Jelle Raaijmakers <jelle@gmta.nl>
CommitDate: Sun Dec 24 23:56:34 2023 +0100

    CI: Bump github/codeql-action from 2 to 3
    
    Bumps [github/codeql-action](https://github.com/github/codeql-action) from 2 to 3.
    - [Release notes](https://github.com/github/codeql-action/releases)
    - [Changelog](https://github.com/github/codeql-action/blob/main/CHANGELOG.md)
    - [Commits](https://github.com/github/codeql-action/compare/v2...v3)
    
    ---
    updated-dependencies:
    - dependency-name: github/codeql-action
      dependency-type: direct:production
      update-type: version-update:semver-major
    ...
    
    Signed-off-by: dependabot[bot] <support@github.com>
```
