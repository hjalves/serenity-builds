---
title: 'SerenityOS build: Thursday, June 15'
date: 2023-06-15T05:04:29Z
author: Buildbot
description: |
  Commit: a8b9130ceb (LibWeb: Support SVG vertical/horizontal lineto with multiple parameters, 2023-06-14)

  - [serenity-x86_64-20230615-a8b9130.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230615-a8b9130.img.gz) (Raw image, 184.48 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230615-a8b9130.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230615-a8b9130.img.gz) (Raw image, 184.48 MiB)

### Last commit :star:

```git
commit a8b9130ceb373d2a9865f52c3eeb81317e9d104c
Author:     MacDue <macdue@dueutil.tech>
AuthorDate: Wed Jun 14 21:22:13 2023 +0100
Commit:     Jelle Raaijmakers <jelle@gmta.nl>
CommitDate: Thu Jun 15 00:10:53 2023 +0200

    LibWeb: Support SVG vertical/horizontal lineto with multiple parameters
    
    This now allows v 1 2 3 or h 1 2 3, which are treated like v 1 v 2 v 3
    and h 1 h 2 h 3 respectively. This fixes the freeCodeCamp SVG logo.
```
