---
title: 'SerenityOS build: Monday, November 21'
date: 2022-11-21T04:00:22Z
author: Buildbot
description: |
  Commit: 767cdf7b11 (LibWeb: Return content box position from calculate_static_position, 2022-11-19)

  - [serenity-x86_64-20221121-767cdf7.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221121-767cdf7.img.gz) (Raw image, 140.52 MiB)
  - [serenity-x86_64-20221121-767cdf7.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221121-767cdf7.vdi.gz) (VirtualBox VDI, 140.21 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221121-767cdf7.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221121-767cdf7.img.gz) (Raw image, 140.52 MiB)
- [serenity-x86_64-20221121-767cdf7.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221121-767cdf7.vdi.gz) (VirtualBox VDI, 140.21 MiB)

### Last commit :star:

```git
commit 767cdf7b113a07749521d3b220f2e83b85295a1b
Author:     Aliaksandr Kalenik <kalenik.aliaksandr@gmail.com>
AuthorDate: Sat Nov 19 05:11:38 2022 +0300
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Sun Nov 20 21:54:39 2022 +0100

    LibWeb: Return content box position from calculate_static_position
    
    This change makes calculate_static_position to return content box
    for both x and y (at least for the case when children are not inline).
    It makes it possible to be consistent about x and y when calculating
    box offset inside layout_absolutely_positioned_element.
```
