---
title: 'SerenityOS build: Saturday, November 05'
date: 2022-11-05T04:00:03Z
author: Buildbot
description: |
  Commit: b0eb45f7c7 (WebDriver+Browser: Implement `GET /session/{id}/element/{id}/screenshot`, 2022-11-04)

  - [serenity-x86_64-20221105-b0eb45f.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221105-b0eb45f.vdi.gz) (VirtualBox VDI, 138.74 MiB)
  - [serenity-x86_64-20221105-b0eb45f.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221105-b0eb45f.img.gz) (Raw image, 139.08 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221105-b0eb45f.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221105-b0eb45f.vdi.gz) (VirtualBox VDI, 138.74 MiB)
- [serenity-x86_64-20221105-b0eb45f.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221105-b0eb45f.img.gz) (Raw image, 139.08 MiB)

### Last commit :star:

```git
commit b0eb45f7c7585c4f8f0aa14fd7120aa74eca6cd4
Author:     Timothy Flynn <trflynn89@pm.me>
AuthorDate: Fri Nov 4 20:28:42 2022 -0400
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Sat Nov 5 01:10:03 2022 +0000

    WebDriver+Browser: Implement `GET /session/{id}/element/{id}/screenshot`
```
