---
title: 'SerenityOS build: Thursday, January 11'
date: 2024-01-11T06:11:58Z
author: Buildbot
description: |
  Commit: 1593ff2d4c (LibGfx/ILBMLoader: Don't throw too early when decoding bitplanes, 2024-01-10)

  - [serenity-x86_64-20240111-1593ff2.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240111-1593ff2.img.gz) (Raw image, 209.31 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240111-1593ff2.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240111-1593ff2.img.gz) (Raw image, 209.31 MiB)

### Last commit :star:

```git
commit 1593ff2d4c20861efef1256fbadd7389d029972e
Author:     Nicolas Ramz <nicolas.ramz@gmail.com>
AuthorDate: Wed Jan 10 17:45:52 2024 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Wed Jan 10 23:41:15 2024 +0100

    LibGfx/ILBMLoader: Don't throw too early when decoding bitplanes
    
    We were (again) throwing even though the image could be decoded.
```
