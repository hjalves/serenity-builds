---
title: 'SerenityOS build: Thursday, October 05'
date: 2023-10-05T05:11:28Z
author: Buildbot
description: |
  Commit: 1a29ff6e9e (Base: Specify the installed.db port database format, 2023-10-04)

  - [serenity-x86_64-20231005-1a29ff6.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231005-1a29ff6.img.gz) (Raw image, 187.77 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231005-1a29ff6.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231005-1a29ff6.img.gz) (Raw image, 187.77 MiB)

### Last commit :star:

```git
commit 1a29ff6e9e989fb4a88fdc38dcfeac30e797c971
Author:     kleines Filmröllchen <filmroellchen@serenityos.org>
AuthorDate: Wed Oct 4 12:45:14 2023 +0200
Commit:     Tim Schumacher <timschumi@gmx.de>
CommitDate: Wed Oct 4 22:28:05 2023 +0200

    Base: Specify the installed.db port database format
```
