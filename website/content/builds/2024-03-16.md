---
title: 'SerenityOS build: Saturday, March 16'
date: 2024-03-16T06:15:11Z
author: Buildbot
description: |
  Commit: f00afa4a71 (HexEditor: Avoid painting white rectangle on a white background, 2024-03-10)

  - [serenity-x86_64-20240316-f00afa4.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240316-f00afa4.img.gz) (Raw image, 219.23 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240316-f00afa4.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240316-f00afa4.img.gz) (Raw image, 219.23 MiB)

### Last commit :star:

```git
commit f00afa4a71862497596dcacac8c13ba5a7742126
Author:     Sam Atkins <atkinssj@serenityos.org>
AuthorDate: Sun Mar 10 21:20:14 2024 +0000
Commit:     Sam Atkins <atkinssj@serenityos.org>
CommitDate: Fri Mar 15 08:37:21 2024 +0000

    HexEditor: Avoid painting white rectangle on a white background
    
    Most of the time, the background color for the hex and text areas will
    be the widget's normal background color, so skip painting it in those
    cases.
```
