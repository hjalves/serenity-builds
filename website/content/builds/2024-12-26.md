---
title: 'SerenityOS build: Thursday, December 26'
date: 2024-12-26T06:25:58Z
author: Buildbot
description: |
  Commit: c3553d213e (Ports: Add menu entry for Lua, 2024-12-21)

  - [serenity-x86_64-20241226-c3553d2.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241226-c3553d2.img.gz) (Raw image, 240.84 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20241226-c3553d2.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241226-c3553d2.img.gz) (Raw image, 240.84 MiB)

### Last commit :star:

```git
commit c3553d213efe17b2954c436640cdf4d005d1397e
Author:     djwisdom <djwisdom@gmail.com>
AuthorDate: Sat Dec 21 08:41:34 2024 +0800
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Wed Dec 25 19:38:51 2024 -0500

    Ports: Add menu entry for Lua
```
