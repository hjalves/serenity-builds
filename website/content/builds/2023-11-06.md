---
title: 'SerenityOS build: Monday, November 06'
date: 2023-11-06T06:09:31Z
author: Buildbot
description: |
  Commit: 30ea218e35 (LibPDF: Implement IndexedColorSpace, 2023-11-03)

  - [serenity-x86_64-20231106-30ea218.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231106-30ea218.img.gz) (Raw image, 200.1 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231106-30ea218.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231106-30ea218.img.gz) (Raw image, 200.1 MiB)

### Last commit :star:

```git
commit 30ea218e35df0bdeaa42b7ff94c6b47075b7a635
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Fri Nov 3 01:49:04 2023 -0400
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Sun Nov 5 14:27:22 2023 -0700

    LibPDF: Implement IndexedColorSpace
```
