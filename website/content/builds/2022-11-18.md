---
title: 'SerenityOS build: Friday, November 18'
date: 2022-11-18T05:18:59Z
author: Buildbot
description: |
  Commit: c2b20b5681 (AK: Give DisjointChunks::m_chunks an inline capacity of 1, 2022-11-16)

  - [serenity-x86_64-20221118-c2b20b5.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221118-c2b20b5.img.gz) (Raw image, 140.25 MiB)
  - [serenity-x86_64-20221118-c2b20b5.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221118-c2b20b5.vdi.gz) (VirtualBox VDI, 139.93 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221118-c2b20b5.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221118-c2b20b5.img.gz) (Raw image, 140.25 MiB)
- [serenity-x86_64-20221118-c2b20b5.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221118-c2b20b5.vdi.gz) (VirtualBox VDI, 139.93 MiB)

### Last commit :star:

```git
commit c2b20b5681bc6e5459c0a584ad42d9bec0016858
Author:     Ali Mohammad Pur <mpfard@serenityos.org>
AuthorDate: Wed Nov 16 01:22:34 2022 +0330
Commit:     Ali Mohammad Pur <mpfard@serenityos.org>
CommitDate: Thu Nov 17 20:13:04 2022 +0330

    AK: Give DisjointChunks::m_chunks an inline capacity of 1
    
    That's one fewer level of indirection for flattened ones.
```
