---
title: 'SerenityOS build: Saturday, April 15'
date: 2023-04-15T05:03:51Z
author: Buildbot
description: |
  Commit: f47f5ff473 (Tests: Add a test for LZMA repetition lengths beyond the distance, 2023-04-14)

  - [serenity-x86_64-20230415-f47f5ff.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230415-f47f5ff.img.gz) (Raw image, 178.74 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230415-f47f5ff.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230415-f47f5ff.img.gz) (Raw image, 178.74 MiB)

### Last commit :star:

```git
commit f47f5ff473824ebca92534c33fd274f014a2b243
Author:     Tim Schumacher <timschumi@gmx.de>
AuthorDate: Fri Apr 14 17:30:11 2023 +0200
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Fri Apr 14 22:52:08 2023 +0200

    Tests: Add a test for LZMA repetition lengths beyond the distance
```
