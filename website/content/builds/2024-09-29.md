---
title: 'SerenityOS build: Sunday, September 29'
date: 2024-09-29T05:22:08Z
author: Buildbot
description: |
  Commit: d11ca444d0 (AK: Assert that `is<T>()` input and output types are not the same, 2024-08-08)

  - [serenity-x86_64-20240929-d11ca44.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240929-d11ca44.img.gz) (Raw image, 221.78 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240929-d11ca44.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240929-d11ca44.img.gz) (Raw image, 221.78 MiB)

### Last commit :star:

```git
commit d11ca444d0e3d031e32267cc8ed67f0293a37ba5
Author:     Tim Ledbetter <tim.ledbetter@ladybird.org>
AuthorDate: Thu Aug 8 14:21:13 2024 +0100
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Sat Sep 28 14:14:28 2024 -0400

    AK: Assert that `is<T>()` input and output types are not the same
    
    This makes it a compile error to use is<T>() where the input and output
    types are known to be the same at compile time.
    
    (cherry picked from commit 82a63e350cb9a67cbd3aa659d909ae9e6599e93f)
```
