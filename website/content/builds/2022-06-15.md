---
title: 'SerenityOS build: Wednesday, June 15'
date: 2022-06-15T03:04:54Z
author: Buildbot
description: |
  Commit: 527fed1391 (LibGfx: Purge doubles from Painter and use more of AK::Math, 2022-06-13)

  - [serenity-i686-20220615-527fed139.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220615-527fed139.img.gz) (Raw image, 124.1 MiB)
  - [serenity-i686-20220615-527fed139.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220615-527fed139.vdi.gz) (VirtualBox VDI, 123.93 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220615-527fed139.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220615-527fed139.img.gz) (Raw image, 124.1 MiB)
- [serenity-i686-20220615-527fed139.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220615-527fed139.vdi.gz) (VirtualBox VDI, 123.93 MiB)

### Last commit :star:

```git
commit 527fed139113b8bc2c40356cf628bf3fc14758de
Author:     Hediadyoin1 <leon2002.la@gmail.com>
AuthorDate: Mon Jun 13 17:14:42 2022 +0200
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Tue Jun 14 23:00:52 2022 +0200

    LibGfx: Purge doubles from Painter and use more of AK::Math
    
    The added precision of doubles is most likely not needed here and floats
    are usually cheaper than doubles, so lets always stick to them.
    
    This also simplifies some calls to sin+cos to AK:sincos and a call to
    atan(1/x) to atan2(1,x) to avoid a division.
```
