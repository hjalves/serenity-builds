---
title: 'SerenityOS build: Tuesday, September 06'
date: 2022-09-06T03:04:36Z
author: Buildbot
description: |
  Commit: d13d571844 (LibJS: Make sure JS::Script visits its HostDefined object, 2022-09-06)

  - [serenity-i686-20220906-d13d571.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220906-d13d571.img.gz) (Raw image, 127.14 MiB)
  - [serenity-i686-20220906-d13d571.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220906-d13d571.vdi.gz) (VirtualBox VDI, 126.97 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220906-d13d571.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220906-d13d571.img.gz) (Raw image, 127.14 MiB)
- [serenity-i686-20220906-d13d571.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220906-d13d571.vdi.gz) (VirtualBox VDI, 126.97 MiB)

### Last commit :star:

```git
commit d13d571844bfa0efe200f8712fd23d9ab4bb29e9
Author:     Andreas Kling <kling@serenityos.org>
AuthorDate: Tue Sep 6 01:19:58 2022 +0200
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Tue Sep 6 01:21:09 2022 +0200

    LibJS: Make sure JS::Script visits its HostDefined object
    
    This allows JS::Script to mark its corresponding HTML::Script, even if
    it's a little roundabout looking. Fixes an issue where the JS::Script
    was kept alive by the execution stack, but the HTML::Script was gone.
    
    This was originally part of 8f9ed415a02dd62c46fce4f5d352ad51bc779a27
    but got lost in a merging accident.
```
