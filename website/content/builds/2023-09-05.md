---
title: 'SerenityOS build: Tuesday, September 05'
date: 2023-09-05T05:10:20Z
author: Buildbot
description: |
  Commit: c24d317d8f (LibWeb: Remove some unnecessary static_casts to Layout::Box&, 2023-09-04)

  - [serenity-x86_64-20230905-c24d317.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230905-c24d317.img.gz) (Raw image, 186.73 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230905-c24d317.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230905-c24d317.img.gz) (Raw image, 186.73 MiB)

### Last commit :star:

```git
commit c24d317d8f8fe9ba0f150e35df1ec00211e038c9
Author:     Andreas Kling <kling@serenityos.org>
AuthorDate: Mon Sep 4 12:31:23 2023 +0200
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Mon Sep 4 18:22:59 2023 +0200

    LibWeb: Remove some unnecessary static_casts to Layout::Box&
    
    UBSAN flagged one of these as invalid, and since it's not even necessary
    to cast here anymore, let's just not.
```
