---
title: 'SerenityOS build: Sunday, December 11'
date: 2022-12-11T17:13:06Z
author: Buildbot
description: |
  Commit: b65258c093 (Help+man+LibManual: Move argument handling to LibManual, 2022-07-12)

  - [serenity-x86_64-20221211-b65258c.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221211-b65258c.vdi.gz) (VirtualBox VDI, 142.89 MiB)
  - [serenity-x86_64-20221211-b65258c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221211-b65258c.img.gz) (Raw image, 143.18 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221211-b65258c.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221211-b65258c.vdi.gz) (VirtualBox VDI, 142.89 MiB)
- [serenity-x86_64-20221211-b65258c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221211-b65258c.img.gz) (Raw image, 143.18 MiB)

### Last commit :star:

```git
commit b65258c093a14a8bac082aa3580ae70b78f92283
Author:     kleines Filmröllchen <filmroellchen@serenityos.org>
AuthorDate: Tue Jul 12 20:23:28 2022 +0200
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Sun Dec 11 16:05:23 2022 +0000

    Help+man+LibManual: Move argument handling to LibManual
    
    This deduplicates argument handling logic from Help and man and makes it
    more modular for future use cases. The argument handling works as
    before: two arguments specify section and page (in this order), one
    argument specifies either a page (the first section that it's found in
    is used) or a path to a manpage markdown file.
```
