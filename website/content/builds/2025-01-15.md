---
title: 'SerenityOS build: Wednesday, January 15'
date: 2025-01-15T06:23:55Z
author: Buildbot
description: |
  Commit: 6127f03108 (Kernel/GPU: Add a simple-framebuffer devicetree driver, 2025-01-12)

  - [serenity-x86_64-20250115-6127f03.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250115-6127f03.img.gz) (Raw image, 241.21 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20250115-6127f03.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250115-6127f03.img.gz) (Raw image, 241.21 MiB)

### Last commit :star:

```git
commit 6127f031081827bae32076085d105bc7b10671ca
Author:     Sönke Holz <sholz8530@gmail.com>
AuthorDate: Sun Jan 12 17:37:48 2025 +0100
Commit:     Sönke Holz <sholz8530@gmail.com>
CommitDate: Tue Jan 14 00:16:43 2025 +0100

    Kernel/GPU: Add a simple-framebuffer devicetree driver
    
    simple-framebuffer nodes describe pre-initialized framebuffers that we
    can use without any special setup code.
```
