---
title: 'SerenityOS build: Thursday, March 28'
date: 2024-03-28T06:17:03Z
author: Buildbot
description: |
  Commit: ce325a9912 (LibWeb: Add SessionHistoryEntry::document(), 2024-03-27)

  - [serenity-x86_64-20240328-ce325a9.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240328-ce325a9.img.gz) (Raw image, 220.76 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240328-ce325a9.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240328-ce325a9.img.gz) (Raw image, 220.76 MiB)

### Last commit :star:

```git
commit ce325a99124990df4c48c48dbef6cf3454164914
Author:     Aliaksandr Kalenik <kalenik.aliaksandr@gmail.com>
AuthorDate: Wed Mar 27 16:14:52 2024 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Wed Mar 27 18:07:07 2024 +0100

    LibWeb: Add SessionHistoryEntry::document()
    
    No behaviour change intended.
```
