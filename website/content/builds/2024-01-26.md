---
title: 'SerenityOS build: Friday, January 26'
date: 2024-01-26T06:12:42Z
author: Buildbot
description: |
  Commit: 09124fc3a5 (LibWeb: Set up the Fetch response's body with the appropriate stream, 2024-01-25)

  - [serenity-x86_64-20240126-09124fc.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240126-09124fc.img.gz) (Raw image, 213.19 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240126-09124fc.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240126-09124fc.img.gz) (Raw image, 213.19 MiB)

### Last commit :star:

```git
commit 09124fc3a52262eb45f556c69786132b685cb7fe
Author:     Timothy Flynn <trflynn89@pm.me>
AuthorDate: Thu Jan 25 14:40:55 2024 -0500
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Thu Jan 25 21:34:03 2024 +0100

    LibWeb: Set up the Fetch response's body with the appropriate stream
```
