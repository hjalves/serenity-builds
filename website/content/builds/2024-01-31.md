---
title: 'SerenityOS build: Wednesday, January 31'
date: 2024-01-31T06:13:13Z
author: Buildbot
description: |
  Commit: 4330cdee74 (Tests/Kernel: Properly synchronize threads in TestTCPSocket, 2024-01-29)

  - [serenity-x86_64-20240131-4330cde.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240131-4330cde.img.gz) (Raw image, 213.62 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240131-4330cde.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240131-4330cde.img.gz) (Raw image, 213.62 MiB)

### Last commit :star:

```git
commit 4330cdee74c86a383055d788de43fd4ae2bd7bc2
Author:     Dan Klishch <danilklishch@gmail.com>
AuthorDate: Mon Jan 29 19:48:51 2024 -0500
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Tue Jan 30 18:46:37 2024 -0700

    Tests/Kernel: Properly synchronize threads in TestTCPSocket
    
    We should wait for a server thread to actually listen(2) a server socket
    before trying to connect to it. This hopefully should make the test less
    flaky.
```
