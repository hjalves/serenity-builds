---
title: 'SerenityOS build: Wednesday, June 22'
date: 2022-06-22T03:04:28Z
author: Buildbot
description: |
  Commit: 2df56f840f (Revert "Toolchain: Load x64 executables at a higher address", 2021-12-22)

  - [serenity-i686-20220622-2df56f8.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220622-2df56f8.img.gz) (Raw image, 124.51 MiB)
  - [serenity-i686-20220622-2df56f8.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220622-2df56f8.vdi.gz) (VirtualBox VDI, 124.31 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220622-2df56f8.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220622-2df56f8.img.gz) (Raw image, 124.51 MiB)
- [serenity-i686-20220622-2df56f8.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220622-2df56f8.vdi.gz) (VirtualBox VDI, 124.31 MiB)

### Last commit :star:

```git
commit 2df56f840f3764b5e1de400748812ee61e052ef8
Author:     Tim Schumacher <timschumi@gmx.de>
AuthorDate: Wed Dec 22 13:32:09 2021 +0100
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Tue Jun 21 22:38:15 2022 +0100

    Revert "Toolchain: Load x64 executables at a higher address"
    
    Now that the lower pages can be unmapped and more of the virtual
    address range is available to us, we can actually use the default
    mapping address of x86_64 again.
    
    This reverts commit 292398b5857d0104f7c33fdb5d79f45fe8b395dd.
```
