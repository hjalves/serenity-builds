---
title: 'SerenityOS build: Sunday, October 02'
date: 2022-10-02T03:56:06Z
author: Buildbot
description: |
  Commit: b288cd6976 (Ports/sdl12-compat: Update to version 1.2.56, 2022-10-02)

  - [serenity-i686-20221002-b288cd6.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20221002-b288cd6.vdi.gz) (VirtualBox VDI, 130.24 MiB)
  - [serenity-i686-20221002-b288cd6.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20221002-b288cd6.img.gz) (Raw image, 130.41 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20221002-b288cd6.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20221002-b288cd6.vdi.gz) (VirtualBox VDI, 130.24 MiB)
- [serenity-i686-20221002-b288cd6.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20221002-b288cd6.img.gz) (Raw image, 130.41 MiB)

### Last commit :star:

```git
commit b288cd6976c9e7d48114ad73531a9e2b973a183c
Author:     Tim Schumacher <timschumi@gmx.de>
AuthorDate: Sun Oct 2 01:39:59 2022 +0200
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Sun Oct 2 00:59:39 2022 +0100

    Ports/sdl12-compat: Update to version 1.2.56
```
