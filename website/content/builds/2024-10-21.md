---
title: 'SerenityOS build: Monday, October 21'
date: 2024-10-21T05:25:17Z
author: Buildbot
description: |
  Commit: e47c20c994 (LibWeb/CSS: Rename CalculatedStyleValue -> CSSMathValue, 2024-09-18)

  - [serenity-x86_64-20241021-e47c20c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241021-e47c20c.img.gz) (Raw image, 224.79 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20241021-e47c20c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241021-e47c20c.img.gz) (Raw image, 224.79 MiB)

### Last commit :star:

```git
commit e47c20c994911b63823ec39119e3852432ab6f21
Author:     Sam Atkins <sam@ladybird.org>
AuthorDate: Wed Sep 18 17:27:47 2024 +0100
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Sun Oct 20 22:24:57 2024 -0400

    LibWeb/CSS: Rename CalculatedStyleValue -> CSSMathValue
    
    This matches the name in the CSS Typed OM spec. There's quite a lot
    still to do to make it match the spec behavior, but this is the first
    step.
    
    (cherry picked from commit 76daba3069de4c184c6b2317d0c89b50f81a8c00)
```
