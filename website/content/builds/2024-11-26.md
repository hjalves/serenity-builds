---
title: 'SerenityOS build: Tuesday, November 26'
date: 2024-11-26T06:25:35Z
author: Buildbot
description: |
  Commit: fcb45f4893 (LibWeb: Fix extra validation for EasingStyleValue intervals, 2024-11-18)

  - [serenity-x86_64-20241126-fcb45f4.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241126-fcb45f4.img.gz) (Raw image, 229.47 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20241126-fcb45f4.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241126-fcb45f4.img.gz) (Raw image, 229.47 MiB)

### Last commit :star:

```git
commit fcb45f489356b39cf4fb3222415e71a0c01d1583
Author:     Pavel Shliak <shlyakpavel@gmail.com>
AuthorDate: Mon Nov 18 17:13:42 2024 +0400
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Mon Nov 25 12:19:48 2024 -0500

    LibWeb: Fix extra validation for EasingStyleValue intervals
    
    (cherry picked from commit b342758dbf8d1138353db89708964e75b9128400)
```
