---
title: 'SerenityOS build: Wednesday, November 27'
date: 2024-11-27T06:25:26Z
author: Buildbot
description: |
  Commit: 7211ba30be (Meta: Add more commits and PRs to lb-cherry-picks.py, 2024-11-26)

  - [serenity-x86_64-20241127-7211ba3.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241127-7211ba3.img.gz) (Raw image, 229.57 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20241127-7211ba3.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241127-7211ba3.img.gz) (Raw image, 229.57 MiB)

### Last commit :star:

```git
commit 7211ba30becf22f9e9d39b2d81225578ff7e7f81
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Tue Nov 26 21:36:45 2024 -0500
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Tue Nov 26 23:27:06 2024 -0500

    Meta: Add more commits and PRs to lb-cherry-picks.py
```
