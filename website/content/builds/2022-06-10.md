---
title: 'SerenityOS build: Friday, June 10'
date: 2022-06-10T03:05:03Z
author: Buildbot
description: |
  Commit: 817c79431d (Ports: Split up the `halflife` port into engine and game, 2022-06-09)

  - [serenity-i686-20220610-817c794.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220610-817c794.vdi.gz) (VirtualBox VDI, 123.49 MiB)
  - [serenity-i686-20220610-817c794.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220610-817c794.img.gz) (Raw image, 123.65 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220610-817c794.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220610-817c794.vdi.gz) (VirtualBox VDI, 123.49 MiB)
- [serenity-i686-20220610-817c794.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220610-817c794.img.gz) (Raw image, 123.65 MiB)

### Last commit :star:

```git
commit 817c79431dcdb76df962d850894d1c09808ac4a3
Author:     Tim Schumacher <timschumi@gmx.de>
AuthorDate: Thu Jun 9 15:21:27 2022 +0200
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Fri Jun 10 00:04:33 2022 +0100

    Ports: Split up the `halflife` port into engine and game
```
