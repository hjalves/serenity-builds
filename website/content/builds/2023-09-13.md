---
title: 'SerenityOS build: Wednesday, September 13'
date: 2023-09-13T05:09:38Z
author: Buildbot
description: |
  Commit: 642802d339 (LibWeb: Add {de}serialization steps for ArrayBuffers, 2023-09-11)

  - [serenity-x86_64-20230913-642802d.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230913-642802d.img.gz) (Raw image, 185.92 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230913-642802d.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230913-642802d.img.gz) (Raw image, 185.92 MiB)

### Last commit :star:

```git
commit 642802d3390dbe507498ae6fccaa61d10f4f0ebf
Author:     Andrew Kaster <akaster@serenityos.org>
AuthorDate: Mon Sep 11 18:06:43 2023 -0600
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Tue Sep 12 22:14:39 2023 +0200

    LibWeb: Add {de}serialization steps for ArrayBuffers
```
