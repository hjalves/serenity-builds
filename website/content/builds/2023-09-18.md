---
title: 'SerenityOS build: Monday, September 18'
date: 2023-09-18T05:10:05Z
author: Buildbot
description: |
  Commit: e31a3ef2ad (CI: Bump docker/login-action from 2 to 3, 2023-09-18)

  - [serenity-x86_64-20230918-e31a3ef.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230918-e31a3ef.img.gz) (Raw image, 186.4 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230918-e31a3ef.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230918-e31a3ef.img.gz) (Raw image, 186.4 MiB)

### Last commit :star:

```git
commit e31a3ef2ad756c34f79556eeb551dcc9d4054a8b
Author:     dependabot[bot] <49699333+dependabot[bot]@users.noreply.github.com>
AuthorDate: Mon Sep 18 01:38:51 2023 +0000
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Sun Sep 17 20:58:02 2023 -0600

    CI: Bump docker/login-action from 2 to 3
    
    Bumps [docker/login-action](https://github.com/docker/login-action) from 2 to 3.
    - [Release notes](https://github.com/docker/login-action/releases)
    - [Commits](https://github.com/docker/login-action/compare/v2...v3)
    
    ---
    updated-dependencies:
    - dependency-name: docker/login-action
      dependency-type: direct:production
      update-type: version-update:semver-major
    ...
    
    Signed-off-by: dependabot[bot] <support@github.com>
```
