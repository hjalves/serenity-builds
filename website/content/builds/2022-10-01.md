---
title: 'SerenityOS build: Saturday, October 01'
date: 2022-10-01T03:56:20Z
author: Buildbot
description: |
  Commit: be6b3710c8 (Ports/qemu: Use the coarse monotonic clock for timing CPU ticks, 2022-07-28)

  - [serenity-i686-20221001-be6b371.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20221001-be6b371.vdi.gz) (VirtualBox VDI, 129.74 MiB)
  - [serenity-i686-20221001-be6b371.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20221001-be6b371.img.gz) (Raw image, 129.91 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20221001-be6b371.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20221001-be6b371.vdi.gz) (VirtualBox VDI, 129.74 MiB)
- [serenity-i686-20221001-be6b371.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20221001-be6b371.img.gz) (Raw image, 129.91 MiB)

### Last commit :star:

```git
commit be6b3710c838f836544857092b407c3e374eb23c
Author:     Tim Schumacher <timschumi@gmx.de>
AuthorDate: Thu Jul 28 08:31:21 2022 +0200
Commit:     Brian Gianforcaro <bgianf@serenityos.org>
CommitDate: Fri Sep 30 20:13:11 2022 -0700

    Ports/qemu: Use the coarse monotonic clock for timing CPU ticks
    
    While this loses quite a bit of accuracy (although to no apparent
    decrease in emulation quality) , it helps avoiding the additional
    overhead of the `clock_gettime` syscall (as `CLOCK_MONOTONIC_COARSE`
    is forwarded using the mapped time page) and we don't have to do a
    HPET timer read for each tick.
    
    This results in a decrease of Serenity boot time from 1h16m down to
    42m when running on Serenity.
```
