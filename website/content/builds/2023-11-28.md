---
title: 'SerenityOS build: Tuesday, November 28'
date: 2023-11-28T06:10:22Z
author: Buildbot
description: |
  Commit: b2302ed23f (LibGfx: Unbreak building with Python 3.9, 2023-11-28)

  - [serenity-x86_64-20231128-b2302ed.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231128-b2302ed.img.gz) (Raw image, 205.22 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231128-b2302ed.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231128-b2302ed.img.gz) (Raw image, 205.22 MiB)

### Last commit :star:

```git
commit b2302ed23fe86d98c37bf197fe0fdf78647ffc7a
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Tue Nov 28 09:54:03 2023 +0900
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Mon Nov 27 21:58:16 2023 -0500

    LibGfx: Unbreak building with Python 3.9
    
    Unbreaks building with macOS 13 system python, and is less code too.
    No behavior change.
```
