---
title: 'SerenityOS build: Sunday, September 18'
date: 2022-09-18T20:31:24Z
author: Buildbot
description: |
  Commit: 30cca01e24 (HeaderCheck: Also check AK for broken headers, 2022-09-13)

  - [serenity-i686-20220918-30cca01.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220918-30cca01.vdi.gz) (VirtualBox VDI, 128.31 MiB)
  - [serenity-i686-20220918-30cca01.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220918-30cca01.img.gz) (Raw image, 128.48 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220918-30cca01.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220918-30cca01.vdi.gz) (VirtualBox VDI, 128.31 MiB)
- [serenity-i686-20220918-30cca01.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220918-30cca01.img.gz) (Raw image, 128.48 MiB)

### Last commit :star:

```git
commit 30cca01e2442e9ac6dc38f5e8a1bb42d878d3266
Author:     Ben Wiederhake <BenWiederhake.GitHub@gmx.de>
AuthorDate: Tue Sep 13 23:23:55 2022 +0200
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Sun Sep 18 13:27:24 2022 -0400

    HeaderCheck: Also check AK for broken headers
```
