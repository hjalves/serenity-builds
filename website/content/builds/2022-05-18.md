---
title: 'SerenityOS build: Wednesday, May 18'
date: 2022-05-18T03:04:59Z
author: Buildbot
description: |
  Commit: 74695ce76e (LibJS: Mark two Get operations in ToTemporalZonedDateTime infallible, 2022-05-17)

  - [serenity-i686-20220518-74695ce.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220518-74695ce.vdi.gz) (VirtualBox VDI, 121.98 MiB)
  - [serenity-i686-20220518-74695ce.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220518-74695ce.img.gz) (Raw image, 122.16 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220518-74695ce.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220518-74695ce.vdi.gz) (VirtualBox VDI, 121.98 MiB)
- [serenity-i686-20220518-74695ce.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220518-74695ce.img.gz) (Raw image, 122.16 MiB)

### Last commit :star:

```git
commit 74695ce76ea5f6b88d8cb166621edafd1abedb47
Author:     Linus Groh <mail@linusgroh.de>
AuthorDate: Tue May 17 21:24:50 2022 +0100
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Tue May 17 21:24:50 2022 +0100

    LibJS: Mark two Get operations in ToTemporalZonedDateTime infallible
    
    This is an editorial change in the Temporal spec.
    
    See: https://github.com/tc39/proposal-temporal/commit/fed9f16
```
