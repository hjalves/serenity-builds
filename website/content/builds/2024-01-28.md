---
title: 'SerenityOS build: Sunday, January 28'
date: 2024-01-28T06:12:28Z
author: Buildbot
description: |
  Commit: eaf9a56c10 (LibWeb: Actually clear stream controller algorithms when needed, 2024-01-26)

  - [serenity-x86_64-20240128-eaf9a56.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240128-eaf9a56.img.gz) (Raw image, 213.25 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240128-eaf9a56.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240128-eaf9a56.img.gz) (Raw image, 213.25 MiB)

### Last commit :star:

```git
commit eaf9a56c1004a4ef20c0153e661e4af1fcb5b992
Author:     Shannon Booth <shannon@serenityos.org>
AuthorDate: Fri Jan 26 18:55:19 2024 +1300
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Sat Jan 27 21:40:25 2024 -0500

    LibWeb: Actually clear stream controller algorithms when needed
    
    Now that these algorithms are a HeapFunction as opposed to SafeFunction,
    the problem mentioned in the FIXME is no longer applicable as these
    functions are GC-allocated like everything else.
```
