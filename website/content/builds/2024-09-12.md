---
title: 'SerenityOS build: Thursday, September 12'
date: 2024-09-12T05:22:23Z
author: Buildbot
description: |
  Commit: 76bd6115cb (CI: Don't build AArch64 QEMU in CI, 2024-08-25)

  - [serenity-x86_64-20240912-76bd611.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240912-76bd611.img.gz) (Raw image, 221.54 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240912-76bd611.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240912-76bd611.img.gz) (Raw image, 221.54 MiB)

### Last commit :star:

```git
commit 76bd6115cb1117f469635d6adba78c7dd05978be
Author:     Sönke Holz <sholz8530@gmail.com>
AuthorDate: Sun Aug 25 22:02:34 2024 +0200
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Wed Sep 11 12:47:12 2024 -0400

    CI: Don't build AArch64 QEMU in CI
    
    We now get the command line from the flattened devicetree, so manually
    building a newer QEMU version to make the RPi "Get command line" mailbox
    message work is not necessary anymore.
```
