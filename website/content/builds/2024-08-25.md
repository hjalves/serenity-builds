---
title: 'SerenityOS build: Sunday, August 25'
date: 2024-08-25T05:21:59Z
author: Buildbot
description: |
  Commit: d73bad14ea (LibIMAP+Mail: Show unseen message count for mailboxes, 2024-08-24)

  - [serenity-x86_64-20240825-d73bad1.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240825-d73bad1.img.gz) (Raw image, 221.52 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240825-d73bad1.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240825-d73bad1.img.gz) (Raw image, 221.52 MiB)

### Last commit :star:

```git
commit d73bad14eabf15daa527977d9498f37abfde30cd
Author:     Alec Murphy <alec@checksum.fail>
AuthorDate: Sat Aug 24 15:03:19 2024 -0400
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Sat Aug 24 21:52:44 2024 -0400

    LibIMAP+Mail: Show unseen message count for mailboxes
    
    This PR implements the standard behavior of displaying the mailbox name
    and parenthesized unseen message count in bold when the unseen message
    count is greater than zero.
```
