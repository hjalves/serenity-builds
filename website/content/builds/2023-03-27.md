---
title: 'SerenityOS build: Monday, March 27'
date: 2023-03-27T05:02:45Z
author: Buildbot
description: |
  Commit: 039d5edc6f (Browser: Show current zoom level in view menu, 2023-03-26)

  - [serenity-x86_64-20230327-039d5ed.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230327-039d5ed.img.gz) (Raw image, 169.96 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230327-039d5ed.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230327-039d5ed.img.gz) (Raw image, 169.96 MiB)

### Last commit :star:

```git
commit 039d5edc6f7c852ab9da39bc581f55ef11f1790c
Author:     MacDue <macdue@dueutil.tech>
AuthorDate: Sun Mar 26 19:41:06 2023 +0100
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Sun Mar 26 21:55:21 2023 +0100

    Browser: Show current zoom level in view menu
```
