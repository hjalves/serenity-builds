---
title: 'SerenityOS build: Monday, February 03'
date: 2025-02-03T06:24:21Z
author: Buildbot
description: |
  Commit: 152b657ad2 (LibGfx: Store JPEG2000ColorSpecificationBox on JPEG2000LoadingContext, 2025-02-02)

  - [serenity-x86_64-20250203-152b657.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250203-152b657.img.gz) (Raw image, 241.58 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20250203-152b657.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250203-152b657.img.gz) (Raw image, 241.58 MiB)

### Last commit :star:

```git
commit 152b657ad273d0b014869834772113d05fb8e1c0
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Sun Feb 2 14:27:02 2025 -0500
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Sun Feb 2 17:28:10 2025 -0500

    LibGfx: Store JPEG2000ColorSpecificationBox on JPEG2000LoadingContext
    
    This will be useful to be more spec-compliant in convert_to_bitmap().
    
    No behavior change.
```
