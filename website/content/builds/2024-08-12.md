---
title: 'SerenityOS build: Monday, August 12'
date: 2024-08-12T05:21:49Z
author: Buildbot
description: |
  Commit: 82053da343 (LibGfx/PNGWriter: Remove one output data copy, 2024-08-10)

  - [serenity-x86_64-20240812-82053da.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240812-82053da.img.gz) (Raw image, 221.5 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240812-82053da.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240812-82053da.img.gz) (Raw image, 221.5 MiB)

### Last commit :star:

```git
commit 82053da343de4951bb57863c9d023caa401b624c
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Sat Aug 10 13:46:50 2024 -0400
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Sun Aug 11 13:50:11 2024 -0400

    LibGfx/PNGWriter: Remove one output data copy
    
    Now that we do two passes, we can easily write into
    uncompressed_block_data directly.
    
    Somewhat surprisingly, this is perf-neutral.
```
