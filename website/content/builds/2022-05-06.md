---
title: 'SerenityOS build: Friday, May 06'
date: 2022-05-06T03:36:03Z
author: Buildbot
description: |
  Commit: e268659d32 (Terminal+TerminalSettings: Allow disabling close confirmations, 2022-05-04)

  - [serenity-i686-20220506-e268659.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220506-e268659.vdi.gz) (VirtualBox VDI, 122.62 MiB)
  - [serenity-i686-20220506-e268659.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220506-e268659.img.gz) (Raw image, 122.8 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220506-e268659.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220506-e268659.vdi.gz) (VirtualBox VDI, 122.62 MiB)
- [serenity-i686-20220506-e268659.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220506-e268659.img.gz) (Raw image, 122.8 MiB)

### Last commit :star:

```git
commit e268659d32f45af26754cbb5a0de61842ded1093
Author:     MacDue <macdue@dueutil.tech>
AuthorDate: Wed May 4 21:36:07 2022 +0100
Commit:     Ali Mohammad Pur <mpfard@serenityos.org>
CommitDate: Fri May 6 02:12:51 2022 +0430

    Terminal+TerminalSettings: Allow disabling close confirmations
```
