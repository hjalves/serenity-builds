---
title: 'SerenityOS build: Sunday, April 28'
date: 2024-04-28T05:17:22Z
author: Buildbot
description: |
  Commit: 6b5deb2259 (Ladybird: Support multiple browser windows in Qt chrome, 2024-04-26)

  - [serenity-x86_64-20240428-6b5deb2.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240428-6b5deb2.img.gz) (Raw image, 221.16 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240428-6b5deb2.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240428-6b5deb2.img.gz) (Raw image, 221.16 MiB)

### Last commit :star:

```git
commit 6b5deb22597f16a69dd01d61f7990e0c652a2474
Author:     Andrew Kaster <akaster@serenityos.org>
AuthorDate: Fri Apr 26 18:53:55 2024 -0600
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Sat Apr 27 20:32:12 2024 -0400

    Ladybird: Support multiple browser windows in Qt chrome
    
    This also moves the ownership of the TaskManger to the Application.
```
