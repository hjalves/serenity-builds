---
title: 'SerenityOS build: Wednesday, January 04'
date: 2023-01-04T05:07:37Z
author: Buildbot
description: |
  Commit: d57e9b53a0 (LibGL: Implement `GL_BLEND_DST` and `GL_BLEND_SRC`, 2023-01-04)

  - [serenity-x86_64-20230104-d57e9b5.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230104-d57e9b5.img.gz) (Raw image, 655.58 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230104-d57e9b5.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230104-d57e9b5.img.gz) (Raw image, 655.58 MiB)

### Last commit :star:

```git
commit d57e9b53a03bf944b6ffdec5fc3bae4695dc5876
Author:     Jelle Raaijmakers <jelle@gmta.nl>
AuthorDate: Wed Jan 4 01:18:19 2023 +0100
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Tue Jan 3 19:49:31 2023 -0500

    LibGL: Implement `GL_BLEND_DST` and `GL_BLEND_SRC`
    
    These context parameters are used to retrieve the active blend
    functions.
```
