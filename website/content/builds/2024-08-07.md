---
title: 'SerenityOS build: Wednesday, August 07'
date: 2024-08-07T05:22:02Z
author: Buildbot
description: |
  Commit: cf8210175f (LibGfx/PNGWriter: Inline the now not very useful append(u8), 2024-08-05)

  - [serenity-x86_64-20240807-cf82101.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240807-cf82101.img.gz) (Raw image, 221.41 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240807-cf82101.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240807-cf82101.img.gz) (Raw image, 221.41 MiB)

### Last commit :star:

```git
commit cf8210175f87815834001312f530a85dfc53b71c
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Mon Aug 5 23:18:55 2024 -0400
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Tue Aug 6 23:00:32 2024 -0400

    LibGfx/PNGWriter: Inline the now not very useful append(u8)
    
    No behavior change.
```
