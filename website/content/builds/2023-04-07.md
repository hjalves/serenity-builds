---
title: 'SerenityOS build: Friday, April 07'
date: 2023-04-07T05:03:20Z
author: Buildbot
description: |
  Commit: 4d87072201 (LibWeb: Port {HTML,UIEvents,XHR}::EventNames to new String, 2023-04-06)

  - [serenity-x86_64-20230407-4d87072.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230407-4d87072.img.gz) (Raw image, 177.69 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230407-4d87072.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230407-4d87072.img.gz) (Raw image, 177.69 MiB)

### Last commit :star:

```git
commit 4d87072201efc4ef9086588255fbb1bb993872ab
Author:     Kenneth Myhra <kennethmyhra@serenityos.org>
AuthorDate: Thu Apr 6 07:25:18 2023 +0200
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Thu Apr 6 23:49:08 2023 +0200

    LibWeb: Port {HTML,UIEvents,XHR}::EventNames to new String
```
