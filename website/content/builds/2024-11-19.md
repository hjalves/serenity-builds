---
title: 'SerenityOS build: Tuesday, November 19'
date: 2024-11-19T06:27:53Z
author: Buildbot
description: |
  Commit: 5148cd3122 (LibWeb/CSS: Start parsing the `color()` function, 2024-10-27)

  - [serenity-x86_64-20241119-5148cd3.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241119-5148cd3.img.gz) (Raw image, 227.44 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20241119-5148cd3.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241119-5148cd3.img.gz) (Raw image, 227.44 MiB)

### Last commit :star:

```git
commit 5148cd312249dc0d587fbe4cac0d7c0c3a3856b2
Author:     Lucas CHOLLET <lucas.chollet@free.fr>
AuthorDate: Sun Oct 27 00:42:13 2024 -0400
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Mon Nov 18 20:22:45 2024 -0500

    LibWeb/CSS: Start parsing the `color()` function
    
    This is really bare bone as we only support the `xyz-d50` color space
    for the moment.
    
    It makes us pass the following WPT tests:
     - css/css-color/predefined-016.html
     - css/css-color/xyz-d50-001.html
     - css/css-color/xyz-d50-002.html
    
    (cherry picked from commit 48bbebc636598eca905b8eef984ee2cba548ff64)
```
