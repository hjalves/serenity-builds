---
title: 'SerenityOS build: Friday, April 26'
date: 2024-04-26T05:17:43Z
author: Buildbot
description: |
  Commit: 0c8a98ac94 (LibWeb: Begin implementing the interface for AudioBuffer, 2024-04-25)

  - [serenity-x86_64-20240426-0c8a98a.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240426-0c8a98a.img.gz) (Raw image, 221.12 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240426-0c8a98a.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240426-0c8a98a.img.gz) (Raw image, 221.12 MiB)

### Last commit :star:

```git
commit 0c8a98ac948051f40660fd4d0d130e22daa4fb9f
Author:     Shannon Booth <shannon@serenityos.org>
AuthorDate: Thu Apr 25 15:13:51 2024 +1200
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Thu Apr 25 19:26:19 2024 -0400

    LibWeb: Begin implementing the interface for AudioBuffer
    
    Implement the constructor and getChannelData function, working towards
    the functionality that we need in order to implement
    OfflineAudioContext.
```
