---
title: 'SerenityOS build: Monday, May 02'
date: 2022-05-02T03:36:02Z
author: Buildbot
description: |
  Commit: 44bcd7c7aa (CI: Add x86_64 Clang Coverage pipeline in Azure, 2022-03-19)

  - [serenity-i686-20220502-44bcd7c.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220502-44bcd7c.vdi.gz) (VirtualBox VDI, 122.63 MiB)
  - [serenity-i686-20220502-44bcd7c.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220502-44bcd7c.img.gz) (Raw image, 122.81 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220502-44bcd7c.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220502-44bcd7c.vdi.gz) (VirtualBox VDI, 122.63 MiB)
- [serenity-i686-20220502-44bcd7c.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220502-44bcd7c.img.gz) (Raw image, 122.81 MiB)

### Last commit :star:

```git
commit 44bcd7c7aa9af41b57d44fe532b1047191c383de
Author:     Andrew Kaster <akaster@serenityos.org>
AuthorDate: Sat Mar 19 16:42:54 2022 -0600
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Mon May 2 01:46:18 2022 +0200

    CI: Add x86_64 Clang Coverage pipeline in Azure
    
    Add a job to the Azure pipelines to run tests with coverage enabled, and
    aggregate the test results in a folder of html pages showing the
    coverage results overall, and per-file.
    
    Future work is needed to take the published pipeline artifact for the
    coverage results and display them somewhere interesting.
```
