---
title: 'SerenityOS build: Sunday, June 26'
date: 2022-06-26T07:16:11Z
author: Buildbot
description: |
  Commit: e85b3fc750 (Ports: Add edid-decode port, 2022-05-21)

  - [serenity-i686-20220626-e85b3fc.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220626-e85b3fc.vdi.gz) (VirtualBox VDI, 124.54 MiB)
  - [serenity-i686-20220626-e85b3fc.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220626-e85b3fc.img.gz) (Raw image, 124.73 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220626-e85b3fc.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220626-e85b3fc.vdi.gz) (VirtualBox VDI, 124.54 MiB)
- [serenity-i686-20220626-e85b3fc.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220626-e85b3fc.img.gz) (Raw image, 124.73 MiB)

### Last commit :star:

```git
commit e85b3fc750414045d47fa3dfa99075b535b9b8af
Author:     Liav A <liavalb@gmail.com>
AuthorDate: Sat May 21 19:40:46 2022 +0300
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Sat Jun 25 12:10:04 2022 +0100

    Ports: Add edid-decode port
```
