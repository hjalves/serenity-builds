---
title: 'SerenityOS build: Friday, April 19'
date: 2024-04-19T05:17:30Z
author: Buildbot
description: |
  Commit: 2a48eff2d6 (CI: Enable KVM on Github Actions runners, 2024-04-18)

  - [serenity-x86_64-20240419-2a48eff.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240419-2a48eff.img.gz) (Raw image, 218.01 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240419-2a48eff.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240419-2a48eff.img.gz) (Raw image, 218.01 MiB)

### Last commit :star:

```git
commit 2a48eff2d653f7185f50ff8bb861c76d59338301
Author:     Andrew Kaster <akaster@serenityos.org>
AuthorDate: Thu Apr 18 15:19:09 2024 -0600
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Thu Apr 18 18:26:36 2024 -0600

    CI: Enable KVM on Github Actions runners
    
    https://github.blog/changelog/2024-04-02-github-actions-hardware-accelerated-android-virtualization-now-available/
```
