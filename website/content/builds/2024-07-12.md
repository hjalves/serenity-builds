---
title: 'SerenityOS build: Friday, July 12'
date: 2024-07-12T05:18:32Z
author: Buildbot
description: |
  Commit: 1dca789d0f (Calendar: Sort Events by start DateTime, 2024-07-10)

  - [serenity-x86_64-20240712-1dca789.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240712-1dca789.img.gz) (Raw image, 219.4 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240712-1dca789.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240712-1dca789.img.gz) (Raw image, 219.4 MiB)

### Last commit :star:

```git
commit 1dca789d0fd27df618f3381fe7feb52f12687890
Author:     Alec Murphy <alec@checksum.fail>
AuthorDate: Wed Jul 10 11:17:25 2024 -0400
Commit:     Sam Atkins <atkinssj@serenityos.org>
CommitDate: Thu Jul 11 07:51:45 2024 +0100

    Calendar: Sort Events by start DateTime
    
    This PR allows Calendar to display Events in chronological order.
```
