---
title: 'SerenityOS build: Monday, May 06'
date: 2024-05-06T05:17:54Z
author: Buildbot
description: |
  Commit: 9065b0b0f6 (LibJS: Truncate roundingIncrement before range check, 2024-03-25)

  - [serenity-x86_64-20240506-9065b0b.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240506-9065b0b.img.gz) (Raw image, 221.45 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240506-9065b0b.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240506-9065b0b.img.gz) (Raw image, 221.45 MiB)

### Last commit :star:

```git
commit 9065b0b0f6ab050afc7e0c3a5ae7180919471d68
Author:     Daniel La Rocque <larocq17@myumanitoba.ca>
AuthorDate: Mon Mar 25 17:10:18 2024 -0500
Commit:     Luke Wilde <25595356+Lubrsi@users.noreply.github.com>
CommitDate: Sun May 5 19:59:26 2024 +0100

    LibJS: Truncate roundingIncrement before range check
    
    This is a normative change in the Temporal spec.
    
    See: https://github.com/tc39/proposal-temporal/commit/8fc8130
```
