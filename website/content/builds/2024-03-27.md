---
title: 'SerenityOS build: Wednesday, March 27'
date: 2024-03-27T06:15:52Z
author: Buildbot
description: |
  Commit: 1e1906865c (LibJS: Remove ToTemporalDateTimeRoundingIncrement, 2024-03-25)

  - [serenity-x86_64-20240327-1e19068.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240327-1e19068.img.gz) (Raw image, 220.79 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240327-1e19068.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240327-1e19068.img.gz) (Raw image, 220.79 MiB)

### Last commit :star:

```git
commit 1e1906865c9f4dcf085d690adc7efc5e86fbe4f6
Author:     Daniel La Rocque <larocq17@myumanitoba.ca>
AuthorDate: Mon Mar 25 11:18:42 2024 -0500
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Tue Mar 26 17:22:46 2024 -0400

    LibJS: Remove ToTemporalDateTimeRoundingIncrement
    
    This is an editorial change in the Temporal spec.
    
    See: https://github.com/tc39/proposal-temporal/commit/aca38be
```
