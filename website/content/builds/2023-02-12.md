---
title: 'SerenityOS build: Sunday, February 12'
date: 2023-02-12T06:06:04Z
author: Buildbot
description: |
  Commit: 2f20f16292 (Base: Add a test using FormData and FormDataEvent, 2023-02-03)

  - [serenity-x86_64-20230212-2f20f16.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230212-2f20f16.img.gz) (Raw image, 162.93 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230212-2f20f16.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230212-2f20f16.img.gz) (Raw image, 162.93 MiB)

### Last commit :star:

```git
commit 2f20f16292d82629980a4f18c42856d5e25e263d
Author:     Kenneth Myhra <kennethmyhra@serenityos.org>
AuthorDate: Fri Feb 3 21:50:36 2023 +0100
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Sun Feb 12 00:18:09 2023 +0000

    Base: Add a test using FormData and FormDataEvent
```
