---
title: 'SerenityOS build: Friday, February 17'
date: 2023-02-17T06:00:58Z
author: Buildbot
description: |
  Commit: be717edd33 (PixelPaint: Propagate errors from making tool property widgets, 2023-02-12)

  - [serenity-x86_64-20230217-be717ed.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230217-be717ed.img.gz) (Raw image, 163.46 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230217-be717ed.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230217-be717ed.img.gz) (Raw image, 163.46 MiB)

### Last commit :star:

```git
commit be717edd339c6d8c1c94e2ff1b203e443e5c36b3
Author:     Karol Kosek <krkk@serenityos.org>
AuthorDate: Sun Feb 12 00:04:02 2023 +0100
Commit:     Sam Atkins <atkinssj@serenityos.org>
CommitDate: Thu Feb 16 23:36:58 2023 +0000

    PixelPaint: Propagate errors from making tool property widgets
```
