---
title: 'SerenityOS build: Friday, August 12'
date: 2022-08-12T03:06:30Z
author: Buildbot
description: |
  Commit: 5099f1da2a (Base: Add more emoji, 2022-08-11)

  - [serenity-i686-20220812-5099f1d.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220812-5099f1d.img.gz) (Raw image, 127.84 MiB)
  - [serenity-i686-20220812-5099f1d.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220812-5099f1d.vdi.gz) (VirtualBox VDI, 127.67 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220812-5099f1d.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220812-5099f1d.img.gz) (Raw image, 127.84 MiB)
- [serenity-i686-20220812-5099f1d.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220812-5099f1d.vdi.gz) (VirtualBox VDI, 127.67 MiB)

### Last commit :star:

```git
commit 5099f1da2a786bbc0574428139f1705f91fffb3a
Author:     Xexxa <93391300+Xexxa@users.noreply.github.com>
AuthorDate: Thu Aug 11 20:01:23 2022 +0200
Commit:     Sam Atkins <atkinssj@serenityos.org>
CommitDate: Thu Aug 11 21:23:24 2022 +0100

    Base: Add more emoji
    
    🗽 - U+1F5FD STATUE OF LIBERTY
    🌛 - U+1F31B FIRST QUARTER MOON FACE
    🕊 - U+1F54A DOVE
    ✋ - U+270B RAISED HAND
    🍔 - U+1F354 HAMBURGER
    ⛽ - U+26FD FUEL PUMP
    🌜 - U+1F31C LAST QUARTER MOON FACE
    💣 - U+1F4A3 BOMB
    🤖 - U+1F916 ROBOT
    👽 - U+1F47D ALIEN
    🧲 - U+1F9F2 MAGNET
    🕯 - U+1F56F CANDLE
    🛡 - U+1F6E1 SHIELD
    🍍 - U+1F34D PINEAPPLE
    🍓 - U+1F353 STRAWBERRY
    📌 - U+1F4CC PUSHPIN
    📍 - U+1F4CD ROUND PUSHPIN
    🇦🇪 - U+1F1E6 U+1F1EA AE United Arab Emirates
    🇧🇻 - U+1F1E7 U+1F1FB BV BOUVET ISLAND
    🇧🇮 - U+1F1E7 U+1F1EE BI Burundi
    🇨🇵 - U+1F1E8 U+1F1F5 CP Clipperton Island
    🇨🇷 - U+1F1E8 U+1F1F7 CR Costa Rica
    🇨🇺 - U+1F1E8 U+1F1FA CU Cuba
    🇩🇴 - U+1F1E9 U+1F1F4 DO Dominican Republic
    🇰🇳 - U+1F1F0 U+1F1F3 KN St. Kitts & Nevis
    🇱🇦 - U+1F1F1 U+1F1E6 LA Laos
    🇱🇧 - U+1F1F1 U+1F1E7 LB Lebanon
    🇵🇦 - U+1F1F5 U+1F1E6 PA Panama
    🇵🇼 - U+1F1F5 U+1F1FC PW Palau
    🇸🇯 - U+1F1F8 U+1F1EF SJ SVALBARD & JAN MAYEN
    🇸🇹 - U+1F1F8 U+1F1F9 ST São Tomé & Príncipe
    🇸🇾 - U+1F1F8 U+1F1FE SY Syria
    🐱‍🐶 - U+1F431 U+200D U+1F436 CATDOG FACE
```
