---
title: 'SerenityOS build: Saturday, February 17'
date: 2024-02-17T06:15:11Z
author: Buildbot
description: |
  Commit: 5d2a36f244 (LibWeb: Stub out all the functions from the execCommand spec, 2024-02-15)

  - [serenity-x86_64-20240217-5d2a36f.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240217-5d2a36f.img.gz) (Raw image, 215.52 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240217-5d2a36f.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240217-5d2a36f.img.gz) (Raw image, 215.52 MiB)

### Last commit :star:

```git
commit 5d2a36f244b16fe17844b388b54e521932404171
Author:     Andrew Kaster <akaster@serenityos.org>
AuthorDate: Thu Feb 15 16:47:29 2024 -0700
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Fri Feb 16 19:31:54 2024 -0500

    LibWeb: Stub out all the functions from the execCommand spec
    
    Per the specification, it's ok if we say that nothing is supported.
    
    It's not ok if we say something is supported but do nothing, apparently.
```
