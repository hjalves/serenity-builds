---
title: 'SerenityOS build: Wednesday, February 15'
date: 2023-02-15T06:39:12Z
author: Buildbot
description: |
  Commit: 82470e9266 (Documentation: Fix Ladybird debug instructions, 2023-02-13)

  - [serenity-x86_64-20230215-82470e9.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230215-82470e9.img.gz) (Raw image, 163.06 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230215-82470e9.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230215-82470e9.img.gz) (Raw image, 163.06 MiB)

### Last commit :star:

```git
commit 82470e92669e599f25debd11b0f5841caa8fc885
Author:     Yedaya Katsman <yedaya.ka@gmail.com>
AuthorDate: Mon Feb 13 22:01:32 2023 +0200
Commit:     Jelle Raaijmakers <jelle@gmta.nl>
CommitDate: Tue Feb 14 19:38:29 2023 +0100

    Documentation: Fix Ladybird debug instructions
    
    The current Meta/serenity.sh script only accepts the `gdb` subcommand,
    not `debug`.
```
