---
title: 'SerenityOS build: Saturday, March 04'
date: 2023-03-04T06:02:05Z
author: Buildbot
description: |
  Commit: a829160d55 (Base: Add more emoji, 2023-03-04)

  - [serenity-x86_64-20230304-a829160.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230304-a829160.img.gz) (Raw image, 166.23 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230304-a829160.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230304-a829160.img.gz) (Raw image, 166.23 MiB)

### Last commit :star:

```git
commit a829160d554db3d92a81e05d7838541d206d7555
Author:     Xexxa <93391300+Xexxa@users.noreply.github.com>
AuthorDate: Sat Mar 4 01:04:49 2023 +0100
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Sat Mar 4 00:16:30 2023 +0000

    Base: Add more emoji
    
    🏻 - U+1F3FB LIGHT SKIN TONE
    🏼 - U+1F3FC MEDIUM-LIGHT SKIN TONE
    🏽 - U+1F3FD MEDIUM SKIN TONE
    🏾 - U+1F3FE MEDIUM-DARK SKIN TONE
    🏿 - U+1F3FF DARK SKIN TONE
```
