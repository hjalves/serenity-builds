---
title: 'SerenityOS build: Thursday, July 28'
date: 2022-07-28T03:05:36Z
author: Buildbot
description: |
  Commit: ef2d4b9ed6 (Base: Add sort(1) man page, 2022-07-28)

  - [serenity-i686-20220728-ef2d4b9.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220728-ef2d4b9.img.gz) (Raw image, 127.87 MiB)
  - [serenity-i686-20220728-ef2d4b9.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220728-ef2d4b9.vdi.gz) (VirtualBox VDI, 127.68 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220728-ef2d4b9.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220728-ef2d4b9.img.gz) (Raw image, 127.87 MiB)
- [serenity-i686-20220728-ef2d4b9.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220728-ef2d4b9.vdi.gz) (VirtualBox VDI, 127.68 MiB)

### Last commit :star:

```git
commit ef2d4b9ed6b7e69e51a8038cf8a881c88e32b83b
Author:     demostanis <demostanis@protonmail.com>
AuthorDate: Thu Jul 28 01:22:57 2022 +0200
Commit:     Brian Gianforcaro <bgianf@serenityos.org>
CommitDate: Thu Jul 28 00:44:08 2022 +0000

    Base: Add sort(1) man page
```
