---
title: 'SerenityOS build: Friday, December 08'
date: 2023-12-08T06:10:47Z
author: Buildbot
description: |
  Commit: 8118ea764f (NetworkSettings: Port NetworkSettings to GML compilation, 2023-09-29)

  - [serenity-x86_64-20231208-8118ea7.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231208-8118ea7.img.gz) (Raw image, 205.58 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231208-8118ea7.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231208-8118ea7.img.gz) (Raw image, 205.58 MiB)

### Last commit :star:

```git
commit 8118ea764fdf72c42903581d86a14e908726f6ec
Author:     tetektoza <skociektoja@gmail.com>
AuthorDate: Fri Sep 29 20:33:46 2023 +0200
Commit:     Tim Schumacher <timschumi@gmx.de>
CommitDate: Fri Dec 8 01:06:09 2023 +0100

    NetworkSettings: Port NetworkSettings to GML compilation
```
