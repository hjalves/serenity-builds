---
title: 'SerenityOS build: Monday, September 02'
date: 2024-09-02T05:22:12Z
author: Buildbot
description: |
  Commit: 0482f4e117 (Kernel: Remove passing of register state to IRQ handlers, 2024-08-21)

  - [serenity-x86_64-20240902-0482f4e.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240902-0482f4e.img.gz) (Raw image, 221.47 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240902-0482f4e.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240902-0482f4e.img.gz) (Raw image, 221.47 MiB)

### Last commit :star:

```git
commit 0482f4e1170010dccec1726ee5222a062f4b3262
Author:     Liav A. <liavalb@gmail.com>
AuthorDate: Wed Aug 21 22:06:07 2024 +0300
Commit:     Tim Schumacher <timschumi@gmx.de>
CommitDate: Sun Sep 1 21:00:18 2024 +0200

    Kernel: Remove passing of register state to IRQ handlers
    
    Linux did the same thing 18 years ago and their reasons for the change
    are similar to ours - https://github.com/torvalds/linux/commit/7d12e78
    
    Most interrupt handlers (i.e. IRQ handlers) never used the register
    state reference anywhere so there's simply no need of passing it around.
    I didn't measure the performance boost but surely this change can't make
    things worse anyway.
```
