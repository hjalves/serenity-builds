---
title: 'SerenityOS build: Thursday, September 01'
date: 2022-09-01T03:06:48Z
author: Buildbot
description: |
  Commit: f2da577e77 (PixelPaint: Change repeated code into a loop for Wand Select Tool, 2022-08-31)

  - [serenity-i686-20220901-f2da577.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220901-f2da577.img.gz) (Raw image, 130 MiB)
  - [serenity-i686-20220901-f2da577.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220901-f2da577.vdi.gz) (VirtualBox VDI, 129.82 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220901-f2da577.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220901-f2da577.img.gz) (Raw image, 130 MiB)
- [serenity-i686-20220901-f2da577.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220901-f2da577.vdi.gz) (VirtualBox VDI, 129.82 MiB)

### Last commit :star:

```git
commit f2da577e7739b4203df4d1321993f33f4b16bc4f
Author:     Timothy Slater <tslater2006@gmail.com>
AuthorDate: Wed Aug 31 11:29:56 2022 -0500
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Wed Aug 31 18:19:46 2022 +0100

    PixelPaint: Change repeated code into a loop for Wand Select Tool
```
