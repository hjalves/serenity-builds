---
title: 'SerenityOS build: Friday, September 22'
date: 2023-09-22T05:09:55Z
author: Buildbot
description: |
  Commit: 3d5cd23393 (LibJS: Remove unused Instruction::is_terminator(), 2023-09-21)

  - [serenity-x86_64-20230922-3d5cd23.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230922-3d5cd23.img.gz) (Raw image, 186.06 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230922-3d5cd23.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230922-3d5cd23.img.gz) (Raw image, 186.06 MiB)

### Last commit :star:

```git
commit 3d5cd23393cc984017b4795619138f1e71e71414
Author:     Andreas Kling <kling@serenityos.org>
AuthorDate: Thu Sep 21 09:41:25 2023 +0200
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Thu Sep 21 16:19:13 2023 +0200

    LibJS: Remove unused Instruction::is_terminator()
```
