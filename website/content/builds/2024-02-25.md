---
title: 'SerenityOS build: Sunday, February 25'
date: 2024-02-25T06:13:56Z
author: Buildbot
description: |
  Commit: 71a784741f (LibGUI: Add Calendar property for mode, 2024-02-13)

  - [serenity-x86_64-20240225-71a7847.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240225-71a7847.img.gz) (Raw image, 217.51 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240225-71a7847.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240225-71a7847.img.gz) (Raw image, 217.51 MiB)

### Last commit :star:

```git
commit 71a784741f1be8d7dd0e04534b0f2f8f0885b6c6
Author:     Domenico Iezzi <domenico@iezzi.info>
AuthorDate: Tue Feb 13 21:57:17 2024 +0100
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Sat Feb 24 18:54:51 2024 -0700

    LibGUI: Add Calendar property for mode
    
    If a user selects Year as the default view mode for Calendar app, then
    all apps using Calendar widget will default to this view if not
    manually overridden with a Calendar::toggle_mode call.
    
    This commit introduces a "mode" property that allows the selection of
    the default mode for the calendar widget in GML files. In this way
    there is no need to manually call toggle_mode when constructing
    UIs with Calendar widget.
```
