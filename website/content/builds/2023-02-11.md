---
title: 'SerenityOS build: Saturday, February 11'
date: 2023-02-11T13:32:00Z
author: Buildbot
description: |
  Commit: 92cb32b905 (LibWeb: Add tests for flex formatting context, 2023-02-08)

  - [serenity-x86_64-20230211-92cb32b.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230211-92cb32b.img.gz) (Raw image, 162.64 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230211-92cb32b.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230211-92cb32b.img.gz) (Raw image, 162.64 MiB)

### Last commit :star:

```git
commit 92cb32b905481b22bbd4e79f694b3a822c06b900
Author:     Aliaksandr Kalenik <kalenik.aliaksandr@gmail.com>
AuthorDate: Wed Feb 8 01:40:38 2023 +0300
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Sat Feb 11 10:42:52 2023 +0100

    LibWeb: Add tests for flex formatting context
```
