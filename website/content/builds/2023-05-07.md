---
title: 'SerenityOS build: Sunday, May 07'
date: 2023-05-07T05:04:38Z
author: Buildbot
description: |
  Commit: 038283f3fc (LibWeb: Rename function to find table box width inside table wrapper, 2023-05-07)

  - [serenity-x86_64-20230507-038283f.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230507-038283f.img.gz) (Raw image, 181.34 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230507-038283f.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230507-038283f.img.gz) (Raw image, 181.34 MiB)

### Last commit :star:

```git
commit 038283f3fc1066c8c991640d5987358605f03730
Author:     Aliaksandr Kalenik <kalenik.aliaksandr@gmail.com>
AuthorDate: Sun May 7 02:59:39 2023 +0300
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Sun May 7 06:31:53 2023 +0200

    LibWeb: Rename function to find table box width inside table wrapper
    
    compute_table_box_width_inside_table_wrapper should be a better name
    considering what this function does.
```
