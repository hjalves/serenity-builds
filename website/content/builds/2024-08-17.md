---
title: 'SerenityOS build: Saturday, August 17'
date: 2024-08-17T05:26:39Z
author: Buildbot
description: |
  Commit: e7b8eed005 (Kernel: Share Processor::capture_stack_trace() between architectures, 2024-08-15)

  - [serenity-x86_64-20240817-e7b8eed.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240817-e7b8eed.img.gz) (Raw image, 221.27 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240817-e7b8eed.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240817-e7b8eed.img.gz) (Raw image, 221.27 MiB)

### Last commit :star:

```git
commit e7b8eed005dd15138b85775f9e5905e22ce15010
Author:     Sönke Holz <sholz8530@gmail.com>
AuthorDate: Thu Aug 15 22:12:34 2024 +0200
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Fri Aug 16 16:01:19 2024 -0400

    Kernel: Share Processor::capture_stack_trace() between architectures
    
    This makes Processor::capture_stack_trace() work on all our
    architectures. For this function to work on AArch64 and RISC-V, the
    frame pointer has to be saved during context switches.
    
    AArch64 and RISC-V don't support SMP yet, so the code for getting a
    backtrace for processes running on other cores is guarded behind a
    '#if ARCH(X86_64)'.
```
