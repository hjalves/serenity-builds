---
title: 'SerenityOS build: Friday, June 07'
date: 2024-06-07T05:18:12Z
author: Buildbot
description: |
  Commit: 3bb41e942f (LibCore: Null-check struct addrinfo to avoid freeaddrinfo(NULL), 2024-05-09)

  - [serenity-x86_64-20240607-3bb41e9.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240607-3bb41e9.img.gz) (Raw image, 217.97 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240607-3bb41e9.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240607-3bb41e9.img.gz) (Raw image, 217.97 MiB)

### Last commit :star:

```git
commit 3bb41e942faecf54ad58b4aa2e33232095e3c474
Author:     Andrew Kaster <akaster@serenityos.org>
AuthorDate: Thu May 9 15:13:48 2024 -0600
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Thu Jun 6 09:27:50 2024 -0600

    LibCore: Null-check struct addrinfo to avoid freeaddrinfo(NULL)
    
    On some C libraries, like NetBSD and musl-libc, this under-specified
    edge case results in a crash rather than silently ignoring the null
    pointer.
```
