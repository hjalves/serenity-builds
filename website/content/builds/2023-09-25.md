---
title: 'SerenityOS build: Monday, September 25'
date: 2023-09-25T05:10:01Z
author: Buildbot
description: |
  Commit: 707ca984bd (LibWeb: Fix memory leak in CSS::ImageStyleValue, 2023-09-24)

  - [serenity-x86_64-20230925-707ca98.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230925-707ca98.img.gz) (Raw image, 186.24 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230925-707ca98.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230925-707ca98.img.gz) (Raw image, 186.24 MiB)

### Last commit :star:

```git
commit 707ca984bdb1f7d098d9275e319c881d31e211df
Author:     Aliaksandr Kalenik <kalenik.aliaksandr@gmail.com>
AuthorDate: Sun Sep 24 18:13:39 2023 +0200
Commit:     Alexander Kalenik <kalenik.aliaksandr@gmail.com>
CommitDate: Mon Sep 25 04:16:36 2023 +0200

    LibWeb: Fix memory leak in CSS::ImageStyleValue
    
    Before this change, whenever ImageStyleValue had a non-null
    `m_image_request`, it was always leaked along with everything related
    to the document to which this value belongs. The issue arises due to
    the use of `JS::Handle` for the image request, as it introduces a
    cyclic dependency where `ImageRequest` prevents the `CSSStyleSheet`,
    that owns `ImageStyleValue`, from being deallocated:
    - ImageRequest
    - FetchController
    - FetchParams
    - Window
    - HTMLDocument
    - HTMLHtmlElement
    - HTMLBodyElement
    - Text
    - HTMLHeadElement
    - Text
    - HTMLMetaElement
    - Text
    - HTMLTitleElement
    - Text
    - HTMLStyleElement
    - CSSStyleSheet
    
    This change solves this by visiting `m_image_request` from
    `visit_edges` instead of introducing new heap root by using
    `JS::Handle`.
```
