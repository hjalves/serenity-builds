---
title: 'SerenityOS build: Saturday, March 01'
date: 2025-03-01T06:23:01Z
author: Buildbot
description: |
  Commit: 09d4354177 (Nix: Use the unwrapped clang package, 2025-02-25)

  - [serenity-x86_64-20250301-09d4354.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250301-09d4354.img.gz) (Raw image, 242.48 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20250301-09d4354.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250301-09d4354.img.gz) (Raw image, 242.48 MiB)

### Last commit :star:

```git
commit 09d43541779947c38ad091c0bea960b32ce3c0e1
Author:     Vinicius Deolindo <andrade.vinicius934@gmail.com>
AuthorDate: Tue Feb 25 15:05:07 2025 -0300
Commit:     Tim Schumacher <timschumi@gmx.de>
CommitDate: Fri Feb 28 15:52:26 2025 +0100

    Nix: Use the unwrapped clang package
```
