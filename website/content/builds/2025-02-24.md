---
title: 'SerenityOS build: Monday, February 24'
date: 2025-02-24T06:23:15Z
author: Buildbot
description: |
  Commit: e10d5a4a23 (LibPDF: Implement painting of extensions of radial shadings, 2025-02-22)

  - [serenity-x86_64-20250224-e10d5a4.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250224-e10d5a4.img.gz) (Raw image, 242.11 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20250224-e10d5a4.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250224-e10d5a4.img.gz) (Raw image, 242.11 MiB)

### Last commit :star:

```git
commit e10d5a4a232d5e0005315e239753b920bb142d18
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Sat Feb 22 19:51:02 2025 -0500
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Sun Feb 23 11:15:58 2025 -0500

    LibPDF: Implement painting of extensions of radial shadings
    
    I couldn't come up with anything nicer than listing many special cases.
    On the plus side, this seems to look correct in all cases I tried.
    I found it pretty easy to get confused and come up with a simplification
    that in fact broke something. Tests/LibPDF/shade-radial.pdf tests quite
    a few cases to prevent this. (It's a manual test for now, though.)
```
