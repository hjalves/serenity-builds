---
title: 'SerenityOS build: Saturday, February 24'
date: 2024-02-24T06:14:18Z
author: Buildbot
description: |
  Commit: 9b1b8828b4 (Ports: Add xmp-cli (Extended Module Player), 2024-02-23)

  - [serenity-x86_64-20240224-9b1b882.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240224-9b1b882.img.gz) (Raw image, 216.89 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240224-9b1b882.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240224-9b1b882.img.gz) (Raw image, 216.89 MiB)

### Last commit :star:

```git
commit 9b1b8828b4c6a0b98bd8cd8f8fcda5211aaf767f
Author:     Julian Offenhäuser <offenhaeuser@protonmail.com>
AuthorDate: Fri Feb 23 23:03:37 2024 +0100
Commit:     Tim Schumacher <timschumi@gmx.de>
CommitDate: Sat Feb 24 02:55:05 2024 +0100

    Ports: Add xmp-cli (Extended Module Player)
```
