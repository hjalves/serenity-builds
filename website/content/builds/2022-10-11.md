---
title: 'SerenityOS build: Tuesday, October 11'
date: 2022-10-11T03:58:07Z
author: Buildbot
description: |
  Commit: 0f2d5f91dc (LibWeb: Fix two uninitialized variables in FormattingContext, 2022-10-10)

  - [serenity-x86_64-20221011-0f2d5f9.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221011-0f2d5f9.vdi.gz) (VirtualBox VDI, 134.81 MiB)
  - [serenity-x86_64-20221011-0f2d5f9.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221011-0f2d5f9.img.gz) (Raw image, 135.15 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221011-0f2d5f9.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221011-0f2d5f9.vdi.gz) (VirtualBox VDI, 134.81 MiB)
- [serenity-x86_64-20221011-0f2d5f9.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221011-0f2d5f9.img.gz) (Raw image, 135.15 MiB)

### Last commit :star:

```git
commit 0f2d5f91dc207190d60a5a1bee4ca1d19707e224
Author:     Andreas Kling <kling@serenityos.org>
AuthorDate: Mon Oct 10 23:42:15 2022 +0200
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Mon Oct 10 23:42:35 2022 +0200

    LibWeb: Fix two uninitialized variables in FormattingContext
```
