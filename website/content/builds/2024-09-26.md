---
title: 'SerenityOS build: Thursday, September 26'
date: 2024-09-26T05:22:24Z
author: Buildbot
description: |
  Commit: 5ba5a599de (LibGfx/Path: Make round end caps show up more consistently, 2024-09-25)

  - [serenity-x86_64-20240926-5ba5a59.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240926-5ba5a59.img.gz) (Raw image, 221.66 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240926-5ba5a59.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240926-5ba5a59.img.gz) (Raw image, 221.66 MiB)

### Last commit :star:

```git
commit 5ba5a599de0185d38bfb5cf4c9c7fc93e844aed4
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Wed Sep 25 10:28:14 2024 -0400
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Wed Sep 25 15:59:58 2024 -0400

    LibGfx/Path: Make round end caps show up more consistently
    
    On macOS, when going from the right end of a horizontal line
    to the left, and when slope_now is pi, and the current
    range.end is 0 (i.e. current_angle = pi, target_angle = 0),
    clockwise() would set target_angle to 2 * pi, and
    `target_angle - current_angle` (ie 2 * pi - pi) would end up
    being ever so sligthly more than `pi` (handwavingly due to
    floating point in accuracies), and we'd go in the wrong direction.
    
    Add an explicit check for that, and a reftest that catches this.
    
    No observed behavior change on linux, but the reftest only passes
    on macOS with the code change.
```
