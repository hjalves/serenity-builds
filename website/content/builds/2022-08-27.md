---
title: 'SerenityOS build: Saturday, August 27'
date: 2022-08-27T03:07:28Z
author: Buildbot
description: |
  Commit: 92295b9584 (LibWeb: Remove one remaining use of JS::InvalidCharacterError, 2022-08-26)

  - [serenity-i686-20220827-92295b9.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220827-92295b9.vdi.gz) (VirtualBox VDI, 129.59 MiB)
  - [serenity-i686-20220827-92295b9.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220827-92295b9.img.gz) (Raw image, 129.77 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220827-92295b9.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220827-92295b9.vdi.gz) (VirtualBox VDI, 129.59 MiB)
- [serenity-i686-20220827-92295b9.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220827-92295b9.img.gz) (Raw image, 129.77 MiB)

### Last commit :star:

```git
commit 92295b9584ee1b3e3974871366c83b3d9944be84
Author:     Linus Groh <mail@linusgroh.de>
AuthorDate: Fri Aug 26 23:19:15 2022 +0100
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Fri Aug 26 23:19:15 2022 +0100

    LibWeb: Remove one remaining use of JS::InvalidCharacterError
```
