---
title: 'SerenityOS build: Tuesday, March 19'
date: 2024-03-19T06:16:09Z
author: Buildbot
description: |
  Commit: e800605ad3 (AK+LibURL: Move AK::URL into a new URL library, 2024-03-18)

  - [serenity-x86_64-20240319-e800605.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240319-e800605.img.gz) (Raw image, 219.26 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240319-e800605.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240319-e800605.img.gz) (Raw image, 219.26 MiB)

### Last commit :star:

```git
commit e800605ad3e4e9dfdc320c137e1beef1a66d2eb0
Author:     Shannon Booth <shannon@serenityos.org>
AuthorDate: Mon Mar 18 16:22:27 2024 +1300
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Mon Mar 18 14:06:28 2024 -0400

    AK+LibURL: Move AK::URL into a new URL library
    
    This URL library ends up being a relatively fundamental base library of
    the system, as LibCore depends on LibURL.
    
    This change has two main benefits:
     * Moving AK back more towards being an agnostic library that can
       be used between the kernel and userspace. URL has never really fit
       that description - and is not used in the kernel.
     * URL _should_ depend on LibUnicode, as it needs punnycode support.
       However, it's not really possible to do this inside of AK as it can't
       depend on any external library. This change brings us a little closer
       to being able to do that, but unfortunately we aren't there quite
       yet, as the code generators depend on LibCore.
```
