---
title: 'SerenityOS build: Monday, February 06'
date: 2023-02-06T06:06:13Z
author: Buildbot
description: |
  Commit: 5ccfd0e49d (LibGfx: Fix comment typo in ICC code, 2023-02-05)

  - [serenity-x86_64-20230206-5ccfd0e.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230206-5ccfd0e.img.gz) (Raw image, 157.74 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230206-5ccfd0e.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230206-5ccfd0e.img.gz) (Raw image, 157.74 MiB)

### Last commit :star:

```git
commit 5ccfd0e49d2884a8896d4d8c66a2c98e446205c1
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Sun Feb 5 18:18:03 2023 -0500
Commit:     Sam Atkins <atkinssj@serenityos.org>
CommitDate: Sun Feb 5 23:20:36 2023 +0000

    LibGfx: Fix comment typo in ICC code
```
