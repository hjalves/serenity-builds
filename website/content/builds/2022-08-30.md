---
title: 'SerenityOS build: Tuesday, August 30'
date: 2022-08-30T03:06:51Z
author: Buildbot
description: |
  Commit: f4b3bb519f (LibJS: Handle non-decimal integer literals in Value::to_number, 2022-08-26)

  - [serenity-i686-20220830-f4b3bb5.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220830-f4b3bb5.img.gz) (Raw image, 129.9 MiB)
  - [serenity-i686-20220830-f4b3bb5.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220830-f4b3bb5.vdi.gz) (VirtualBox VDI, 129.73 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220830-f4b3bb5.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220830-f4b3bb5.img.gz) (Raw image, 129.9 MiB)
- [serenity-i686-20220830-f4b3bb5.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220830-f4b3bb5.vdi.gz) (VirtualBox VDI, 129.73 MiB)

### Last commit :star:

```git
commit f4b3bb519f4dcf9aa79b68420da4e7250226ecb7
Author:     Slappy826 <summon123@hotmail.com>
AuthorDate: Fri Aug 26 11:14:04 2022 -0500
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Tue Aug 30 01:00:48 2022 +0100

    LibJS: Handle non-decimal integer literals in Value::to_number
    
    Implements support for parsing binary and octal literals, and fixes
    instances where a hex literal is parsed in ways the spec doesn't
    allow.
```
