---
title: 'SerenityOS build: Saturday, December 02'
date: 2023-12-02T06:10:39Z
author: Buildbot
description: |
  Commit: 4c145fdb12 (Ladybird/AppKit: Support enabling the GPU painter, 2023-12-01)

  - [serenity-x86_64-20231202-4c145fd.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231202-4c145fd.img.gz) (Raw image, 204.59 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231202-4c145fd.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231202-4c145fd.img.gz) (Raw image, 204.59 MiB)

### Last commit :star:

```git
commit 4c145fdb12e3b7ab24c16188a81f6023256e1f8e
Author:     Timothy Flynn <trflynn89@pm.me>
AuthorDate: Fri Dec 1 12:32:33 2023 -0500
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Fri Dec 1 20:07:27 2023 -0500

    Ladybird/AppKit: Support enabling the GPU painter
```
