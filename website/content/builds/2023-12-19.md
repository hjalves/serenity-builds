---
title: 'SerenityOS build: Tuesday, December 19'
date: 2023-12-19T06:10:51Z
author: Buildbot
description: |
  Commit: b27a62488c (AK: Add ByteString::must_from_utf8(StringView) for Jakt, 2023-12-18)

  - [serenity-x86_64-20231219-b27a624.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231219-b27a624.img.gz) (Raw image, 206.54 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20231219-b27a624.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20231219-b27a624.img.gz) (Raw image, 206.54 MiB)

### Last commit :star:

```git
commit b27a62488c7fcf6bc39dfd927388f575192d1565
Author:     Andreas Kling <kling@serenityos.org>
AuthorDate: Mon Dec 18 12:41:25 2023 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Mon Dec 18 12:41:25 2023 +0100

    AK: Add ByteString::must_from_utf8(StringView) for Jakt
```
