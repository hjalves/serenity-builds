---
title: 'SerenityOS build: Friday, April 28'
date: 2023-04-28T05:04:10Z
author: Buildbot
description: |
  Commit: 2a1e58f8cc (LibWeb: Consider cell computed height in total row min height of table, 2023-04-28)

  - [serenity-x86_64-20230428-2a1e58f.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230428-2a1e58f.img.gz) (Raw image, 180.05 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230428-2a1e58f.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230428-2a1e58f.img.gz) (Raw image, 180.05 MiB)

### Last commit :star:

```git
commit 2a1e58f8cc0dfe4f1be920bed909aaeeca963a9b
Author:     Aliaksandr Kalenik <kalenik.aliaksandr@gmail.com>
AuthorDate: Fri Apr 28 02:43:48 2023 +0300
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Fri Apr 28 06:17:07 2023 +0200

    LibWeb: Consider cell computed height in total row min height of table
    
    Previously, the minimum height of a table row was calculated based
    on the automatic height of the cells inner layout. This change makes
    computed height of a cell boxes also be considered if it has definite
    value.
```
