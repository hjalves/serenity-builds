---
title: 'SerenityOS build: Sunday, March 26'
date: 2023-03-26T05:02:54Z
author: Buildbot
description: |
  Commit: 30b3c30aba (PixelPaint: Remove unused function definition from Image, 2023-03-26)

  - [serenity-x86_64-20230326-30b3c30.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230326-30b3c30.img.gz) (Raw image, 169.91 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230326-30b3c30.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230326-30b3c30.img.gz) (Raw image, 169.91 MiB)

### Last commit :star:

```git
commit 30b3c30aba082dc9a2fbafdeb34e4a697ee31add
Author:     Tim Ledbetter <timledbetter@gmail.com>
AuthorDate: Sun Mar 26 00:22:16 2023 +0000
Commit:     Jelle Raaijmakers <jelle@gmta.nl>
CommitDate: Sun Mar 26 01:49:58 2023 +0100

    PixelPaint: Remove unused function definition from Image
```
