---
title: 'SerenityOS build: Sunday, January 12'
date: 2025-01-12T06:23:02Z
author: Buildbot
description: |
  Commit: 524f699ae2 (Kernel+LibC: Clean up assembly code, 2025-01-09)

  - [serenity-x86_64-20250112-524f699.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250112-524f699.img.gz) (Raw image, 240.88 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20250112-524f699.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250112-524f699.img.gz) (Raw image, 240.88 MiB)

### Last commit :star:

```git
commit 524f699ae20f57f8dcd28be1042a29e34c8b96b5
Author:     Sönke Holz <sholz8530@gmail.com>
AuthorDate: Thu Jan 9 19:40:35 2025 +0100
Commit:     Sönke Holz <sholz8530@gmail.com>
CommitDate: Fri Jan 10 12:31:16 2025 +0100

    Kernel+LibC: Clean up assembly code
    
    Some assembly files were previously inconsistently formatted.
    
    - Use 4 spaces for indentation
    - Use `.L` as the local label prefix
    - Remove trailing whitespace
```
