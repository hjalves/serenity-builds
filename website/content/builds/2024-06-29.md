---
title: 'SerenityOS build: Saturday, June 29'
date: 2024-06-29T05:18:33Z
author: Buildbot
description: |
  Commit: 225108c830 (LibWeb: Set the correct prototype for SVGAElement instances, 2024-06-28)

  - [serenity-x86_64-20240629-225108c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240629-225108c.img.gz) (Raw image, 218.83 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240629-225108c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240629-225108c.img.gz) (Raw image, 218.83 MiB)

### Last commit :star:

```git
commit 225108c830561fc8e853eca7bbca7c5ca4414ff9
Author:     Andreas Kling <andreas@ladybird.org>
AuthorDate: Fri Jun 28 14:51:57 2024 +0200
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Fri Jun 28 23:43:59 2024 +0200

    LibWeb: Set the correct prototype for SVGAElement instances
    
    (cherry picked from commit 4db05ecf69bee07a060f2e4513e9d53b6110dcc4)
```
