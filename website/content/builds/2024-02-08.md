---
title: 'SerenityOS build: Thursday, February 08'
date: 2024-02-08T06:13:14Z
author: Buildbot
description: |
  Commit: e8a20b3c3e (Ports: Update mc to 4.8.31, 2024-02-06)

  - [serenity-x86_64-20240208-e8a20b3.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240208-e8a20b3.img.gz) (Raw image, 214.35 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240208-e8a20b3.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240208-e8a20b3.img.gz) (Raw image, 214.35 MiB)

### Last commit :star:

```git
commit e8a20b3c3e4af1dff6e88c108166fab7d35332d2
Author:     Kenneth Myhra <kennethmyhra@serenityos.org>
AuthorDate: Tue Feb 6 20:12:34 2024 +0100
Commit:     Tim Schumacher <timschumi@gmx.de>
CommitDate: Wed Feb 7 15:04:21 2024 +0100

    Ports: Update mc to 4.8.31
```
