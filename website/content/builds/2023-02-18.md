---
title: 'SerenityOS build: Saturday, February 18'
date: 2023-02-18T06:01:04Z
author: Buildbot
description: |
  Commit: 4cd3a84c4b (AK: Remove unused `rehash_for_collision`, 2023-02-14)

  - [serenity-x86_64-20230218-4cd3a84.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230218-4cd3a84.img.gz) (Raw image, 163.12 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230218-4cd3a84.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230218-4cd3a84.img.gz) (Raw image, 163.12 MiB)

### Last commit :star:

```git
commit 4cd3a84c4b094783377b940f9a464eebf599b169
Author:     Jelle Raaijmakers <jelle@gmta.nl>
AuthorDate: Tue Feb 14 01:35:10 2023 +0100
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Fri Feb 17 22:29:51 2023 -0700

    AK: Remove unused `rehash_for_collision`
```
