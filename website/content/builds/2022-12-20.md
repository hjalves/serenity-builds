---
title: 'SerenityOS build: Tuesday, December 20'
date: 2022-12-20T13:36:33Z
author: Buildbot
description: |
  Commit: 9a2ee5a9dd (AK: Add DeprecatedString::find_last(StringView), 2022-12-15)

  - [serenity-x86_64-20221220-9a2ee5a.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221220-9a2ee5a.vdi.gz) (VirtualBox VDI, 144.5 MiB)
  - [serenity-x86_64-20221220-9a2ee5a.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221220-9a2ee5a.img.gz) (Raw image, 144.81 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20221220-9a2ee5a.vdi.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221220-9a2ee5a.vdi.gz) (VirtualBox VDI, 144.5 MiB)
- [serenity-x86_64-20221220-9a2ee5a.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20221220-9a2ee5a.img.gz) (Raw image, 144.81 MiB)

### Last commit :star:

```git
commit 9a2ee5a9dd792359440429832ac564f5dde2b315
Author:     Agustin Gianni <agustingianni@gmail.com>
AuthorDate: Thu Dec 15 21:20:14 2022 +0000
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Tue Dec 20 11:24:05 2022 +0100

    AK: Add DeprecatedString::find_last(StringView)
    
    This adds the the method DeprecatedString::find_last() as wrapper for
    StringUtils::find_last for the StringView type.
```
