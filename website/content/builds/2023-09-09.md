---
title: 'SerenityOS build: Saturday, September 09'
date: 2023-09-09T05:26:21Z
author: Buildbot
description: |
  Commit: ce556c9566 (Ports: Remove the ability to override `fetch` and `patch_internal`, 2023-09-02)

  - [serenity-x86_64-20230909-ce556c9.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230909-ce556c9.img.gz) (Raw image, 186.57 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230909-ce556c9.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230909-ce556c9.img.gz) (Raw image, 186.57 MiB)

### Last commit :star:

```git
commit ce556c95660d79a333f8d05b41b3140393c4b5cc
Author:     Tim Schumacher <timschumi@gmx.de>
AuthorDate: Sat Sep 2 09:21:01 2023 +0200
Commit:     Tim Schumacher <timschumi@gmx.de>
CommitDate: Sat Sep 9 01:06:31 2023 +0200

    Ports: Remove the ability to override `fetch` and `patch_internal`
```
