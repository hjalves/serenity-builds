---
title: 'SerenityOS build: Tuesday, January 17'
date: 2023-01-17T06:09:27Z
author: Buildbot
description: |
  Commit: 87b18f9304 (Meta: Add dependabot configuration to update github actions, 2023-01-15)

  - [serenity-x86_64-20230117-87b18f9.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230117-87b18f9.img.gz) (Raw image, 681.35 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230117-87b18f9.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230117-87b18f9.img.gz) (Raw image, 681.35 MiB)

### Last commit :star:

```git
commit 87b18f9304370149f3c3b1c12cd851f69d81e0d4
Author:     Brian Gianforcaro <bgianf@serenityos.org>
AuthorDate: Sun Jan 15 20:10:51 2023 -0800
Commit:     Brian Gianforcaro <bgianf@serenityos.org>
CommitDate: Mon Jan 16 20:59:23 2023 -0800

    Meta: Add dependabot configuration to update github actions
```
