---
title: 'SerenityOS build: Sunday, May 21'
date: 2023-05-21T05:03:43Z
author: Buildbot
description: |
  Commit: f7185dfa91 (SystemServer: Print useful information when failing to drop privileges, 2023-05-20)

  - [serenity-x86_64-20230521-f7185df.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230521-f7185df.img.gz) (Raw image, 182.39 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230521-f7185df.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230521-f7185df.img.gz) (Raw image, 182.39 MiB)

### Last commit :star:

```git
commit f7185dfa912fa0e0dae555c2d1d7fc013b50aba5
Author:     Liav A <liavalb@gmail.com>
AuthorDate: Sat May 20 21:59:08 2023 +0300
Commit:     Jelle Raaijmakers <jelle@gmta.nl>
CommitDate: Sat May 20 21:44:03 2023 +0200

    SystemServer: Print useful information when failing to drop privileges
    
    It occurred to me that when trying to running "pls pro SOME_URL" with a
    subsequent failure (which will be fixed in a future patch), that a small
    error message was printed to the debug log about "Failed to drop
    privileges (GID=0, UID=0)".
    
    To actually understand where it failed, I added the actual errno to
    printed message which helped me with further debugging, but this could
    easily help others in similar scenarios so let's print the actual error.
```
