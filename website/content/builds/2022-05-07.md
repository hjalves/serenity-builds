---
title: 'SerenityOS build: Saturday, May 07'
date: 2022-05-07T03:35:46Z
author: Buildbot
description: |
  Commit: 05748ed607 (LibJS: Convert Console to use MarkedVector<Value>, 2022-05-07)

  - [serenity-i686-20220507-05748ed.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220507-05748ed.img.gz) (Raw image, 122.93 MiB)
  - [serenity-i686-20220507-05748ed.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220507-05748ed.vdi.gz) (VirtualBox VDI, 122.77 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220507-05748ed.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220507-05748ed.img.gz) (Raw image, 122.93 MiB)
- [serenity-i686-20220507-05748ed.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220507-05748ed.vdi.gz) (VirtualBox VDI, 122.77 MiB)

### Last commit :star:

```git
commit 05748ed607bfd14301a45bb34bb62e14e0b51d28
Author:     Luke Wilde <lukew@serenityos.org>
AuthorDate: Sat May 7 00:20:22 2022 +0100
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Sat May 7 01:22:09 2022 +0200

    LibJS: Convert Console to use MarkedVector<Value>
    
    Using a Vector<Value> is unsafe as GC cannot see the stored values.
    This is then vended to outside users of ConsoleClient, e.g. LibWeb and
    WebContent, which is then outside of LibJS's control.
    
    An example issue is if the client stores it for later use and forgets
    to visit the stored values, meaning they can be destroyed at any time.
    We can save the client from this by vending a MarkedVector<Value> to
    them.
```
