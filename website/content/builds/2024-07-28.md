---
title: 'SerenityOS build: Sunday, July 28'
date: 2024-07-28T05:19:21Z
author: Buildbot
description: |
  Commit: d8dafd6b28 (LibWeb: Check CanvasTextDrawingStyles.font assignment is valid, 2024-07-23)

  - [serenity-x86_64-20240728-d8dafd6.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240728-d8dafd6.img.gz) (Raw image, 221.09 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240728-d8dafd6.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240728-d8dafd6.img.gz) (Raw image, 221.09 MiB)

### Last commit :star:

```git
commit d8dafd6b28afbc5a2153d66e4f7af1e7b82e336c
Author:     Sam Atkins <atkinssj@serenityos.org>
AuthorDate: Tue Jul 23 16:14:26 2024 +0100
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Sat Jul 27 22:54:40 2024 -0400

    LibWeb: Check CanvasTextDrawingStyles.font assignment is valid
    
    Checking that the string parsed for the `font` property is not enough,
    the spec also wants to rule out CSS-wide keywords like `inherit`. The
    simplest way to do so is to check if it's a ShorthandStyleValue, which
    also rules out use of `var()`; this matches other browsers' behaviour.
    
    The newly-added test would previously crash, and now doesn't. :^)
    
    (cherry picked from commit 64d45afd8a650981d4dbab98aa49d47629568f5b)
```
