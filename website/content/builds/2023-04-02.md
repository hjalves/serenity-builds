---
title: 'SerenityOS build: Sunday, April 02'
date: 2023-04-02T05:02:09Z
author: Buildbot
description: |
  Commit: 85d0637058 (LibCompress: Make CanonicalCode::from_bytes() return ErrorOr<>, 2023-04-01)

  - [serenity-x86_64-20230402-85d0637.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230402-85d0637.img.gz) (Raw image, 169.37 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230402-85d0637.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230402-85d0637.img.gz) (Raw image, 169.37 MiB)

### Last commit :star:

```git
commit 85d063705825a92bdb83e4712234a42894c70711
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Sat Apr 1 19:52:38 2023 -0400
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Sun Apr 2 06:19:46 2023 +0200

    LibCompress: Make CanonicalCode::from_bytes() return ErrorOr<>
    
    No intended behavior change.
```
