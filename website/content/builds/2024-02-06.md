---
title: 'SerenityOS build: Tuesday, February 06'
date: 2024-02-06T06:13:52Z
author: Buildbot
description: |
  Commit: 92a628c07c (LibPDF: Always treat `/Subtype /Image` as binary data when dumping, 2024-02-05)

  - [serenity-x86_64-20240206-92a628c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240206-92a628c.img.gz) (Raw image, 214.34 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240206-92a628c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240206-92a628c.img.gz) (Raw image, 214.34 MiB)

### Last commit :star:

```git
commit 92a628c07cb5638a76ebef0642b25b32e2cab51b
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Mon Feb 5 19:36:35 2024 -0500
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Mon Feb 5 21:18:19 2024 -0500

    LibPDF: Always treat `/Subtype /Image` as binary data when dumping
    
    Sometimes, the "is mostly text" heuristic fails for images.
    
    Before:
    
        Build/lagom/bin/pdf --render out.png ~/Downloads/0000/0000521.pdf \
            --page 10 --dump-contents 2>&1 | wc -l
           25709
    
    After:
    
        Build/lagom/bin/pdf --render out.png ~/Downloads/0000/0000521.pdf \
             --page 10 --dump-contents 2>&1 | wc -l
           11376
```
