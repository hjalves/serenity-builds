---
title: 'SerenityOS build: Sunday, November 10'
date: 2024-11-10T06:26:47Z
author: Buildbot
description: |
  Commit: 9f53517674 (Meta: Add {Entries,MediaCapabilities}API, AbstractWorker to GN, 2024-11-09)

  - [serenity-x86_64-20241110-9f53517.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241110-9f53517.img.gz) (Raw image, 226.54 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20241110-9f53517.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241110-9f53517.img.gz) (Raw image, 226.54 MiB)

### Last commit :star:

```git
commit 9f535176742abe622882e2c54aa8eb726d102596
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Sat Nov 9 13:06:16 2024 -0500
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Sat Nov 9 16:08:01 2024 -0500

    Meta: Add {Entries,MediaCapabilities}API, AbstractWorker to GN
```
