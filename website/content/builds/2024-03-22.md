---
title: 'SerenityOS build: Friday, March 22'
date: 2024-03-22T06:15:34Z
author: Buildbot
description: |
  Commit: 50ae3ca659 (Base: Add Emoji, 2024-03-21)

  - [serenity-x86_64-20240322-50ae3ca.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240322-50ae3ca.img.gz) (Raw image, 219.36 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240322-50ae3ca.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240322-50ae3ca.img.gz) (Raw image, 219.36 MiB)

### Last commit :star:

```git
commit 50ae3ca65929f837f0e95a9a57b39c620e4305ed
Author:     Torben Virtmann <Torben.Virtmann@sva.de>
AuthorDate: Thu Mar 21 13:19:54 2024 +0100
Commit:     Sam Atkins <atkinssj@serenityos.org>
CommitDate: Thu Mar 21 21:28:12 2024 +0000

    Base: Add Emoji
```
