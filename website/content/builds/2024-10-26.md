---
title: 'SerenityOS build: Saturday, October 26'
date: 2024-10-26T05:25:05Z
author: Buildbot
description: |
  Commit: a2763f9bbe (LibWeb: Implement canvas lineJoin and miterLimit attributes, 2024-10-25)

  - [serenity-x86_64-20241026-a2763f9.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241026-a2763f9.img.gz) (Raw image, 224.93 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20241026-a2763f9.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241026-a2763f9.img.gz) (Raw image, 224.93 MiB)

### Last commit :star:

```git
commit a2763f9bbe8f4d6498ef67661b4d83d95c8ac4dd
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Fri Oct 25 18:13:29 2024 -0400
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Fri Oct 25 21:30:08 2024 -0400

    LibWeb: Implement canvas lineJoin and miterLimit attributes
```
