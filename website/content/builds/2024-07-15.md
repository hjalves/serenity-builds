---
title: 'SerenityOS build: Monday, July 15'
date: 2024-07-15T05:19:03Z
author: Buildbot
description: |
  Commit: e8bcd3842d (LibWeb: Implement support for scrollbar-gutter, 2024-06-26)

  - [serenity-x86_64-20240715-e8bcd38.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240715-e8bcd38.img.gz) (Raw image, 219.75 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240715-e8bcd38.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240715-e8bcd38.img.gz) (Raw image, 219.75 MiB)

### Last commit :star:

```git
commit e8bcd3842de9df50ebc21d86ae97264b95ecba87
Author:     Luke Warlow <lwarlow@igalia.com>
AuthorDate: Wed Jun 26 23:19:38 2024 +0100
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Sun Jul 14 20:44:46 2024 -0400

    LibWeb: Implement support for scrollbar-gutter
    
    This property is now correctly parsed.
    
    Ladybird always uses overlay scrollbars so this property does nothing.
    
    (cherry picked from commit 662317726549cd2dde4c7902b99f0b83397a3396)
```
