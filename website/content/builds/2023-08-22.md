---
title: 'SerenityOS build: Tuesday, August 22'
date: 2023-08-22T05:07:31Z
author: Buildbot
description: |
  Commit: 434d95ef55 (LibWeb: Add missing promise rejection in `execute_async_script`, 2023-06-17)

  - [serenity-x86_64-20230822-434d95e.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230822-434d95e.img.gz) (Raw image, 185.16 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230822-434d95e.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230822-434d95e.img.gz) (Raw image, 185.16 MiB)

### Last commit :star:

```git
commit 434d95ef55d33f3c7dab86aa7f40f1c8d1e8c7f5
Author:     stelar7 <dudedbz@gmail.com>
AuthorDate: Sat Jun 17 18:17:05 2023 +0200
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Mon Aug 21 13:22:04 2023 -0600

    LibWeb: Add missing promise rejection in `execute_async_script`
```
