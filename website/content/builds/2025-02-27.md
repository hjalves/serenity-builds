---
title: 'SerenityOS build: Thursday, February 27'
date: 2025-02-27T06:23:42Z
author: Buildbot
description: |
  Commit: 796e3888f8 (LibGfx/JPEG2000: Support loading CMYK files, 2025-02-17)

  - [serenity-x86_64-20250227-796e388.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250227-796e388.img.gz) (Raw image, 242.46 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20250227-796e388.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250227-796e388.img.gz) (Raw image, 242.46 MiB)

### Last commit :star:

```git
commit 796e3888f8b1af8516f70eba9ec04511465607cb
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Mon Feb 17 21:31:03 2025 -0500
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Wed Feb 26 16:30:36 2025 -0500

    LibGfx/JPEG2000: Support loading CMYK files
    
    `image` can now operate on CMYK data in jpeg2000 files. This also means
    you now have to explicitly convert them to RGB with a profile to store
    them in most file formats we can write:
    
            Build/lagom/bin/image -o tmp.webp \
                Tests/LibGfx/test-inputs/jpeg2000/cmyk-small.jpf \
                --assign-color-profile \
                    Build/lagom/Root/res/icc/Adobe/CMYK/USWebCoatedSWOP.icc \
                --convert-to-color-profile serenity-sRGB.icc
    
    We _can_ write CMYK .jpg files, so `image` can now transcode CMYK data
    in JPEG2000 files to CMYK jpg files, which is kind of cool.
    
    We can now also show CMYK JPEG2000 files in PDFs.
    
    The big challenge here was to create test files. I haven't found a good
    way to create a reference CMYK image. So instead I figured I'd make a
    small 3x2 test image that has a few well-known colors, but Photoshop
    has trouble writing 3x2 CMYK JPEG2000 files:
    
    https://community.adobe.com/t5/photoshop-ecosystem-bugs/photoshop-writes-cmyk-jpeg2000-file-in-a-way-that-macos-s-preview-app-does-not-decode-correctly/idc-p/15178650#M119489
    
    (...and jasper, openjpeg, and grok can't handle CMYK data in JPEG2000
    files at all.)
    
    I then tried writing a 3x2 image that has the CMYK data I want in
    the RGBA channels, converting that to a JPEG2000 using opj_compress,
    and then changing the colr and cdef boxes. But that required putting
    0 into the alpha channel for several pixels, and Photoshop then was
    smart and set the color values to 0 for these pixels too.
    
    So then I figured I'd write a small program to write this 3x2 RGBA
    image using Serenity's libraries. This worked
    (https://github.com/nico/serenity/tree/mkcmyk), and I modified the
    boxes, and the file showed up fine in Preview.app -- but not in
    Photoshop 😱
    
    In the end, I noticed that if I make the image 4x2 instead of 3x2,
    Photoshop's JPEG2000 saving code path works fine. So that's how
    I created the test files in the end 😅
```
