---
title: 'SerenityOS build: Saturday, April 06'
date: 2024-04-06T05:16:30Z
author: Buildbot
description: |
  Commit: c3217754f1 (LibWeb: Remove a bunch of calls to `to_byte_string`, 2024-04-05)

  - [serenity-x86_64-20240406-c321775.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240406-c321775.img.gz) (Raw image, 221.01 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240406-c321775.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240406-c321775.img.gz) (Raw image, 221.01 MiB)

### Last commit :star:

```git
commit c3217754f19cbf25adde0ecb4dbb5deafb9a5490
Author:     Shannon Booth <shannon@serenityos.org>
AuthorDate: Fri Apr 5 09:26:03 2024 +0200
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Fri Apr 5 20:01:37 2024 -0400

    LibWeb: Remove a bunch of calls to `to_byte_string`
    
    A bunch of this is leftover from pre porting over to new AK::String.
    For example, for functions which previously took a ByteString const&
    now accepting a StringView.
```
