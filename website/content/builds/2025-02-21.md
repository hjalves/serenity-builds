---
title: 'SerenityOS build: Friday, February 21'
date: 2025-02-21T06:23:21Z
author: Buildbot
description: |
  Commit: 87c847cc8d (EFIPrekernel: Make it boot the RISC-V kernel, 2024-07-21)

  - [serenity-x86_64-20250221-87c847c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250221-87c847c.img.gz) (Raw image, 242.09 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20250221-87c847c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20250221-87c847c.img.gz) (Raw image, 242.09 MiB)

### Last commit :star:

```git
commit 87c847cc8daac887afb0672fd8c350f9e575cabc
Author:     Sönke Holz <sholz8530@gmail.com>
AuthorDate: Sun Jul 21 19:28:28 2024 +0200
Commit:     Sönke Holz <sholz8530@gmail.com>
CommitDate: Fri Feb 21 00:41:53 2025 +0100

    EFIPrekernel: Make it boot the RISC-V kernel
    
    This adds all the remaining code necessary to actually boot the kernel.
    This only works on riscv64 for now.
```
