---
title: 'SerenityOS build: Saturday, January 20'
date: 2024-01-20T06:11:42Z
author: Buildbot
description: |
  Commit: a681429dff (LibWeb: Remove DOM element deprecated_get_attribute(), 2024-01-16)

  - [serenity-x86_64-20240120-a681429.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240120-a681429.img.gz) (Raw image, 210.65 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240120-a681429.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240120-a681429.img.gz) (Raw image, 210.65 MiB)

### Last commit :star:

```git
commit a681429dfff65ce24de65dd79a85d1b9ad97e518
Author:     Bastiaan van der Plaat <bastiaan.v.d.plaat@gmail.com>
AuthorDate: Tue Jan 16 19:04:45 2024 +0100
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Fri Jan 19 13:12:54 2024 -0700

    LibWeb: Remove DOM element deprecated_get_attribute()
```
