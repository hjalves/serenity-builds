---
title: 'SerenityOS build: Thursday, September 21'
date: 2023-09-21T05:09:46Z
author: Buildbot
description: |
  Commit: c9297126db (SystemServer: Use correct unix device numbers for null, full and random, 2023-09-17)

  - [serenity-x86_64-20230921-c929712.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230921-c929712.img.gz) (Raw image, 186.26 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230921-c929712.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230921-c929712.img.gz) (Raw image, 186.26 MiB)

### Last commit :star:

```git
commit c9297126db413732577b2247cb67304fa1cd6e74
Author:     Liav A <liavalb@gmail.com>
AuthorDate: Sun Sep 17 21:04:19 2023 +0300
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Wed Sep 20 13:27:09 2023 -0600

    SystemServer: Use correct unix device numbers for null, full and random
    
    This fixes regression made in 446200d6f3bcf3a828e23902e9dbbb44702135d0
```
