---
title: 'SerenityOS build: Monday, February 27'
date: 2023-02-27T06:01:26Z
author: Buildbot
description: |
  Commit: a83f4ec186 (WebServer: Remove a call to String::from_deprecated_string, 2023-02-26)

  - [serenity-x86_64-20230227-a83f4ec.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230227-a83f4ec.img.gz) (Raw image, 164.02 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230227-a83f4ec.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230227-a83f4ec.img.gz) (Raw image, 164.02 MiB)

### Last commit :star:

```git
commit a83f4ec1869549018f7f1b77f395db111050fbb5
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Sun Feb 26 18:00:31 2023 -0500
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Sun Feb 26 21:01:58 2023 -0500

    WebServer: Remove a call to String::from_deprecated_string
    
    guess_mime_type_based_on_filename() returns a StringView, so no
    need to bring DeprecatedString's (implicit) ctor into this.
    
    No behavior change.
```
