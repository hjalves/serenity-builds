---
title: 'SerenityOS build: Wednesday, January 11'
date: 2023-01-11T06:08:14Z
author: Buildbot
description: |
  Commit: d4367f42ba (PDFViewer: Port to Core::Stream::File, 2023-01-09)

  - [serenity-x86_64-20230111-d4367f4.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230111-d4367f4.img.gz) (Raw image, 673.68 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230111-d4367f4.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230111-d4367f4.img.gz) (Raw image, 673.68 MiB)

### Last commit :star:

```git
commit d4367f42ba53c4359fbf7286fd53c9ac1eed1994
Author:     Karol Kosek <krkk@serenityos.org>
AuthorDate: Mon Jan 9 21:28:05 2023 +0100
Commit:     Sam Atkins <atkinssj@serenityos.org>
CommitDate: Tue Jan 10 22:15:23 2023 +0000

    PDFViewer: Port to Core::Stream::File
```
