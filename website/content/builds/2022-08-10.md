---
title: 'SerenityOS build: Wednesday, August 10'
date: 2022-08-10T03:05:59Z
author: Buildbot
description: |
  Commit: d40b0439d8 (Terminal: Propagate more errors, 2022-08-06)

  - [serenity-i686-20220810-d40b043.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220810-d40b043.vdi.gz) (VirtualBox VDI, 127.65 MiB)
  - [serenity-i686-20220810-d40b043.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220810-d40b043.img.gz) (Raw image, 127.84 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220810-d40b043.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220810-d40b043.vdi.gz) (VirtualBox VDI, 127.65 MiB)
- [serenity-i686-20220810-d40b043.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220810-d40b043.img.gz) (Raw image, 127.84 MiB)

### Last commit :star:

```git
commit d40b0439d8fa3c911041dc4c3e1d80c1519a2d77
Author:     Junior Rantila <junior.rantila@gmail.com>
AuthorDate: Sat Aug 6 02:50:22 2022 +0200
Commit:     Brian Gianforcaro <bgianf@serenityos.org>
CommitDate: Tue Aug 9 20:09:33 2022 -0400

    Terminal: Propagate more errors
```
