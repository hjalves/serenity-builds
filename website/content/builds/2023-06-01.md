---
title: 'SerenityOS build: Thursday, June 01'
date: 2023-06-01T05:03:57Z
author: Buildbot
description: |
  Commit: 617edafbf2 (LibGUI: Add support for jumping to a line and column in TextEditor, 2023-05-31)

  - [serenity-x86_64-20230601-617edaf.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230601-617edaf.img.gz) (Raw image, 183.89 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230601-617edaf.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230601-617edaf.img.gz) (Raw image, 183.89 MiB)

### Last commit :star:

```git
commit 617edafbf2f3201744e523015c11b317205ef76d
Author:     Caoimhe <caoimhebyrne06@gmail.com>
AuthorDate: Wed May 31 18:11:46 2023 +0100
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Thu Jun 1 06:26:40 2023 +0200

    LibGUI: Add support for jumping to a line and column in TextEditor
    
    We had support for going to a specific line before, but now we support
    jumping around using the `line:column` format :^)
```
