---
title: 'SerenityOS build: Sunday, August 27'
date: 2023-08-27T05:08:08Z
author: Buildbot
description: |
  Commit: b0eea51335 (LibWeb: Port DOMTokenList from DeprecatedString to String, 2023-08-12)

  - [serenity-x86_64-20230827-b0eea51.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230827-b0eea51.img.gz) (Raw image, 185.58 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230827-b0eea51.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230827-b0eea51.img.gz) (Raw image, 185.58 MiB)

### Last commit :star:

```git
commit b0eea51335d48efac3efe277ee514b4d6591c696
Author:     Shannon Booth <shannon@serenityos.org>
AuthorDate: Sat Aug 12 21:30:21 2023 +1200
Commit:     Andreas Kling <kling@serenityos.org>
CommitDate: Sun Aug 27 05:34:54 2023 +0200

    LibWeb: Port DOMTokenList from DeprecatedString to String
```
