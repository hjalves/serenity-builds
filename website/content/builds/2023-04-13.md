---
title: 'SerenityOS build: Thursday, April 13'
date: 2023-04-13T05:03:40Z
author: Buildbot
description: |
  Commit: ad60a0b522 (Fuzzers: Stop loading audio frames once the end is reached, 2023-04-12)

  - [serenity-x86_64-20230413-ad60a0b.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230413-ad60a0b.img.gz) (Raw image, 178.62 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230413-ad60a0b.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230413-ad60a0b.img.gz) (Raw image, 178.62 MiB)

### Last commit :star:

```git
commit ad60a0b522cede4bf95b7ea93660e9caf0e1930a
Author:     Tim Schumacher <timschumi@gmx.de>
AuthorDate: Wed Apr 12 16:23:24 2023 +0200
Commit:     Tim Flynn <trflynn89@pm.me>
CommitDate: Wed Apr 12 14:03:20 2023 -0400

    Fuzzers: Stop loading audio frames once the end is reached
    
    Previously, the condition was reversed, so we would stop immediately on
    a file that has at least one working chunk, and we would infinitely loop
    on a file with no chunks.
```
