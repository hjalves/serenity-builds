---
title: 'SerenityOS build: Saturday, September 03'
date: 2022-09-03T03:07:24Z
author: Buildbot
description: |
  Commit: c91511b883 (Meta+Tests: Allow running FLAC spec tests, 2022-07-25)

  - [serenity-i686-20220903-c91511b.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220903-c91511b.img.gz) (Raw image, 129.98 MiB)
  - [serenity-i686-20220903-c91511b.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220903-c91511b.vdi.gz) (VirtualBox VDI, 129.79 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220903-c91511b.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220903-c91511b.img.gz) (Raw image, 129.98 MiB)
- [serenity-i686-20220903-c91511b.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220903-c91511b.vdi.gz) (VirtualBox VDI, 129.79 MiB)

### Last commit :star:

```git
commit c91511b883a4a342daecf447a0ca0f1ba0995b03
Author:     kleines Filmröllchen <filmroellchen@serenityos.org>
AuthorDate: Mon Jul 25 13:28:16 2022 +0200
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Fri Sep 2 23:54:50 2022 +0100

    Meta+Tests: Allow running FLAC spec tests
    
    The FLAC "spec tests", or rather the test suite by xiph that exercises
    weird FLAC features and edge cases, can be found at
    https://github.com/ietf-wg-cellar/flac-test-files and is a good
    challenge for our FLAC decoder to become more spec compliant. Running
    these tests is similar to LibWasm spec tests, you need to pass
    INCLUDE_FLAC_SPEC_TESTS to CMake.
    
    As of integrating these tests, 23 out of 63 fail. :yakplus:
```
