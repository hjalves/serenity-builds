---
title: 'SerenityOS build: Saturday, September 07'
date: 2024-09-07T05:22:50Z
author: Buildbot
description: |
  Commit: b72136cad5 (LibCompress: Remove needless check, 2024-08-26)

  - [serenity-x86_64-20240907-b72136c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240907-b72136c.img.gz) (Raw image, 221.47 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20240907-b72136c.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20240907-b72136c.img.gz) (Raw image, 221.47 MiB)

### Last commit :star:

```git
commit b72136cad57bd0d8d2444835e15bf849ec52fdd8
Author:     Nico Weber <thakis@chromium.org>
AuthorDate: Mon Aug 26 02:00:06 2024 -0400
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Fri Sep 6 18:09:29 2024 -0400

    LibCompress: Remove needless check
    
    I added this check in #18216 in commit 6d388249851e71.
    
    Back then, I mentioned that `m_bit_codes` is only used for writing,
    but only added reading support. `CanonicalCode::from_bytes()` sets
    up both tables for reading and writing, so I needed the construction
    of the writing tables to not crash. This check in
    `CanonicalCode::write_symbol()` was dead code back then though.
    
    Later, #24700 added support for writing WebP files, and it can create
    canonical codes with more than 288 symbols. This works just fine, is
    now under test, and this check in `write_symbol()` isn't needed
    (and never was). So remove it again.
    
    No behavior change.
    
    (I saw this in the profiler once, so maybe a tiny speedup for
    writing deflate-compressed data, but on the order of < 2%.)
```
