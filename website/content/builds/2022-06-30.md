---
title: 'SerenityOS build: Thursday, June 30'
date: 2022-06-30T03:05:56Z
author: Buildbot
description: |
  Commit: 1e36224321 (LibWeb: Print unhandled rejections the same way as unhandled exceptions, 2022-06-27)

  - [serenity-i686-20220630-1e36224.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220630-1e36224.img.gz) (Raw image, 125.02 MiB)
  - [serenity-i686-20220630-1e36224.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220630-1e36224.vdi.gz) (VirtualBox VDI, 124.86 MiB)

---

### Images :floppy_disk:

- [serenity-i686-20220630-1e36224.img.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220630-1e36224.img.gz) (Raw image, 125.02 MiB)
- [serenity-i686-20220630-1e36224.vdi.gz](https://serenity-files.halves.dev/builds/serenity-i686-20220630-1e36224.vdi.gz) (VirtualBox VDI, 124.86 MiB)

### Last commit :star:

```git
commit 1e3622432107dc5b47242da88479e0c189e7a488
Author:     Luke Wilde <lukew@serenityos.org>
AuthorDate: Mon Jun 27 19:53:22 2022 +0100
Commit:     Linus Groh <mail@linusgroh.de>
CommitDate: Wed Jun 29 21:21:50 2022 +0100

    LibWeb: Print unhandled rejections the same way as unhandled exceptions
```
