---
title: 'SerenityOS build: Saturday, October 19'
date: 2024-10-19T05:24:56Z
author: Buildbot
description: |
  Commit: c62dd41874 (LibWeb: Move the navigable's cursor position to be owned by the document, 2024-08-02)

  - [serenity-x86_64-20241019-c62dd41.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241019-c62dd41.img.gz) (Raw image, 224.72 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20241019-c62dd41.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20241019-c62dd41.img.gz) (Raw image, 224.72 MiB)

### Last commit :star:

```git
commit c62dd41874b8f58d95e93b4d2a5246766a7aeade
Author:     Timothy Flynn <trflynn89@pm.me>
AuthorDate: Fri Aug 2 07:31:40 2024 -0400
Commit:     Nico Weber <thakis@chromium.org>
CommitDate: Fri Oct 18 21:58:17 2024 -0400

    LibWeb: Move the navigable's cursor position to be owned by the document
    
    Navigables are re-used for navigations within the same tab. Its current
    ownership of the cursor position is a bit ad-hoc, so nothing in the spec
    indicates when to reset the cursor, nor do we manually do so. So when a
    cursor update happens on one page, that cursor is retained on the next
    page.
    
    Instead, let's have the document own the cursor. Each navigation results
    in a new document, thus we don't need to worry about resetting cursors.
    
    This also makes many of the callsites feel nicer. We were previously
    often going from the node, to the document, to the navigable, to the
    cursor. This patch removes the navigable hop.
    
    (cherry picked from commit faebbbc281d1267062770b02fc6d962d92a3bd62)
```
