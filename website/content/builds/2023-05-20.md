---
title: 'SerenityOS build: Saturday, May 20'
date: 2023-05-20T05:03:52Z
author: Buildbot
description: |
  Commit: 28d2e26678 (Kernel: Enable data and instruction cache on aarch64, 2023-05-15)

  - [serenity-x86_64-20230520-28d2e26.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230520-28d2e26.img.gz) (Raw image, 182.46 MiB)

---

### Images :floppy_disk:

- [serenity-x86_64-20230520-28d2e26.img.gz](https://serenity-files.halves.dev/builds/serenity-x86_64-20230520-28d2e26.img.gz) (Raw image, 182.46 MiB)

### Last commit :star:

```git
commit 28d2e266788e88ae595b4645ac03f9a04351b877
Author:     Andrew Kaster <akaster@serenityos.org>
AuthorDate: Mon May 15 01:42:26 2023 -0600
Commit:     Andrew Kaster <akaster@serenityos.org>
CommitDate: Fri May 19 20:12:25 2023 -0600

    Kernel: Enable data and instruction cache on aarch64
    
    Enabling these will fix the Unsupported Exclusive or Atomic access data
    fault we get on bare metal Raspberry Pi 3. On A53/A57 chips (and newer),
    atomic compare-exchange operations require the data cache to be enabled.
```
