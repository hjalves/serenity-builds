FROM ubuntu:24.04

RUN apt-get update -y && apt-get install -y \
    git build-essential cmake curl libmpfr-dev libmpc-dev libgmp-dev  \
    e2fsprogs ninja-build qemu-system-gui qemu-system-x86 qemu-utils  \
    ccache rsync unzip texinfo fuse fuse2fs genext2fs sudo \
    grub2-common grub-pc-bin parted udev hugo python-is-python3 \
    gcc-13 g++-13 libssl-dev

RUN useradd -m buildbot && echo "buildbot ALL=(root) NOPASSWD:ALL" > /etc/sudoers.d/buildbot
USER buildbot
